The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 13.3, 12.7, 11.12, 10.17, and 9.6.22.
This release closes three security vulnerabilities and fixes over 45 bugs
reported over the last three months.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Security Issues
---------------

### [CVE-2021-32027](https://www.postgresql.org/support/security/CVE-2021-32027/): Buffer overrun from integer overflow in array subscripting calculations

Versions Affected: 9.6 - 13. The security team typically does not test
unsupported versions, but this problem is quite old.

While modifying certain SQL array values, missing bounds checks let
authenticated database users write arbitrary bytes to a wide area of server
memory.

The PostgreSQL project thanks Tom Lane for reporting this problem.

### [CVE-2021-32028](https://www.postgresql.org/support/security/CVE-2021-32028/): Memory disclosure in `INSERT ... ON CONFLICT ... DO UPDATE`

Versions Affected: 9.6 - 13. The security team typically does not test
unsupported versions. The feature first appeared in 9.5.

Using an `INSERT ... ON CONFLICT ... DO UPDATE` command on a purpose-crafted
table, an attacker can read arbitrary bytes of server memory. In the default
configuration, any authenticated database user can create prerequisite objects
and complete this attack at will. A user lacking the `CREATE` and `TEMPORARY`
privileges on all databases and the `CREATE` privilege on all schemas cannot use
this attack at will.

The PostgreSQL project thanks Andres Freund for reporting this problem.

### [CVE-2021-32029](https://www.postgresql.org/support/security/CVE-2021-32029/): Memory disclosure in partitioned-table `UPDATE ... RETURNING`

Versions Affected: 11 - 13

Using an `UPDATE ... RETURNING` on a purpose-crafted partitioned table, an
attacker can read arbitrary bytes of server memory. In the default
configuration, any authenticated database user can create prerequisite objects
and complete this attack at will. A user lacking the `CREATE` and `TEMPORARY`
privileges on all databases and the `CREATE` privilege on all schemas typically
cannot use this attack at will.

The PostgreSQL project thanks Tom Lane for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update fixes over 45 bugs that were reported in the last several months.
Some of these issues only affect version 13, but could also apply to other
supported versions.

Some of these fixes include:

* Fix potential incorrect computation of `UPDATE ... RETURNING` outputs for
joined, cross-partition updates.
* Fix `ALTER TABLE ... ALTER CONSTRAINT` when used on foreign-key constraints on
partitioned tables. The command would fail to adjust the `DEFERRABLE` and/or
`INITIALLY DEFERRED` properties of the constraints and triggers of leaf
partitions, leading to unexpected behavior. After updating to this version, you
can execute the `ALTER TABLE ... ALTER CONSTRAINT` command to fix any
misbehaving partitioned tables.
* Ensure that when a child table is attached with `ALTER TABLE ... INHERIT` that
generated columns in the parent are generated in the same way in the child.
* Forbid marking an identity column as `NULL`.
* Allow `ALTER ROLE ... SET`/`ALTER DATABASE ... SET` to set the role,
session\_authorization, and temp\_buffers parameters.
* Ensure that REINDEX CONCURRENTLY preserves any statistics target set for the
index.
* Fix an issue where, in some cases, saving records within `AFTER` triggers
could cause crashes.
* Fix how `to_char()` handles Roman-numeral month format codes with negative
intervals.
* Fix use of uninitialized value while parsing an `\{m,n\}` quantifier in a
BRE-mode regular expression.
* Fix "could not find pathkey item to sort" planner errors that occur in some
situations when the sort key involves an aggregate or window function.
* Fix issue with BRIN index bitmap scans that could lead to "could not open
file" errors.
* Fix potentially wrong answers from GIN `tsvector` index searches when there
are many matching records.
* Fixes for `COMMIT AND CHAIN` functionality on both the server and `psql`.
* Avoid incorrect timeline change while recovering uncommitted two-phase
transactions from WAL, which could lead to consistency issues and the inability
to restart the server.
* Ensure that` wal_sync_method` is set to `fdatasync` by default on newer
FreeBSD releases.
* Disable the `vacuum_cleanup_index_scale_factor` parameter and storage option.
* Fix several memory leaks in the server, including one with SSL/TLS parameter
initialization.
* Restore the previous behavior of `\connect service=XYZ` to `psql`, i.e.
disallow environmental variables (e.g. `PGPORT`) from overriding entries in the
service file.
* Fix how `pg_dump` handles generated columns in partitioned tables.
* Add additional checks to `pg_upgrade` for user tables containing
non-upgradable data types.
* On Windows, `initdb` now prints instructions about how to start the server
with `pg_ctl` using backslash separators.
* Fix `pg_waldump` to count XACT records correctly when generating per-record
statistics.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 9.6 EOL Notice
-------------------------

PostgreSQL 9.6 will stop receiving fixes on November 11, 2021. If you are
running PostgreSQL 9.6 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
