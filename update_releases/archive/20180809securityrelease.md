2018-08-08 Cumulative Security Update and PostgreSQL 11 Beta 3 Released
=======================================================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 10.5, 9.6.10, 9.5.14, 9.4.19, 9.3.24. This release fixes two security issues as well as bugs reported over the last three months.

If you have untrusted users accessing your system and you are either running PostgreSQL 9.5 or a newer version OR have installed the [dblink](https://www.postgresql.org/docs/current/static/dblink.html) or [postgres_fdw](https://www.postgresql.org/docs/current/static/postgres-fdw.html) extensions, you must apply this update as soon as possible. All other users can upgrade at the next convenient downtime.

Please note that PostgreSQL changed its versioning scheme with the release of version 10.0, so updating to version 10.5 from any 10.x release is considered a minor update.

The PostgreSQL Global Development Group also announces that the third beta release of PostgreSQL 11 is now available for download. This release contains previews of all features that will be available in the final release of PostgreSQL 11 (though some details of the release could change before then) as well as bug fixes that were reported during the second beta.

Security Issues
---------------

Two security vulnerabilities have been closed by this release:

* [CVE-2018-10915](https://access.redhat.com/security/cve/CVE-2018-10915): Certain host connection parameters defeat client-side security defenses

libpq, the client connection API for PostgreSQL that is also used by other connection libraries, had an internal issue where it did not reset all of its connection state variables when attempting to reconnect. In particular, the state variable that determined whether or not a password is needed for a connection would not be reset, which could allow users of features requiring libpq, such as the [dblink](https://www.postgresql.org/docs/current/static/dblink.html) or  [postgres_fdw](https://www.postgresql.org/docs/current/static/postgres-fdw.html) extensions, to login to servers they should not be able to access.

You can check if your database has either extension installed by running the following from your PostgreSQL shell:

`\dx dblink|postgres_fdw`

Users are advised to upgrade their libpq installations as soon as possible.

The PostgreSQL Global Development Group thanks Andrew Krasichkov for reporting this problem.

* [CVE-2018-10925](https://access.redhat.com/security/cve/CVE-2018-10925): Memory disclosure and missing authorization in `INSERT ... ON CONFLICT DO UPDATE`

An attacker able to issue CREATE TABLE can read arbitrary bytes of server memory using an upsert (`INSERT ... ON CONFLICT DO UPDATE`) query.  By default, any user can exploit that. A user that has specific INSERT privileges and an UPDATE privilege on at least one column in a given table can also update other columns using a view and an upsert query.

Bug Fixes and Improvements
--------------------------

This update also fixes over 40 bugs reported in the last several months. Some of these issues affect only version 10, but many affect all supported versions.

These fixes include:

* Several fixes related to `VACUUM`, including an issue that could lead to data corruption in certain system catalog tables
* Several fixes for replaying write-ahead logs, including a case where a just-promoted standby server would not restart if it crashed before its first post-recovery checkpoint
* Several performance improvements for replaying write-ahead logs
* Several fixes for logical replication and logical decoding, including ensuring logical WAL senders are reporting the streaming state correctly
* Allow replication slots to be dropped in single-user mode
* Fix to have `variance` and similar aggregate functions return accurate results when executed using parallel query
* Fix SQL-standard FETCH FIRST syntax to allow parameters ($n), as the standard expects
* Fix to ensure that a process doing a parallel index scan will respond to signals, such as one to abort a query
* Fix EXPLAIN's accounting for resource usage, particularly buffer accesses, in parallel workers
* Several fixes for the query planner including improving the cost estimates for hash-joins and choosing to use indexes for mergejoins on composite type columns
* Fix performance regression related to POSIX semaphores for multi-CPU systems running Linux or FreeBSD
* Fix for GIN indexes that could lead to an assertion failure after a pg\_upgrade from a version before PostgreSQL 9.4
* Fix for `SHOW ALL` to display superuser configuration settings to roles that are allowed to read all settings
* Fix issue where `COPY FROM .. WITH HEADER` would drop a line after every 4,294,967,296 lines processed
* Several fixes for XML support, including using the document node as the context for XPath queries as defined in the SQL standard, which affects the `xpath` and `xpath_exists` functions, as well as `XMLTABLE`
* Fix libpq for certain cases where `hostaddr` is used
* Several ecpg fixes for Windows
* Fix password prompting in Windows client programs so that echo is properly disabled
* Several `pg_dump` fixes, including correctly outputting `REPLICA IDENTITY` properties for constraint indexes
* Make `pg_upgrade` check that the old server was shut down cleanly

This update also contains tzdata release 2018e, with updates for North Korea. The 2018e also reintroduces the negative-DST changes that were originally introduced in 2018a, which affects historical and present timestamps for Ireland (1971-), as well as historical timestamps for Namibia (1994-2017) and the former Czechoslovakia (1946-1947). If your application is storing timestamps with those timezones in the affected date ranges, we ask that you please test to ensure your applications behave as expected.

PostgreSQL 11 Beta 3 Fixes and Improvements
-------------------------------------------

PostgreSQL 11 Beta 3 contains applicable bug fixes from the cumulative release as well as over 20 fixes of its own. For a full list of fixes for PostgreSQL 11 Beta 3, please visit the [open items](https://wiki.postgresql.org/wiki/PostgreSQL_11_Open_Items#resolved_before_11beta3) page.

EOL Warning for PostgreSQL 9.3
------------------------------

PostgreSQL 9.3 will become end-of-life after the next planned release in November.  We urge users to start planning an upgrade to a later version of PostgreSQL as soon as possible. See our Versioning Policy for more information

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use `pg_upgrade` in order to apply this update release; you may simply shutdown PostgreSQL and update its binaries.

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Upgrading to PostgreSQL 11 Beta 3
---------------------------------

To upgrade to PostgreSQL 11 Beta 3 from Beta 1 or 2, or a earlier version of PostgreSQL, you will to use a strategy similar to upgrading between major versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`). For more information, please visit the documentation section on [upgrading](https://www.postgresql.org/docs/11/static/upgrading.html).

PostgreSQL 11 Beta Schedule and Testing
---------------------------------------

This is the third beta release of version 11. The PostgreSQL Project will release additional betas as required for testing, followed by one or more release candidates, until the final release in late 2018. For further information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

The stability of each PostgreSQL release greatly depends on you, the community, to test the upcoming version with your workloads and testing tools in order to find bugs and regressions before the release of PostgreSQL 11. We greatly appreciate all of the testing performed to date as we get closer to the final release. Your feedback and testing will help determine the final tweaks on the new features, so please continue to test. The quality of user testing helps determine when we can make a final release.

A list of [open items](https://wiki.postgresql.org/wiki/PostgreSQL_11_Open_Items) is publicly available in the PostgreSQL wiki.  You can [report bugs](https://www.postgresql.org/account/submitbug/) using this form on the PostgreSQL website:

[https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/current/static/release.html)
* [PostgreSQL 11 Beta Release Notes](https://www.postgresql.org/docs/11/static/release-11.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 11 Open Items](https://wiki.postgresql.org/wiki/PostgreSQL_11_Open_Items)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
