2016-05-12 Cumulative Bug Fix Release
======================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.5.3, 9.4.8, 9.3.13, 9.2.17 and 9.1.22. This release fixes a number of issues reported by users over the last two months. Most database administrators should plan to upgrade at the next available downtime, unless they have been affected directly by the fixed issues.

Bug Fixes and Improvements
--------------------------

This update fixes several problems which caused downtime for users, including:

* Clearing the OpenSSL error queue before OpenSSL calls, preventing errors in SSL
  connections, particularly when using the Python, Ruby or PHP OpenSSL wrappers
* Fixed the "failed to build N-way joins" planner error
* Fixed incorrect handling of equivalence in multilevel nestloop query plans,
  which could emit rows which didn't match the WHERE clause.
* Prevented two memory leaks with using GIN indexes, including a potential index
  corruption risk.

The release also includes many other bug fixes for reported issues, many of which
affect all supported versions:

* Fix corner-case parser failures occurring when operator_precedence_warning is turned on
* Prevent possible misbehavior of TH, th, and Y,YYY format codes in to_timestamp()
* Correct dumping of VIEWs and RULEs which use ANY (array) in a subselect
* Disallow newlines in ALTER SYSTEM parameter values
* Avoid possible misbehavior after failing to remove a tablespace symlink
* Fix crash in logical decoding on alignment-picky platforms
* Avoid repeated requests for feedback from receiver while shutting down walsender
* Multiple fixes for pg_upgrade
* Support building with Visual Studio 2015

This update also contains tzdata release 2016d, with updates for Russia, Venezuela, Kirov, and Tomsk.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.  Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Links:
* Download: http://www.postgresql.org/download
* Release Notes: http://www.postgresql.org/docs/current/static/release.html
* Security Page: http://www.postgresql.org/support/security/
* Abbreviated Keys Issue: http://wiki.postgresql.org/abbreviatedkeys_issue
