2016-03-31 Security Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.5.2, 9.4.7, 9.3.12, 9.2.16, and 9.1.21. This release fixes two security issues and one index corruption issue in version 9.5.  It also contains a variety of bug fixes for earlier versions.  Users of PostgreSQL 9.5.0 or 9.5.1 should update as soon as possible.

Security Fixes for RLS, BRIN
----------------------------

This release closes security hole [CVE-2016-2193](https://access.redhat.com/security/cve/CVE-2016-2193), where a query plan might get reused for more than one ROLE in the same session.  This could cause the wrong set of Row Level Security (RLS) policies to be used for the query.

The update also fixes [CVE-2016-3065](https://access.redhat.com/security/cve/CVE-2016-3065), a server crash bug triggered by using `pageinspect` with BRIN index pages.  Since an attacker might be able to expose a few bytes of server memory, this crash is being treated as a security issue.

Abbreviated Keys and Corrupt Indexes
------------------------------------

In this release, the PostgreSQL Project has been forced to disable 9.5's Abbreviated Keys performance feature for many indexes due to reports of index corruption.  This may affect any B-tree indexes on TEXT, VARCHAR, and CHAR columns which are not in "C" locale. Indexes in other locales will lose the performance benefits of the feature, and should be REINDEXed in case of existing index corruption. The feature may be re-enabled in future versions if the project finds a solution for the problem.  See the release notes, and [the wiki page on this issue](http://wiki.postgresql.org/abbreviatedkeys_issue) for more information.

Other Fixes and Improvements
----------------------------

In addition to the above, many other issues were patched in this release based on bugs reported by our users over the last few months.  This includes bugs which affect multiple versions of PostgreSQL, such as:

* Fix two bugs in indexed ROW() comparisons
* Avoid data loss due to renaming files
* Prevent an error in rechecking rows in SELECT FOR UPDATE/SHARE
* Fix bugs in multiple json_* and jsonb_* functions
* Log lock waits for INSERT ON CONFLICT correctly
* Ignore recovery_min_apply_delay until reaching a consistent state
* Fix issue with pg_subtrans XID wraparound
* Fix assorted bugs in Logical Decoding
* Fix planner error with nested security barrier views
* Prevent memory leak in GIN indexes
* Fix two issues with ispell dictionaries
* Avoid a crash on old Windows versions
* Skip creating an erroneous delete script in pg_upgrade
* Correctly translate empty arrays into PL/Perl
* Make PL/Python cope with identifier names

This update also contains tzdata release 2016c, with updates for Azerbaijan, Chile, Haiti, Palestine, and Russia, and historical fixes for other regions.

Updating
--------

Users of version 9.5 will want to REINDEX any indexes they created on character columns in non-C locales.  Users of other versions who have skipped multiple update releases may need to perform additional post-update steps; see the Release Notes for details.

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.

Links:
  * [Download](http://www.postgresql.org/download)
  * [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
  * [Security Page](http://www.postgresql.org/support/security/)
  * [Abbreviated Keys Issue](http://wiki.postgresql.org/abbreviatedkeys_issue)
