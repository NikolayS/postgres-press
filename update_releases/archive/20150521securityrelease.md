The PostgreSQL Global Development Group has released an update with multiple functionality and security fixes to all supported versions of the PostgreSQL database system, which includes minor versions 9.4.2, 9.3.7, 9.2.11, 9.1.16, and 9.0.20. The update contains a critical fix for a potential data corruption issue in PostgreSQL 9.3 and 9.4; users of those versions should update their servers at the next possible opportunity.

Data Corruption Fix
-------------------

For users of PostgreSQL versions 9.3 or 9.4, this release fixes a problem where the database will fail to protect against "multixact wraparound", resulting in data corruption or loss. Users with a high transaction rate (1 million or more per hour) in a database with many foreign keys are especially vulnerable. We strongly urge all users of 9.4 and 9.3 to update their installations in the next few days.

Users of versions 9.2 and earlier are not affected by this issue.

Security Fixes
--------------

This update fixes three security vulnerabilities reported in PostgreSQL over the past few months.  Nether of these issues is seen as particularly urgent. However, users should examine them in case their installations are vulnerable:

* [CVE-2015-3165](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3165) Double "free" after authentication timeout.
* [CVE-2015-3166](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3166) Unanticipated errors from the standard library.
* [CVE-2015-3167](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3167) pgcrypto has multiple error messages for decryption with an incorrect key.

Additionally, we are recommending that all users who use Kerberos, GSSAPI, or SSPI authentication set include_realm to 1 in pg_hba.conf, which will become the default in future versions.

More information about these issues, as well as older patched issues, is available on the PostgreSQL Security Page.

Other Fixes and Improvements
----------------------------

A new, non-default version of the citext extension fixes its previously undocumented regexp_matches() functions to align with the ordinary text version of those functions. The fixed version has a different return type than the old version, so users of CIText should test their applications before updating the function by running "ALTER EXTENSION citext UPDATE".

In addition to the above, more than 50 reported issues have been fixed in this cumulative update release.  Most of the issues named affect all supported versions.  These fixes include:

* Render infinite dates and timestamps as infinity when converting to json
* Fix json/jsonb's populate_record() and to_record()
* Fix incorrect checking of deferred exclusion constraints
* Improve planning of star-schema-style queries
* Fix three issues with joins
* Ensure correct locking with security barrier views
* Fix deadlock at startup when max_prepared_transactions is too small
* Recursively fsync() the data directory after a crash
* Fix autovacuum launcher's possible failure to shut down
* Cope with unexpected signals in LockBufferForCleanup()
* Fix crash when doing COPY IN to a table with check constraints
* Avoid waiting for synchronous replication of read-only transactions
* Fix two issues with hash indexes
* Prevent memory leaks in GIN index vacuum
* Fix two issues with background workers
* Several fixes to Logical Decoding replication
* Fix several minor issues with pg_dump and pg_upgrade

This release includes an update to tzdata release 2015d, with updates to Egypt, Mongolia, and Palestine, plus historical changes in Canada and Chile.

9.0 EOL Soon
------------

Version 9.0 will become End-Of-Life in September 2015.  This means that this update is likely to be the next-to-last update for that version.  Users of PostgreSQL 9.0 should start planning to upgrade to a more current version before then.  See our [versioning policy](http://www.postgresql.org/support/versioning/) for more information about EOL dates.


As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries. Users of the CIText extension need to run a command. Users who have skipped multiple update releases may need to perform additional post-update steps; see the Release Notes for details.

Links:
  * [Download](http://www.postgresql.org/download)
  * [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
  * [Security Page](http://www.postgresql.org/support/security/)
