2018-11-08 Cumulative Security Update
=====================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 11.1, 10.6, 9.6.11, 9.5.15, 9.4.20, and 9.3.25. This release fixes one security issue as well as bugs reported over the last three months.

All users using the affected versions of PostgreSQL should update as soon as possible. Please see the notes on "Updating" below for any post-update steps that may be required if you are using `pg_stat_statements` in your installation.

This update is also the final release for PostgreSQL 9.3, which is now end-of-life and will no longer receive any bug or security fixes. If your environment still uses PostgreSQL 9.3, please make plans to update to a community supported version as soon as possible. Please see our [versioning policy](https://www.postgresql.org/support/versioning/) for more information.

Security Issues
---------------

One security vulnerability has been closed by this release:

* [CVE-2018-16850](https://access.redhat.com/security/cve/CVE-2018-16850): SQL injection in `pg_upgrade` and `pg_dump`, via `CREATE TRIGGER ... REFERENCING`.

Using a purpose-crafted trigger definition, an attacker can run arbitrary SQL statements with superuser privileges when a superuser runs `pg_upgrade` on the database or during a pg\_dump dump/restore cycle.  This attack requires a `CREATE` privilege on some non-temporary schema or a `TRIGGER` privilege on a table.  This is exploitable in the default PostgreSQL configuration, where all users have `CREATE` privilege on `public` schema.

Bug Fixes and Improvements
--------------------------

This update also fixes numerous bugs that were reported in the last several months. Some of these issues affect only version 11, but many affect all supported versions.

These releases include fixes that:

* Ensure that automatically created child indexes are created in the same tablespace as the parent partitioned index
* Fix several crashes with triggers
* Fix problems with applying `ON COMMIT DELETE ROWS` to a partitioned temporary table
* Fix how `NULL` values are handled when using `LEFT JOIN` with a parallelized hash join
* Several fixes around using named or defaulted arguments in `CALL` statements
* Fix for strict aggregate functions (i.e. aggregates that cannot accept `NULL` inputs) with ORDER BY columns that enforces the strictness check
* Fix with `CASE` statements where an expression was cast to an array type
* Disable an optimization for updating expression indexes in order to prevent a crash
* Fix a memory leak that occurred on a specific case of using a SP-GiST index
* Fix for `pg_verify_checksums` incorrectly reporting on files that are not expected to have checksums
* Prevent the PostgreSQL server from starting when `wal_level` is set to a value that cannot support an existing replication slot
* Ensure that the server will process already-received NOTIFY and SIGTERM interrupts before waiting for client input
* Allow PL/Ruby to work with newer versions of PostgreSQL
* Fix for character-class checks on Windows for Unicode characters above U+FFFF, which affected full-text search as well as `contrib/ltree` and `contrib/pg_trgm`
* Fix a case where `psql` would not report the receipt of a message from a `NOTIFY` call until after the next command
* Fix build problems on macOS 10.14 (Mojave)
* Several build fixes for the Windows platform

This updates also contains tzdata release 2018g for DST law changes in Chile, Fiji, Morocco, and Russia (Volgograd), plus historical corrections for China, Hawaii, Japan, Macau, and North Korea.

PostgreSQL 9.3 is End-of-Life (EOL)
-----------------------------------

PostgreSQL 9.3 is now end-of-life and will no longer receive any bug or security fixes.  We urge users to start planning an upgrade to a later version of PostgreSQL as soon as possible.  Please see our [versioning policy](https://www.postgresql.org/support/versioning/) for more information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use `pg_upgrade` in order to apply this update release; you may simply shutdown PostgreSQL and update its binaries.

If your system is using `pg_stat_statements` and you are running a version of PostgreSQL 10 or PostgreSQL 11, we advise that you execute the following command after upgrading:

`ALTER EXTENSION pg_stat_statements UPDATE;`

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/current/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
