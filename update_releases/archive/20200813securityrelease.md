2020-08-13 Cumulative Security Update
====================================

The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 12.4, 11.9, 10.14, 9.6.19, and
9.5.23, as well as the 3rd Beta release of PostgreSQL 13. This release closes
two security vulnerabilities and fixes over 50 bugs reported over the last three
months.

Please plan to update at your earliest convenience.

Security Issues
---------------

* CVE-2020-14349: Uncontrolled search path element in logical replication.

Versions Affected: 10 - 12.

The PostgreSQL `search_path` setting determines schemas searched for tables,
functions, operators, etc. The [CVE-2018-1058](https://wiki.postgresql.org/wiki/A_Guide_to_CVE-2018-1058%3A_Protect_Your_Search_Path)
fix caused most PostgreSQL-provided client applications to sanitize `search_path`,
but logical replication continued to leave `search_path` unchanged. Users of a
replication publisher or subscriber database can create objects in the `public`
schema and harness them to execute arbitrary SQL functions under the identity
running replication, often a superuser. Installations having adopted a documented
secure [schema usage pattern](https://www.postgresql.org/docs/current/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)
are not vulnerable.

The PostgreSQL project thanks Noah Misch for reporting this problem.

* CVE-2020-14350: Uncontrolled search path element in `CREATE EXTENSION`.

Versions Affected: 9.5 - 12. The security team typically does not test
unsupported versions, but this problem is quite old.

When a superuser runs certain `CREATE EXTENSION` statements, users may be able
to execute arbitrary SQL functions under the identity of that superuser. The
attacker must have permission to create objects in the new extension's schema
or a schema of a prerequisite extension.  Not all extensions are vulnerable.

In addition to correcting the extensions provided with PostgreSQL, the
PostgreSQL Global Development Group is issuing guidance for third-party
extension authors to secure their own work.

The PostgreSQL project thanks Andres Freund for reporting this problem.

A Note on the PostgreSQL 13 Beta
--------------------------------

This release marks the third beta release of PostgreSQL 13 and puts the
community one step closer to general availability this fall.

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 13 in your database systems to help us
eliminate any bugs or other issues that may exist. While we do not advise you to
run PostgreSQL 13 Beta 3 in your production environments, we encourage you to
find ways to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 13
release upholds our standards of providing a stable, reliable release of the
world's most advanced open source relational database.

PostgreSQL 9.5 EOL Notice
-------------------------

PostgreSQL 9.5 will stop receiving fixes on February 11, 2021. If you are
running PostgreSQL 9.5 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Bug Fixes and Improvements
--------------------------

This update also fixes over 50 bugs that were reported in the last several
months. Some of these issues affect only version 12, but many affect all
supported versions.

Some of these fixes include:

* Fix edge cases in partition pruning involving multiple partition key columns
with multiple or no constraining WHERE clauses.
* Several fixes for query planning and execution involving partitions.
* Fix for determining when to execute a column-specific UPDATE trigger on a
logical replication subscriber.
* `pg_replication_slot_advance()` now updates the oldest xmin and LSN values, as
the failure to do this could prevent resources (e.g. WAL files) from being cleaned up.
* Fix a performance regression in `ts_headline()`.
* Ensure that `pg_read_file()` and related functions read until EOF is reached,
which fixes compatibility with pipes and other virtual files.
* Forbid numeric `NaN` values in jsonpath computations, which do not exist in
SQL nor JSON.
* Several fixes for `NaN` inputs with aggregate functions. This fixes a change
in PostgreSQL 12 where `NaN` values caused the following aggregates to emit
values of `0` instead of `NaN`: `corr()`, `covar_pop()`, `regr_intercept()`,
`regr_r2()`, `regr_slope()`, `regr_sxx()`, `regr_sxy()`, `regr_syy()`,
`stddev_pop()`, and `var_pop()`.
* `time` and `timetz` values fractionally greater than `24:00:00` are now
rejected.
* Several fixes for `EXPLAIN`, including a fix for reporting resource usage when
a plan uses parallel workers with "Gather Merge" nodes.
* Fix timing of constraint revalidation in `ALTER TABLE` that could lead to odd
errors.
* Fix for REINDEX CONCURRENTLY that could prevent old values from being included
in future logical decoding output.
* Fix for LATERAL references that could potentially cause crashes during query
execution.
* Use the collation specified for a query when estimating operator costs
* Fix conflict-checking anomalies in SERIALIZABLE transaction isolation mode.
* Ensure checkpointer process discards file sync requests when fsync is off
* Fix issue where `pg_control` could be written out with an inconsistent
checksum, which could lead to the inability to restart the database if it
crashed before the next `pg_control` update.
* Ensure that libpq continues to try to read from the database connection socket
after a write failure, as this allows the connection to collect any final error
messages from the server.
* Report out-of-disk-space errors properly in `pg_dump` and `pg_basebackup`
* Several fixes for `pg_restore`, including a fix for parallel restore on tables
that have both table-level and column-level privileges.
* Fix for `pg_upgrade` to ensure it runs with `vacuum_defer_cleanup_age` set to
`0`.
* Fix how `pg_rewind` handles just-deleted files in the source data directory
* Fix failure to initialize local state correctly in `contrib/dblink`, which
could lead to `dblink_close()` issuing an unexpected `COMMIT` on the remote
server.
* Change `contrib/amcheck` to not report about deleted index pages that are
empty, as this is normal during WAL replay.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/current/release.html).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/current/release.html).

**NOTE**: PostgreSQL 9.5 will stop receiving fixes on February 11, 2021. Please
see our [versioning policy](https://www.postgresql.org/support/versioning/) for
more information.


PostgreSQL 13 Beta 3 Notes
--------------------------

PostgreSQL 13 Beta 3 introduces a new configuration parameter,
`hash_mem_multiplier`, which allows users to tune how much memory should be
allotted for a hash aggregation. This gives users more control over whether
hash aggregates will use disk storage or remain in memory, the latter being the
only option prior to PostgreSQL 13.

PostgreSQL 13 Beta 3 also removes the `hashagg_avoid_disk_plan` configuration
parameter, which was also previously known as `enable_hashagg_disk`.

For a list of changes that are included in PostgreSQL 13 Beta 3, please review
the [open items](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items#resolved_before_13beta3)
page:

  [https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items#resolved_before_13beta3](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items#resolved_before_13beta3)

To upgrade to PostgreSQL 13 Beta 3 from Beta 2, Beta 1, or an earlier version of
PostgreSQL, you will need to use a strategy similar to upgrading between
major versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`).
For more information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/13/static/upgrading.html).

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 13. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

https://www.postgresql.org/account/submitbug/

Beta Schedule
-------------

This is the third beta release of version 13. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2020. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 13 Beta Release Notes](https://www.postgresql.org/docs/13/release-13.html)
* [PostgreSQL 13 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
