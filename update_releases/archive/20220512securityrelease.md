The PostgreSQL Global Development Group has released an update to all supported
versions of PostgreSQL, including 14.3, 13.7, 12.11, 11.16, and 10.21. This
release closes one security vulnerability and fixes over 50 bugs reported over
the last three months.

We encourage you to install this update at your earliest possible convenience.

If you have any GiST indexes on columns using the
[`ltree`](https://www.postgresql.org/docs/current/ltree.html) data type,
you will need to [reindex](https://www.postgresql.org/docs/current/sql-reindex.html)
them after upgrading.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 10 EOL Notice
------------------------

PostgreSQL 10 will stop receiving fixes on November 10, 2022. If you are
running PostgreSQL 10 in a production environment, we suggest that you make
plans to upgrade to a newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

### [CVE-2022-1552](https://www.postgresql.org/support/security/CVE-2022-1552/): Autovacuum, REINDEX, and others omit "security restricted operation" sandbox.

Versions Affected: 10 - 14. The security team typically does not test
unsupported versions, but this problem is quite old.

Autovacuum, `REINDEX`, `CREATE INDEX`, `REFRESH MATERIALIZED VIEW`, `CLUSTER`,
and `pg_amcheck` made incomplete efforts to operate safely when a privileged
user is maintaining another user's objects. Those commands activated relevant
protections too late or not at all. An attacker having permission to create
non-temp objects in at least one schema could execute arbitrary SQL functions
under a superuser identity.

While promptly updating PostgreSQL is the best remediation for most users, a
user unable to do that can work around the vulnerability by disabling
autovacuum, not manually running the above commands, and not restoring from
output of the `pg_dump` command. Performance may degrade quickly under this
workaround. `VACUUM` is safe, and all commands are fine when a trusted user
owns the target object.

The PostgreSQL project thanks Alexander Lakhin for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update fixes over 50 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 14. Some of these issues may also
affect other supported versions of PostgreSQL.

Included in this release:

* Fix issue that could lead to corruption of GiST indexes on
[`ltree`](https://www.postgresql.org/docs/current/ltree.html) columns. After
upgrading, you will need to [reindex](https://www.postgresql.org/docs/current/sql-reindex.html)
any GiST indexes on [`ltree`](https://www.postgresql.org/docs/current/ltree.html)
columns.
* Column names in tuples produced by a whole-row variable (e.g. `tbl.*`) outside
of a top-level of a `SELECT` list are now always associated with those of the
associated named composite type, if there is one. The
[release notes](https://www.postgresql.org/docs/release/) detail a workaround if
you depend on the previous behavior.
* Fix incorrect rounding when extracting epoch values from `interval` types.
* Prevent issues with calling `pg_stat_get_replication_slot(NULL)`.
* Fix incorrect output for types `timestamptz` and `timetz` in
`table_to_xmlschema()`.
* Fix errors related to a planner issue that affected asynchronous remote
queries.
* Fix planner failure if a query using `SEARCH` or `CYCLE` features contains a
duplicate common-table expression (`WITH`) name.
* Fix `ALTER FUNCTION` to support changing a function's parallelism property and
its `SET`-variable list in the same command.
* Fix incorrect sorting of table rows when using `CLUSTER` on an index whose
leading key is an expression.
* Prevent data loss if a system crash occurs shortly after a sorted GiST index
build.
* Fix risk of deadlock failures while dropping a partitioned index.
* Fix race condition between `DROP TABLESPACE` and checkpointing that could fail
to remove all dead files from the tablespace directory.
* Fix potential issue in crash recovery after a `TRUNCATE` command that overlaps
with a checkpoint.
* Re-allow `_` as the first character in a custom configuration parameter name.
* Fix `PANIC: xlog flush request is not satisfied` failure during standby
promotion when there is a missing WAL continuation record.
* Fix possibility of self-deadlock in hot standby conflict handling.
* Ensure that logical replication apply workers can be restarted when the server
is near the `max_sync_workers_per_subscription` limit.
* Disallow execution of SPI functions during PL/Perl function compilation.
* libpq now accepts root-owned SSL private key files, which matches the rules
the server has used since the 9.6 release.
* Re-allow `database.schema.table` patterns in
[`psql`](https://www.postgresql.org/docs/current/app-psql.html),
[`pg_dump`](https://www.postgresql.org/docs/current/app-pgdump.html), and
[`pg_amcheck`](https://www.postgresql.org/docs/current/app-pgamcheck.html).
* Several fixes for [`pageinspect`](https://www.postgresql.org/docs/current/pageinspect.html)
to improve overall stability.
* Disable batch insertion in [`postgres_fdw`](https://www.postgresql.org/docs/current/postgres-fdw.html)
when `BEFORE INSERT ... FOR EACH ROW` triggers exist on the foreign table.
* Update JIT code to work with LLVM 14.

This update also contains the tzdata release 2022a for DST law changes in
Palestine, plus historical corrections for Chile and Ukraine.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

However, if you have any GiST indexes on columns using the
[`ltree`](https://www.postgresql.org/docs/current/ltree.html) data type,
you will need to [reindex](https://www.postgresql.org/docs/current/sql-reindex.html)
them after upgrading.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
