2019-02-14 Cumulative Update Release
====================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 11.2, 10.7, 9.6.12, 9.5.16, and 9.4.21. This release changes the behavior in how PostgreSQL interfaces with `fsync()` and includes fixes for partitioning and over 70 other bugs that were reported over the past three months.

Users should plan to apply this update at the next scheduled downtime.

Highlight: Change in behavior with fsync()
------------------------------------------

When available in an operating system and enabled in the configuration file (which it is by default), PostgreSQL uses the kernel function `fsync()` to help ensure that data is written to a disk. In some operating systems that provide `fsync()`, when the kernel is unable to write out the data, it returns a failure and flushes the data that was supposed to be written from its data buffers.

This flushing operation has an unfortunate side-effect for PostgreSQL: if PostgreSQL tries again to write the data to disk by again calling `fsync()`, `fsync()` will report back that it succeeded, but the data that PostgreSQL believed to be saved to the disk would not actually be written. This presents a possible data corruption scenario.

This update modifies how PostgreSQL handles a `fsync()` failure: PostgreSQL will no longer retry calling `fsync()` but instead will panic. In this case, PostgreSQL can then replay the data from the write-ahead log (WAL) to help ensure the data is written. While this may appear to be a suboptimal solution, there are presently few alternatives and, based on reports, the problem case occurs extremely rarely.

A new server parameter `data_sync_retry` has been added to manage this behavior. If you are certain that your kernel does not discard dirty data buffers in such scenarios, you can set `data_sync_retry` to `on` to restore the old behavior.

Bug Fixes and Improvements
--------------------------

This update introduces a change into how release notes are packaged. As of this update, all currently supported versions of PostgreSQL will only contain their major version-specific release notes. For example, PostgreSQL 11 only packages the release notes for version 11.2, 11.1, and 11.0. The release notes for unsupported versions (PostgreSQL 9.3 and older) will be available in older releases and from a soon-to-be released archive on the PostgreSQL website.

This update also fixes over 70 bugs that were reported in the last several months. Some of these issues affect only version 11, but many affect all supported versions.

Some of these fixes include:

* Fix handling of unique indexes with `INCLUDE` columns on partitioned tables
* Ensure that `NOT NULL` constraints of a partitioned table are honored within its partitions
* Several fixes for constraints on partitioned tables
* Fix problems with applying `ON COMMIT DROP` and `ON COMMIT DELETE ROWS` to partitioned tables and tables with inheritance children
* Disallow `COPY FREEZE` on partitioned tables
* Several fixes for the `ALTER TABLE .. ADD COLUMN` with a non-nullable default feature, including a possible index corruption case
* Several fixes in GIN indexes, including avoiding a deadlock with vacuuming and concurrent index insertions (which partially reverts a performance improvement introduced in PostgreSQL 10)
* Fix possible crashes in logical replication when index expressions or predicates are in use
* Several fixes for the write-ahead log (WAL)
* Fix possible crash in `UPDATE` with a multiple `SET` clause using a sub-`SELECT`
* Fix crash when zero rows are provided to `json[b]_populate_recordset()` or `json[b]_to_recordset()`
* Several fixes related to collation handling, including the parsing of collation-sensitive expressions in the arguments of a `CALL` statement
* Several fixes for the query planner, including an improvement to planning speed for large inheritance or partitioning table groups
* Several fixes for `TRUNCATE`
* Ensure `ALTER TABLE ONLY ADD COLUMN IF NOT EXISTS` is processed correctly
* Allow `UNLISTEN` in hot-standby (replica) mode
* Fix parsing of space-separated lists of host names in the ldapserver parameter of LDAP authentication entries in `pg_hba.conf`
* Several fixes for ecpg
* Several fixes for psql, including having `\g target` work with `COPY TO STDOUT`
* The random number generation for `pgbench` is now fully deterministic and platform-independent when `--random-seed=N` is specified
* Fix `pg_basebackup` and `pg_verify_checksums` to appropriately ignore temporary files
* Several fixes for `pg_dump`, including having `ALTER INDEX SET STATISTICS` commands present
* Prevent false index-corruption reports from contrib/amcheck caused by inline-compressed data
* Support new Makefile variables to help with building extensions

This update also contains tzdata release 2018i for DST law changes in Kazakhstan, Metlakatla, and Sao Tome and Principe. Kazakhstan's Qyzylorda zone is split in two, creating a new zone Asia/Qostanay, as some areas did not change UTC offset.  Historical corrections for Hong Kong and numerous Pacific islands.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use `pg_upgrade` in order to apply this update release; you may simply shutdown PostgreSQL and update its binaries.

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

PostgreSQL 9.4 will stop receiving fixes on February 13, 2020. Please see our [versioning policy](https://www.postgresql.org/support/versioning/) for more information.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/11/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
