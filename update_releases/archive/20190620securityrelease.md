2019-06-20 Cumulative Security Update
=====================================

The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 11.4, 10.9, 9.6.14, 9.5.18, and
9.4.23, as well as the second beta of PostgreSQL 12. This release fixes one
security issue and over 25 bugs since the previous cumulative update in May.

This release is made outside of the normal update release schedule as the
security vulnerability was determined to be critical enough to distribute the
fix as quickly as possible. **Users who are running PostgreSQL 10,
PostgreSQL 11, or the PostgreSQL 12 beta should upgrade as soon as possible.**

All other users should plan to apply this update at the next scheduled downtime.

Security Issues
---------------

This release closes one security vulnerability:

* CVE-2019-10164: Stack-based buffer overflow via setting a password

Versions affected: 10, 11, 12 beta.

An authenticated user could create a stack-based buffer overflow by changing
their own password to a purpose-crafted value. In addition to the ability to
crash the PostgreSQL server, this could be further exploited to execute
arbitrary code as the PostgreSQL operating system account.

Additionally, a rogue server could send a specifically crafted message during
the SCRAM authentication process and cause a libpq-enabled client to either
crash or execute arbitrary code as the client's operating system account.

This issue is fixed by upgrading and restarting your PostgreSQL server as well
as your libpq installations. **All users running PostgreSQL 10, 11, and 12 beta
are encouraged to upgrade as soon as possible.**

The PostgreSQL Project thanks Alexander Lakhin for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update also fixes over 25 bugs since the previous cumulative update in May.
Some of these issues affect only version 11, but many affect all supported
versions.

Some of these fixes include:

* Fix assorted errors in run-time partition pruning that could lead to wrong
answers in queries on partitioned tables
* `pg_dump` now recreates table partitions using `CREATE TABLE` and
`ALTER TABLE .. ATTACH PARTITION` rather than including `PARTITION OF` in the
creation command
* Improve how `initdb` determines which system time zone to select if there are
equivalent names for the time zone. Also explicitly prefer `UTC` over `UCT`
* Fix possible crash while trying to copy trigger definitions to a new partition
* Fix failure of `ALTER TABLE .. ALTER COLUMN TYPE` when the table has a partial
exclusion constraint
* Fix failure of `COMMENT` command for comments on domains
* Several fixes related to aggregation
* Fix faulty generation of merge-append plans that could lead to "could not find
pathkey item to sort" errors
* Fix failures on dump/restore where views contained queries with duplicate join
names
* Fix conversion of JSON string literals to JSON-type output columns in
`json_to_record()` and `json_populate_record()`
* Fix incorrect optimization of `{1,1}` quantifiers in regular expressions
* Fix issue for B-tree indexes during edge case failure involving columns
covered with the `INCLUDE` clause, which manifests itself with errors during
VACUUM. If you are affected by this issue, you will need to reindex the specific
index
* Fix race condition in check to see whether a pre-existing shared memory
segment is still in use by a conflicting postmaster
* Fix for the walreceiver process that avoids a crash or deadlock on shutdown
* Avoid possible hang in libpq if using SSL and OpenSSL's pending-data buffer
contains an exact multiple of 256 bytes
* Fix ordering of GRANT commands emitted by `pg_dump` and `pg_dumpall` for
databases and tablespaces
* Fix misleading error reports from `reindexdb`
* Ensure that `vacuumdb` returns correct status if an error occurs while using
parallel jobs
* Fix `contrib/auto_explain` to not cause problems in parallel queries, which
resulted in failures like "could not find key N in shm TOC"
* Account for possible data modifications by local `BEFORE ROW UPDATE` triggers
in `contrib/postgres_fdw`
* On Windows, avoid failure when the database encoding is set to `SQL_ASCII` and
we attempt to log a non-ASCII string

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

If one of your B-tree indexes that uses an `INCLUDE` clause is affected by the
issue mentioned above, you will need to reindex any affected indexes. The issue
manifests itself based on errors that occur during a VACUUM. You can read more
about reindexing here:

[https://www.postgresql.org/docs/current/sql-reindex.html](https://www.postgresql.org/docs/current/sql-reindex.html)

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

PostgreSQL 9.4 will stop receiving fixes on February 13, 2020. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Beta Schedule
-------------

This includes the second beta release of version 12. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2019. For further
information please see the
[Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/11/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 12 Beta Release Notes](https://www.postgresql.org/docs/devel/release-12.html)
* [PostgreSQL 12 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_12_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
