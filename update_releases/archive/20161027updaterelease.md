2016-10-27 Cumulative Update Release
====================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.6.1, 9.5.5, 9.4.10, 9.3.15, 9.2.19, and 9.1.24.  This is also the last update for the PostgreSQL 9.1 series as it is now end-of-life. This release fixes two issues that can cause data corruption, which are described in more detail below. It also patches a number of other bugs reported over the last three months. The project urges users to apply this update at the next possible downtime.

WAL-logging of truncated relations
----------------------------------

Prior to this release, there was a chance that a PostgreSQL instance would try to access data that no longer existed on disk.  If the free space map was not updated to be aware of the truncation, a PostgreSQL database could return a page that was already truncated and produce an error such as:

    ERROR:  could not read block 28991 in file "base/16390/572026": read only 0 of 8192 bytes

If checksumming is enabled, checksum failures in the visibility map could also occur.

This issue is present in the 9.3, 9.4, 9.5, and 9.6 series of PostgreSQL releases.

pg_upgrade issues on big-endian machines
----------------------------------

On big-endian machines (e.g. many non-Intel CPU architectures), pg_upgrade would incorrectly write the bytes of the visibility map leading to pg_upgrade failing to complete.

If you are using a big-endian machine (many non-Intel architectures are big-endian) and have used pg_upgrade to upgrade from a pre-9.6 release, you should assume that all visibility maps are incorrect and need to be regenerated. It is sufficient to truncate each relation's visibility map with contrib/pg_visibility's pg_truncate_visibility_map() function.  Please read the "Updating" section below for post-installation instructions on how to resolve this issue on your PostgreSQL instances.

This issue is present only in the PostgreSQL 9.6.0 release.

Bug Fixes and Improvements
--------------------------

In addition to the above, this update also fixes a number of bugs reported in the last few months. Some of these issues affect only the 9.6 series, but many affect all supported versions.  There are more than 50 fixes provided in this release, including:

* Fix use-after-free hazard in execution of aggregate functions using DISTINCT, which could lead to crashes
* Fix incorrect handling of polymorphic aggregates used as window functions, which could lead to crashes
* Fix incorrect creation of GIN index WAL records on big-endian machines
* Fix file descriptor leakage when truncating a temporary relation of more than 1GB
* Fix query-lifespan memory leak in a bulk UPDATE on a table with a PRIMARY KEY or REPLICA IDENTITY index
* Fix SELECT FOR UPDATE/SHARE to correctly lock tuples that have been updated by a subsequently-aborted transaction
* Fix COPY with a column name list from a table that has row-level security enabled
* Fix deletion of speculatively inserted TOAST tuples when backing out of INSERT ... ON CONFLICT
* Fix timeout length when VACUUM is waiting for exclusive table lock so that it can truncate the table
* Fix bugs in merging inherited CHECK constraints while creating or altering a table
* Fix replacement of array elements in jsonb_set()
* Fix possible sorting error when aborting use of abbreviated keys in btree indexes
* On Windows, retry creation of the dynamic shared memory control segment after an access-denied error
* Fix pgbench's calculation of average latency
* Make pg_receivexlog work correctly with --synchronous without slots
* Make pg_rewind turn off synchronous_commit in its session on the source servere
* Don't try to share SSL contexts across multiple connections in libpq
* Support OpenSSL 1.1.0
* Install TAP test infrastructure so that it's available for extension testing
* Several fixes for logical WAL decoding and replication slots
* Several fixes for minor issues in pg_dump, pg_xlogdump, and pg_upgrade
* Several fixes for minor issues in the query planner and in the output of EXPLAIN
* Several fixes for timezone support

This update also contains tzdata release 2016h for DST law changes in Palestine and Turkey, plus historical corrections for Turkey and some regions of Russia. Switch to numeric abbreviations for some time zones in Antarctica, the former Soviet Union, and Sri Lanka.

The IANA time zone database previously provided textual abbreviations for all time zones, sometimes making up abbreviations that have little or no currency among the local population. They are in process of reversing that policy in favor of using numeric UTC offsets in zones where there is no evidence of real-world use of an English abbreviation. At least for the time being, PostgreSQL will continue to accept such removed abbreviations for timestamp input. But they will not be shown in the pg_timezone_names view nor used for output.

In this update, AMT is no longer shown as being in use to mean Armenia Time. Therefore, we have changed the Default abbreviation set to interpret it as Amazon Time, thus UTC-4 not UTC+4.

EOL Notice for Version 9.1
--------------------------

PostgreSQL version 9.1 is now End-of-Life (EOL). No additional updates or security patches will be released by the community for this version. Users still on 9.1 are urged to upgrade as soon as possible. See our [Versioning Policy](https://www.postgresql.org/support/versioning/) for more information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.

If your system was affected by the big-endian pg_upgrade bug, please read [Visibility Map Problems](https://wiki.postgresql.org/wiki/Visibility_Map_Problems) and follow the instructions on how to fix your this issue on your PostgreSQL instances.

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Links:
* [Download](http://www.postgresql.org/download)
* [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
* [Security Page](http://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
