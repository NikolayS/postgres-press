2016-08-11 Security Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 9.5.4, 9.4.9, 9.3.14, 9.2.18 and 9.1.23. This release fixes two security issues. It also patches a number of other bugs reported over the last three months.  Users who rely on security isolation between database users should update as soon as possible.  Other users should plan to update at the next convenient
downtime.

Security Issues
---------------

Two security holes have been closed by this release:

* [CVE-2016-5423](https://access.redhat.com/security/cve/CVE-2016-5423): certain nested CASE expressions can cause the server to crash.
* [CVE-2016-5424](https://access.redhat.com/security/cve/CVE-2016-5424): database and role names with embedded special characters
  can allow code injection during administrative operations like pg_dumpall.

The fix for the second issue also adds an option, -reuse-previous, to
psql's \connect command.  pg_dumpall will also refuse to handle database and
role names containing line breaks after the update.  For more information on
these issues and how they affect backwards-compatibility, see the Release Notes.

Bug Fixes and Improvements
--------------------------

This update also fixes a number of bugs reported in the last few months. Some
of these issues affect only version 9.5, but many affect all supported versions:

* Fix misbehaviors of IS NULL/IS NOT NULL with composite values
* Fix three areas where INSERT ... ON CONFLICT failed to work properly
  with other SQL features.
* Make INET and CIDR data types properly reject bad IPv6 values
* Prevent crash in "point ## lseg" operator for NaN input
* Avoid possible crash in pg_get_expr()
* Fix several one-byte buffer over-reads in to_number()
* Don't needlessly plan query if WITH NO DATA is specified
* Avoid crash-unsafe state in expensive heap_update() paths
* Fix hint bit update during WAL replay of row locking operations
* Avoid unnecessary "could not serialize access" with FOR KEY SHARE
* Avoid crash in postgres -C when the specified variable is a null string
* Fix two issues with logical decoding and subtransactions
* Ensure that backends see up-to-date statistics for shared catalogs
* Prevent possible failure when vacuuming multixact IDs in an upgraded database
* When a manual ANALYZE specifies columns, don't reset changes_since_analyze
* Fix ANALYZE's overestimation of n_distinct for columns with nulls
* Fix bug in b-tree mark/restore processing
* Fix building of large (bigger than shared_buffers) hash indexes
* Prevent infinite loop in GiST index build with NaN values
* Fix possible crash during a nearest-neighbor indexscan
* Fix "PANIC: failed to add BRIN tuple" error
* Prevent possible crash during background worker shutdown
* Many fixes for issues in parallel pg_dump and pg_restore
* Make pg_basebackup accept -Z 0 as no compression
* Make regression tests safe for Danish and Welsh locales

The libpq client library has also been updated to support future two-part
PostgreSQL version numbers. This update also contains tzdata release 2016f,
with updates for Kemerovo, Novosibirsk, Azerbaijan, Belarus, and Morocco.

EOL Warning for Version 9.1
--------------------------

PostgreSQL version 9.1 will be End-of-Life in September 2016.  The project expects to only release one more update for that version.  We urge users to start
planning an upgrade to a later version of PostgreSQL as soon as possible. See our
Versioning Policy for more information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.  Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Links:
* [Download](http://www.postgresql.org/download)
* [Release Notes](http://www.postgresql.org/docs/current/static/release.html)
* [Security Page](http://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
