The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 14.1, 13.5, 12.9, 11.14, 10.19, and
9.6.24. This release closes two security vulnerabilities and fixes over 40 bugs
reported over the last three months.

Additionally, this is the **final release of PostgreSQL 9.6**. If you are
running PostgreSQL 9.6 in a production environment, we suggest that you make
plans to upgrade.

For the full list of changes, please review the
[release notes](https://www.postgresql.org/docs/release/).

Security Issues
---------------

### [CVE-2021-23214](https://www.postgresql.org/support/security/CVE-2021-23214/): Server processes unencrypted bytes from man-in-the-middle

Versions Affected: 9.6 - 14.  The security team typically does not test
unsupported versions, but this problem is quite old.

When the server is configured to use `trust` authentication with a `clientcert`
requirement or to use `cert` authentication, a man-in-the-middle attacker can
inject arbitrary SQL queries when a connection is first established, despite the
use of SSL certificate verification and encryption.

The PostgreSQL project thanks Jacob Champion for reporting this problem.

### [CVE-2021-23222](https://www.postgresql.org/support/security/CVE-2021-23222/): libpq processes unencrypted bytes from man-in-the-middle

Versions Affected: 9.6 - 14.  The security team typically does not test
unsupported versions, but this problem is quite old.

A man-in-the-middle attacker can inject false responses to the client's first
few queries, despite the use of SSL certificate verification and encryption.

If more preconditions hold, the attacker can exfiltrate the client's password
or other confidential data that might be transmitted early in a session.  The
attacker must have a way to trick the client's intended server into making the
confidential data accessible to the attacker.  A known implementation having
that property is a PostgreSQL configuration vulnerable to [CVE-2021-23214](https://www.postgresql.org/support/security/CVE-2021-23214/).

As with any exploitation of [CVE-2021-23214](https://www.postgresql.org/support/security/CVE-2021-23214/), the server must be using `trust`
authentication with a `clientcert` requirement or using `cert` authentication.
To disclose a password, the client must be in possession of a password, which is
atypical when using an authentication configuration vulnerable to
[CVE-2021-23214](https://www.postgresql.org/support/security/CVE-2021-23214/). The attacker must have some other way to access the server to
retrieve the exfiltrated data (a valid, unprivileged login account would be
sufficient).

The PostgreSQL project thanks Jacob Champion for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update fixes over 40 bugs that were reported in the last several months.
The issues listed below affect PostgreSQL 14. Some of these issues may also
affect other supported versions of PostgreSQL.

Some of these fixes include:

* Fix physical replication for cases where the primary crashes after shipping a
WAL segment that ends with a partial WAL record. When applying this update,
update your standby servers before the primary so that they will be ready to
handle the fix if the primary happens to crash.
* Fix parallel `VACUUM` so that it will process indexes below the
[`min_parallel_index_scan_size`](https://www.postgresql.org/docs/14/runtime-config-query.html#GUC-MIN-PARALLEL-INDEX-SCAN-SIZE)
threshold if the table has at least two indexes that are above that size. This
problem does not affect autovacuum. If you are affected by this issue, you
should reindex any manually-vacuumed tables.
* Fix causes of `CREATE INDEX CONCURRENTLY` and `REINDEX CONCURRENTLY` writing
corrupt indexes.  You should reindex any concurrently-built indexes.
* Fix for attaching/detaching a partition that could allow certain
`INSERT`/`UPDATE` queries to misbehave in active sessions.
* Fix for creating a new range type with `CREATE TYPE` that could cause
problems for later event triggers or subsequent executions of the `CREATE TYPE`
command.
* Fix updates of element fields in arrays of a domain that is a part of a
composite.
* Disallow the combination of `FETCH FIRST WITH TIES` and
`FOR UPDATE SKIP LOCKED`.
* Fix corner-case loss of precision in the numeric `power()` function.
* Fix restoration of a Portal's snapshot inside a subtransaction, which could
lead to a crash. For example, this could occur in PL/pgSQL when a `COMMIT` is
immediately followed by a `BEGIN ... EXCEPTION` block that performs a query.
* Clean up correctly if a transaction fails after exporting its snapshot. This
could occur if a replication slot was created then rolled back, and then another
replication slot was created in the same session.
* Fix for "overflowed-subtransaction" wraparound tracking on standby servers
that could lead to performance degradation.
* Ensure that prepared transactions are properly accounted for during promotion
of a standby server.
* Ensure that the correct lock level is used when renaming a table.
* Avoid crash when dropping a role that owns objects being dropped concurrently.
* Disallow setting `huge_pages` to on when `shared_memory_type` is `sysv`
* Fix query type checking in the PL/pgSQL `RETURN QUERY`.
* Several fixes for `pg_dump`, including the ability to dump non-global default
privileges correctly.
* Use the CLDR project's data to map Windows time zone names to IANA time zones.

This update also contains tzdata release 2021e for DST law changes in Fiji,
Jordan, Palestine, and Samoa, plus historical corrections for Barbados,
Cook Islands, Guyana, Niue, Portugal, and Tonga.

Also, the Pacific/Enderbury zone has been renamed to Pacific/Kanton. Also, the
following zones have been merged into nearby, more-populous zones whose clocks
have agreed with them since 1970: Africa/Accra, America/Atikokan,
America/Blanc-Sablon, America/Creston, America/Curacao, America/Nassau,
America/Port_of_Spain, Antarctica/DumontDUrville, and Antarctica/Syowa. In all
these cases, the previous zone name remains as an alias.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/release/).

PostgreSQL 9.6 is EOL
---------------------

This is the final release of PostgreSQL 9.6. If you are running PostgreSQL 9.6
in a production environment, we suggest that you make plans to upgrade to a
newer, supported version of PostgreSQL. Please see our
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

For more details, please see the
[release notes](https://www.postgresql.org/docs/release/).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/release/)
* [Security](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
