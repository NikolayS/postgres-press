2018-02-08 Security Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 10.2, 9.6.7, 9.5.11, 9.4.16, 9.3.21. This release fixes two security issues. This release also fixes issues with VACUUM, GIN indexes, and hash indexes that could lead to data corruption, as well as fixes for using parallel queries and logical replication.

All users using the affected versions of PostgreSQL should update as soon as possible. Please see the notes on "Updating" below for any post-update steps that may be required.

Please note that PostgreSQL changed its versioning scheme with the release of version 10.0, so updating to version 10.2 from 10.0 or 10.1 is considered a minor update.

Security Issues
---------------

Two security vulnerabilities have been fixed by this release:

* [CVE-2018-1052](https://access.redhat.com/security/cve/CVE-2018-1052): Fix the processing of partition keys containing multiple expressions
* [CVE-2018-1053](https://access.redhat.com/security/cve/CVE-2018-1053): Ensure that all temporary files made with "pg\_upgrade" are non-world-readable

Bug Fixes and Improvements
--------------------------

This update fixes over 60 bugs reported in the last few months. Some of these issues affect only version 10, but many affect all supported versions:

* Fix crash and potential disclosure of backend memory when processing partition keys containing multiple expressions
* Fix potential disclosure of temporary files containing database passwords created by pg\_upgrade by not allowing these files to be world-accessible
* Fix cases where VACUUM would not remove dead rows if they were updated while "key-share" locked, leading to potential data corruption
* Fix for GIN indexes to prevent bloat by ensuring the pending-insertions list is cleaned up by VACUUM
* Fix potential index corruption with hash indexes due to failure to mark metapages as dirty
* Fix several potential crash scenarios for parallel queries, including when a bitmap heap scan cannot allocate memory
* Fix several potential hang-ups in parallel queries, including when a parallel worker fails to start
* Fix collection of EXPLAIN statistics from parallel workers
* Prevent fake deadlock failures when multiple sessions are running CREATE INDEX CONCURRENTLY
* Fix for trigger behavior when using logical replication
* Several fixes for "walsender" functionality to improve stability as well as visibility into the replication process
* Fix logical decoding to correctly clean up disk files for crashed transactions
* Several fixes for identity columns, including disallowing identity columns on tables derived from composite types and partitions
* Fix handling of list partitioning constraints for partition keys of boolean and array types
* Fix incorrectly generated plans for UPDATE and DELETE queries when a table has a mix of inherited regular and foreign child tables
* Fix incorrect query results from cases involving GROUPING SETS when used with flattened subqueries
* Fix UNION/INTERSECT/EXCEPT over zero columns, e.g. "SELECT UNION SELECT;"
* Several fixes for subqueries within a LATERAL subquery
* Several improvements for query planning estimation
* Allow a client that supports SCRAM channel binding, such as a future version of PostgreSQL or libpq, to connect to a PostgreSQL 10 server
* Fix sample INSTR() functions used to help transition from Oracle(r) PL/SQL to PostgreSQL PL/pgSQL to correctly match Oracle functional behavior
* Fix pg\_dump to make permissions (ACL), security label, and comment entries reliably identifiable in archive outputs
* Modify behavior for contrib/cube's "cube ~> int" operator to make it compatible with KNN search. This is a backwards incompatible change and any expression indexes or materialized views using this operator will need to be reindexed and refreshed, respectively.
* Several fixes in contrib/postgres\_fdw to prevent query planner errors
* Added modern examples of auto-start scripts for PostgreSQL on macOS in the contrib/start-scripts/macos directory
* Several fixes for Windows, including postmaster startup and compatibility with libperl
* Spinlock fixes and support for Motorola 68K and 88K architectures

This update also contains tzdata release 2018c, with updates for DST law changes in Brazil, Sao Tome and Principe, plus historical corrections for Bolivia, Japan, and South Sudan. The US/Pacific-New zone has been removed (it was only an alias for "America/Los_Angeles" anyway).

Updating
--------
All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg\_upgrade in order to apply this update release; you may simply shutdown PostgreSQL and update its binaries.

If your installation is affected by one of the following issues, you may need to perform additional post-update steps:

* Users affected by the GIN and hash index issues should consider rebuilding these indexes
* Users who copied the "[INSTR](https://www.postgresql.org/docs/current/static/plpgsql-porting.html#PLPGSQL-PORTING-APPENDIX)" example from the PostgreSQL documentation should analyze their code to determine if they need to apply the corrected "[INSTR](https://www.postgresql.org/docs/current/static/plpgsql-porting.html#PLPGSQL-PORTING-APPENDIX)" example
* Users who use the "~>" operator found in "contrib/cube" with expression indexes or materialized views will need to reindex and refresh them, respectively. This change is also backwards incompatible, so please test any code using this operator before releasing to a production environment.

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Links
------
* [Download](https://www.postgresql.org/download)
* [Release Notes](https://www.postgresql.org/docs/current/static/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
