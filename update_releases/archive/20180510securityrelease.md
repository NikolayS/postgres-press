2018-05-10 Security Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including 10.4, 9.6.9, 9.5.13, 9.4.18, 9.3.23. This release fixes one security issue as well as several bugs reported over the last three months.  Users should plan to update at the next convenient downtime.

Please see the "Updating" section for post-installation steps for the security fix and the "incorrect volatility and parallel-safety markings" fix.

Please note that PostgreSQL changed its versioning scheme with the release of version 10.0, so updating to version 10.4 from 10.0, 10.1, 10.2, or 10.3 is considered a minor update.

Security Issues
---------------

One security vulnerability has been closed by this release:

* [CVE-2018-1115](https://access.redhat.com/security/cve/CVE-2018-1115): Too-permissive access control list on function pg\_logfile\_rotate()

Please see the "Updating" section below for post-update steps.

Bug Fixes and Improvements
--------------------------

This update also fixes over 50 bugs reported in the last several months. Some of these issues affect only version 10, but many affect all supported versions.  These fixes include:

* Fix incorrect volatility and parallel-safety markings on several built-in functions to ensure correct query planning optimizations
* Several fixes for partitioning, including potential crashes as well as allowing TRUE and FALSE to be used as partition bounds
* Fix where a new TOAST value could be assigned to a dead-but-not-yet-vacuumed TOAST OID, which would result in an error similar to "unexpected chunk number 0 (expected 1) for toast value nnnnn"
* Fix "CREATE TABLE ... LIKE" with bigint identity columns on 32-bit platforms
* Fix memory leak within the runtime of a query that repeatedly executes hash joins
* Several crash fixes around queries using GROUPING SET
* Avoid failure if a query-cancel or session-termination interrupt occurs while committing a prepared transaction
* Reduce locking during autovacuum worker scheduling, which prevents loss of potential worker concurrency
* Fix possible slow execution of REFRESH MATERIALIZED VIEW CONCURRENTLY
* Several fixes around query plans that use "index-only" scans
* Avoid deadlocks in concurrent CREATE INDEX CONCURRENTLY commands that are run under SERIALIZABLE or REPEATABLE READ transaction isolation
* Several fixes for SP-GiST indexes, including one collation-aware searches on text columns
* Fixes related to counting the number of tuples in partial GiST, SP-GiST, and Bloom indexes
* Several fixes for logical decoding and replication
* Fix misquoting of values for list-valued GUC variables (e.g. local\_preload\_libraries, session\_preload\_libraries, shared\_preload\_libraries, temp\_tablespaces) in dumps
* Several fixes for pg\_stat\_activity
* Several fixes for ecpg
* Fix for pg\_recvlogical to ensure compatibility with PostgreSQL versions released before 10
* Several fixes for pg\_rewind

This update also contains tzdata release 2018d, with updates for Palestine and Antarctica (Casey Station), plus historical corrections for Portugal and its colonies, as well as Enderbury, Jamaica, Turks & Caicos Islands, and Uruguay.

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg\_upgrade to apply this update release (though for this release, please see the note about "volatility and parallel-safety markings"); you may simply shut down PostgreSQL and update its binaries.

Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.

Post-Installation Steps for CVE-2018-1115
-----------------------------------------

**If you installed "adminpack" in any PostgreSQL 9.6 or 10 release** you will need to have your database administrator run the following command in every database that "adminpack" is installed in:

    ALTER EXTENSION adminpack UPDATE;

Post-Installation Steps for Function Markings
---------------------------------------------

**Functions that should be marked "volatile"**

* cursor\_to\_xml
* cursor\_to\_xmlschema
* query\_to\_xml
* query\_to\_xml\_and\_xmlschema
* query\_to\_xmlschema

**Functions that should be marked "parallel-unsafe"**

* binary\_upgrade\_create\_empty\_extension,
* brin\_desummarize\_range
* brin\_summarize\_new\_values
* brin\_summarize\_range
* cursor\_to\_xml
* cursor\_to\_xmlschema
* gin\_clean\_pending\_list
* pg\_import\_system\_collations
* ts\_rewrite
* ts\_stat

If you use any of the above functions, you can update the markings in the following ways:

1\. Update the markings manually in the "pg\_proc" table in each database you use these functions. You can achieve this by executing the following commands as a database superuser:

```
/* Functions that should be marked "volatile" */
ALTER FUNCTION pg_catalog.cursor_to_xml(refcursor, int, boolean, boolean, text) VOLATILE;
ALTER FUNCTION pg_catalog.cursor_to_xmlschema(refcursor, boolean, boolean, text) VOLATILE;
ALTER FUNCTION pg_catalog.query_to_xml(text, boolean, boolean, text) VOLATILE;
ALTER FUNCTION pg_catalog.query_to_xml_and_xmlschema(text, boolean, boolean, text) VOLATILE;
ALTER FUNCTION pg_catalog.query_to_xmlschema(text, boolean, boolean, text) VOLATILE;
/* Functions that should be marked "parallel-unsafe" */
ALTER FUNCTION pg_catalog.binary_upgrade_create_empty_extension(text, text, bool, text, _oid, _text, _text) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.brin_desummarize_range(regclass, bigint) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.brin_summarize_new_values(regclass) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.brin_summarize_range(regclass, bigint) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.cursor_to_xml(refcursor, int, boolean, boolean, text) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.cursor_to_xmlschema(refcursor, boolean, boolean, text) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.gin_clean_pending_list(regclass) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.pg_import_system_collations(regnamespace) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.ts_rewrite(tsquery, text) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.ts_rewrite(tsquery, tsquery, tsquery) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.ts_stat(text) PARALLEL UNSAFE;
ALTER FUNCTION pg_catalog.ts_stat(text, text) PARALLEL UNSAFE;
```

2\. Run a pg\_upgrade to a version containing the correct marking data (e.g. 10.4 and above)

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/current/static/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
