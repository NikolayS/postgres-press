2020-02-13 Cumulative Security Update
====================================

The PostgreSQL Global Development Group has released an update to all supported
versions of our database system, including 12.2, 11.7, 10.12, 9.6.17, 9.5.21,
and 9.4.26. This release fixes one security issue found in the PostgreSQL
server and over 75 bugs reported over the last three months.

Users should plan to update as soon as possible.

PostgreSQL 9.4 Now EOL
----------------------

This is the last release for PostgreSQL 9.4, which will no longer receive
security updates and bug fixes. [PostgreSQL 9.4 introduced new features](https://www.postgresql.org/about/news/1557/)
such as JSONB support, the `ALTER SYSTEM` command, the ability to stream logical
changes to an output plugin, [and more](https://www.postgresql.org/docs/9.4/release-9-4.html).

While we are very proud of this release, these features are also found in newer
versions of PostgreSQL. Many of these features have also received improvements,
and, per our [versioning policy](https://www.postgresql.org/support/versioning/),
it is time to retire PostgreSQL 9.4.

To receive continued support, we suggest that you make plans to upgrade to a
newer, supported version of PostgreSQL. Please see the PostgreSQL
[versioning policy](https://www.postgresql.org/support/versioning/) for more
information.

Security Issues
---------------

* CVE-2020-1720: `ALTER ... DEPENDS ON EXTENSION` is missing authorization
checks.

Versions Affected: 9.6 - 12

The `ALTER ... DEPENDS ON EXTENSION` sub-commands do not perform authorization
checks, which can allow an unprivileged user to  drop any function, procedure,
materialized view, index, or trigger under certain conditions. This attack is
possible if an administrator has installed an extension and an unprivileged
user can `CREATE`, or an extension owner either executes `DROP EXTENSION`
predictably or can be convinced to execute `DROP EXTENSION`.

The PostgreSQL project thanks Tom Lane for reporting this problem.

Bug Fixes and Improvements
--------------------------

This update also fixes over 75 bugs that were reported in the last several
months. Some of these issues affect only version 12, but may also affect all
supported versions.

Some of these fixes include:

* Fix for partitioned tables with foreign-key references where
`TRUNCATE ... CASCADE` would not remove all data. If you have previously used
`TRUNCATE ... CASCADE` on a partitioned table with foreign-key references
please see the "Updating" section for verification and cleanup steps.
* Fix failure to add foreign key constraints to table with sub-partitions (aka a
multi-level partitioned table). If you have previously used this functionality,
you can fix it by either detaching and re-attaching the affected partition, or
by dropping and re-adding the foreign key constraint to the parent table. You
can find more information on how to perform these steps in the
[ALTER TABLE](https://www.postgresql.org/docs/current/sql-altertable.html)
documentation.
* Fix performance issue for partitioned tables introduced by the fix for
CVE-2017-7484 that now allows the planner to use statistics on a child table for
a column that the user is granted access to on the parent table when the query
contains a leaky operator.
* Several other fixes and changes for partitioned tables, including disallowing
partition key expressions that return pseudo-types, such as `RECORD`.
* Fix for logical replication subscribers for executing per-column `UPDATE`
triggers.
* Fix for several crashes and failures for logical replication subscribers and
publishers.
* Improve efficiency of logical replication with `REPLICA IDENTITY FULL`.
* Ensure that calling `pg_replication_slot_advance()` on a physical replication
slot will persist changes across restarts.
* Several fixes for the walsender processes.
* Improve performance of hash joins with very large inner relations.
* Fix placement of "Subplans Removed" field in EXPLAIN output by placing it with
its parent Append or MergeAppend plan.
* Several fixes for parallel query plans.
* Several fixes for query planner errors, including one that affected joins to
single-row subqueries.
* Several fixes for MCV extend statistics, including one for incorrect
estimation for OR clauses.
* Improve efficiency of parallel hash join on CPUs with many cores.
* Ignore the `CONCURRENTLY` option when performing an index creation, drop, or
reindex on a temporary table.
* Fall back to non-parallel index builds when a parallelized CREATE INDEX has no
free dynamic shared memory slots.
* Several fixes for GiST & GIN indexes.
* Fix possible crash in BRIN index operations with `box`, `range` and `inet`
data types.
* Fix support for BRIN hypothetical indexes.
* Fix failure in `ALTER TABLE` when a column referenced in a `GENERATED`
expression is added or changed in type earlier in the same `ALTER TABLE`
statement.
* Fix handling of multiple `AFTER ROW` triggers on a foreign table.
* Fix off-by-one result for `EXTRACT(ISOYEAR FROM timestamp)` for BC dates.
* Prevent unwanted lowercasing and truncation of RADIUS authentication
parameters in the `pg_hba.conf` file.
* Several fixes for GSSAPI support, including having libpq accept all
GSS-related connection parameters even if the GSSAPI code is not compiled in.
* Several fixes for `pg_dump` and `pg_restore` when run in parallel mode.
* Fix crash with `postgres_fdw` when trying to execute a remote query on the
remote server such as `UPDATE remote_tab SET (x,y) = (SELECT ...)`.
* Disallow NULL category values in the `crosstab()` function of
`contrib/tablefunc` to prevent crashes.
* Several fixes for Windows, including a race condition that could cause timing
oddities with `NOTIFY`.
* Several ecpg fixes.

For the full list of changes available, please review the
[release notes](https://www.postgresql.org/docs/current/release.html).

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases,
users are not required to dump and reload their database or use `pg_upgrade` in
order to apply this update release; you may simply shutdown PostgreSQL and
update its binaries.

Users who have skipped one or more update releases may need to run additional,
post-update steps; please see the release notes for earlier versions for
details.

If you had previously executed `TRUNCATE ... CASCADE` on a sub-partition of a
partitioned table, and the partitioned table has a foreign-key reference from
another table, you may have to execute the `TRUNCATE` on the other table, or
execute a `DELETE` if you have added rows since running `TRUNCATE ... CASCADE`.
The issue that caused this is fixed in this release, but you will have to
perform this step to ensure all of your data is cleaned up.

For more details, please see the
[release notes](https://www.postgresql.org/docs/current/release.html).

Links
-----
* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/current/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
