2018-03-01 Security Update Release
==================================
The PostgreSQL Global Development Group has released an update to all supported versions of the PostgreSQL database system, including 10.3, 9.6.8, 9.5.12, 9.4.17, 9.3.22.

The purpose of this release is to address CVE-2018-1058, which describes how a user can create like-named objects in different schemas that can change the behavior of other users' queries and cause unexpected or malicious behavior, also known as a "trojan-horse" attack. Most of this release centers around added documentation that describes the issue and how to take steps to mitigate the impact on PostgreSQL databases.

We strongly encourage all of our users to please visit [A Guide to CVE-2018-1058: Protect Your Search Path](https://wiki.postgresql.org/wiki/A_Guide_to_CVE-2018-1058:_Protect_Your_Search_Path) for a detailed explanation of CVE-2018-1058 and how to protect your PostgreSQL installations.

After evaluating the documentation for CVE-2018-1058, a database administrator may need to take follow up steps on their PostgreSQL installations to ensure they are protected from exploitation.

Security Issues
---------------

One security vulnerability is addressed in this release:

* CVE-2018-1058: Uncontrolled search path element in pg\_dump and other client applications

Please visit [A Guide to CVE-2018-1058: Protect Your Search Path](https://wiki.postgresql.org/wiki/A_Guide_to_CVE-2018-1058:_Protect_Your_Search_Path) for a full explanation of the CVE-2018-1058.

Bug Fixes and Improvements
--------------------------

This update fixes several bugs reported since the last cumulative update. Some of these issues affect only version 10, but many affect all supported versions.  These fixes include:

* Prevent logical replication from trying to replicate changes for unpublishable relations, such as materialized views and the "information\_schema" tables
* Fix for a common table expression (WITH clause) returning correct results when being referenced in a subplan where there are concurrent-update rechecks
* Fix for an unexpected query planner error in certain cases where there are overlapping merge join clauses in an OUTER JOIN.
* Fix for potential data corruption with materialized views after running pg\_upgrade. If receiving errors such as "could not access status of transaction" or "found xmin from before relfrozenxid" on materialized views, please use "REFRESH MATERIALIZED VIEW" without "CONCURRENTLY" to fix.
* Several fix for pg\_dump, including a fix to help with the future work of cross-table statistics
* Fix for reporting a PL/Python stack trace relative to inner PL/Python functions
* Allow contrib/auto\_explain to range up to INT\_MAX, which is about 24 days
* Mark assorted configuration variables as PGDLLIMPORT, to ease porting extension modules to Windows

Acknowledgements
----------------

The PostgreSQL Global Development Group would like to thank Arseniy Sharoglazov for reporting CVE-2018-1058 to the security team.

Links
------
* [A Guide to CVE-2018-1058: Protect Your Search Path](https://wiki.postgresql.org/wiki/A_Guide_to_CVE-2018-1058:_Protect_Your_Search_Path)
* [Download](https://www.postgresql.org/download)
* [Release Notes](https://www.postgresql.org/docs/current/static/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
