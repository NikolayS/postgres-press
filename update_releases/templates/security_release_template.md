{{ DATE_HERE }} Security Update Release
==================================

The PostgreSQL Global Development Group has released an update to all supported versions of our database system, including {{ LIST OF MINOR VERSIONS }}. This release fixes {{ NUMBER }} security issues. It also patches a number of other bugs reported over the last three months.  Users who {{ AFFECTED SECURITY CONDITION }} should update as soon as possible.  Other users should plan to update at the next convenient downtime.

Security Issues
---------------

{{ NUMBER }} security vulnerabilities have been closed by this release:

* [CVE-0000-0000](https://access.redhat.com/security/cve/CVE-0000-0000): {{ CVE_SUMMARY }}
* [CVE-0000-0000](https://access.redhat.com/security/cve/CVE-0000-0000): {{ CVE_SUMMARY }}

{{ EXTRA NOTES ON ISSUES AND BACKWARDS COMPATIBILITY }}

Bug Fixes and Improvements
--------------------------

This update also fixes a number of bugs reported in the last few months. Some
of these issues affect only version {{ CURRENT_VERSION }}, but many affect
all supported versions:

{{ LIST OF PATCHED ISSUES FROM GITLOG AND/OR RELEASE NOTES }}

{{ EXTRA NOTES ON SPECIAL ISSUE IF REQUIRED }}

This update also contains tzdata release {{ TZDATA_RELEASE }},
with updates for {{ LIST OF TIME ZONES }}.

{{ %IF 2 to 5 MONTHS FROM EOL }}
EOL Warning for Version {{ EOL VERSION }}
-----------------------------------------

PostgreSQL version {{ EOL VERSION }} will be End-of-Life in {{ MONTH YEAR }}.  The project expects to only release one more update for that version.  We urge users to start planning an upgrade to a later version of PostgreSQL as soon as possible. See our Versioning Policy for more information.

{{ %ENDIF }}

{{ %IF 0 to 1 MONTHS FROM EOL }}
EOL Notice for Version {{ EOL VERSION }}
-----------------------------------------

PostgreSQL version {{ EOL VERSION }} is now End-of-Life (EOL).  No additional updates or security patches
will be released by the community for this version.  Users still on {{ EOL VERSION }}
are urged to upgrade as soon as possible.  See our Versioning Policy for more information.

{{ %ENDIF }}

Updating
--------

All PostgreSQL update releases are cumulative. As with other minor releases, users are not required to dump and reload their database or use pg_upgrade in order to apply this update release; you may simply shut down PostgreSQL and update its binaries.
{{ %IF POST-UPDATE STEPS }}
After update, users will need to {{ BRIEF DESC OF UPDATE STEPS }}.  See the Release Notes for more details.
{{ %ELSE }}
Users who have skipped one or more update releases may need to run additional, post-update steps; please see the release notes for earlier versions for details.
{{ %ENDIF }}

Links:
* [Download](https://www.postgresql.org/download)
* [Release Notes](https://www.postgresql.org/docs/current/static/release.html)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
