PostgreSQL 13 Beta 1 Released
=============================

The PostgreSQL Global Development Group announces that the first beta release of
PostgreSQL 13 is now [available for download](https://www.postgresql.org/download/).
This release contains previews of all features that will be available in the
final release of PostgreSQL 13, though some details of the release could change
before then.

You can find information about all of the features and changes found in
PostgreSQL 13 in the [release notes](https://www.postgresql.org/docs/13/release-13.html):

  [https://www.postgresql.org/docs/13/release-13.html](https://www.postgresql.org/docs/13/release-13.html)

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 13 in your systems to help us eliminate
any bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 13 Beta 1 in your production environments, we encourage you to find
ways to run your typical application workloads against this beta release.

Your testing and feedback will help the community ensure that the PostgreSQL 13
release upholds our standards of providing a stable, reliable release of the
world's most advanced open source relational database. You can read more about
our [beta testing process](https://www.postgresql.org/developer/beta/) and how
you can contribute here:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

PostgreSQL 13 Feature Highlights
--------------------------------

### Functionality

There are many new features in PostgreSQL 13 that help improve the overall
performance of PostgreSQL while making it even easier to develop applications.

B-tree indexes, the standard index of PostgreSQL, received improvements for
how they handle duplicate data. These enhancements help to shrink index size and
improve lookup speed, particularly for indexes that contain repeated values.

PostgreSQL 13 adds incremental sorting, which accelerates sorting data when data
that is sorted from earlier parts of a query are already sorted. Additionally,
queries with OR clauses or IN/ANY constant lists can use extended statistics
(created via `CREATE STATISTICS`), which can lead to better planning and
performance gains. PostgreSQL 13 can now use disk storage for hash aggregation
(used as part of aggregate queries) with large aggregation sets.

There are more improvements added to PostgreSQL's partitioning functionality in
this release, including an increased number of cases where a join directly
between partitioned tables can occur, which can improve overall query execution
time. Partitioned tables now support `BEFORE` row-level triggers, and a
partitioned table can now be fully replicated via logical replication without
having to publish individual partitions.

PostgreSQL 13 brings more convenience to writing queries with features like
`FETCH FIRST WITH TIES`, which returns any additional rows that match the last
row. There is also the addition of the `.datetime()` function for jsonpath
queries, which will automatically convert a date-like or time-like string to the
appropriate PostgreSQL date/time datatype. It is also even easier now to
generate random UUIDs, as the `gen_random_uuid()` function can be used without
having to enable any extensions.

### Administration

One of the most anticipated features of PostgreSQL 13 is the ability for the
`VACUUM` command to process indexes in parallel. This functionality can be
accessed using the new `PARALLEL` option on the `VACUUM` command (or
`--parallel` on `vacuumdb`), which allows you to specify the number of parallel
workers to use for vacuuming indexes. Note that this does not work with the
`FULL` option.

The `reindexdb` command has also added parallelism with the new `--jobs` flag,
which lets you specify the number of concurrent sessions to use when reindexing
a database.

PostgreSQL 13 introduces the concept of a "trusted extension", which allows for
a superuser to specify extensions that a user can install in their database so
long as they have a `CREATE` privilege.

This release includes more ways to monitor activity within a PostgreSQL
database: PostgreSQL 13 can now track WAL usage statistics and the progress of
streaming base backups, and the progress of an `ANALYZE` command.
`pg_basebackup` can also generate a manifest that can be used to verify the
integrity of a backup using a new tool called `pg_verifybackup`. It is also now
possible to limit the amount of WAL space reserved by replication slots.

A new flag for `pg_dump`, `--include-foreign-data`, includes data from  servers
referenced by foreign data wrappers in the dump output.

The `pg_rewind` command also has improvements in PostgreSQL 13. In addition to
`pg_rewind` automatically performing crash recovery, you can now use it to
configure standby PostgreSQL instances using the `--write-recovery-conf` flag.
`pg_rewind` can also use the `restore_command` of the target instance to fetch
needed write-ahead logs.

### Security

PostgreSQL continues to improve on its security capabilities in this latest
release, introducing several features to help further deploy PostgreSQL safely.

`libpq`, the connection library that powers `psql` and many PostgreSQL
connection drivers, includes several new parameters to help secure connections.
PostgreSQL 13 introduces the `channel_binding` connection parameters, which
lets a client specify that they want to require the channel binding
functionality as part of SCRAM. Additionally, a client that is using a password
protected TLS certificate can now specify its password using the `sslpassword`
parameter. PostgreSQL 13 also adds support for DER encoded certificates.

The PostgreSQL foreign data wrapper (`postgres_fdw`) also received several
enhancements to how it can secure connections, including the ability to use
certificate-based authentication to connect to other PostgreSQL clusters.
Additionally, unprivileged accounts can now connect to another PostgreSQL
database via the `postgres_fdw` without using a password.

### Other Highlights

PostgreSQL 13 continues to improve operability on Windows, as now users who
run PostgreSQL on Windows now have the option to connect over UNIX domain
sockets.

The PostgreSQL 13 documentation adds a [glossary](https://www.postgresql.org/docs/13/glossary.html)
of terms to help people familiarize themselves with both PostgreSQL and general
database concepts. This coincides with a significant rework in the display of
functions and operators in tables, which helps to improve readability both on
the web and in the PDF documentation.

The `pgbench` utility, used for performance testing, now supports the ability to
partition its "accounts" table, making it easier to benchmark workloads that
contain partitions.

`psql` now includes the `\warn` command that is similar to the `\echo` command
in terms of outputting data, except `\warn` sends it to stderr. And in case you
need additional guidance on any of the PostgreSQL commands, the `--help` flag
now includes a link to [https://www.postgresql.org](https://www.postgresql.org).

Additional Features
-------------------

Many other new features and improvements have been added to PostgreSQL 13, some
of which may be as or more important to your use case than what is mentioned
above. Please see the [release notes](https://www.postgresql.org/docs/13/release-13.html)
for a complete list of new and changed features:

  [https://www.postgresql.org/docs/13/release-13.html](https://www.postgresql.org/docs/13/release-13.html)

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 13. As
this is a Beta, minor changes to database behaviors, feature details, and APIs
are still possible. Your feedback and testing will help determine the final
tweaks on the new features, so please test in the near future. The quality of
user testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the first beta release of version 13. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2020. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 13 Beta Release Notes](https://www.postgresql.org/docs/13/release-13.html)
* [PostgreSQL 13 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
