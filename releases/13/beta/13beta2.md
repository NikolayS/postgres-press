PostgreSQL 13 Beta 2 Released
=============================

The PostgreSQL Global Development Group announces that the second beta release
of PostgreSQL 13 is now [available for download](https://www.postgresql.org/download/).
This release contains previews of all features that will be available in the
final release of PostgreSQL 13, though some details of the release could change
before then.

You can find information about all of the features and changes found in
PostgreSQL 13 in the [release notes](https://www.postgresql.org/docs/13/release-13.html):

  [https://www.postgresql.org/docs/13/release-13.html](https://www.postgresql.org/docs/13/release-13.html)

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 13 in your systems to help us eliminate
any bugs or other issues that may exist. While we do not advise you to run
PostgreSQL 13 Beta 2 in your production environments, we encourage you to find
ways to run your typical application workloads against this beta release.

You can read more about our [beta testing process](https://www.postgresql.org/developer/beta/)
and how you can contribute here:

  [https://www.postgresql.org/developer/beta/](https://www.postgresql.org/developer/beta/)

Upgrading to PostgreSQL 13 Beta 2
---------------------------------

To upgrade to PostgreSQL 13 Beta 2 from Beta 1 or an earlier version of
PostgreSQL, you will need to use a strategy similar to upgrading between
major versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`).
For more information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/13/static/upgrading.html).

Changes Since Beta 1
--------------------

There have been many bug fixes for PostgreSQL 13 reported during the Beta 1
period and applied to the Beta 2 release. This includes:

- The `effective_io_concurrency` configuration parameter is now simpler to use.
To convert from the old value to the new value, you can use this formula:
`SELECT round(sum(OLD / n::float)) from generate_series(1, OLD) s(n);`.
- The `enable_hashagg_disk` configuration parameter is renamed to
`hashagg_avoid_disk_plan` and defaults to `off`.
- The `enable_groupingsets_hash_disk` configuration parameter has been removed.
- `EXPLAIN ANALYZE` output for parallel query plans that have hash aggregates
spill to disk now show max memory and disk usage per worker.
- Several additional fixes and improvements for the hash aggregation spilling to
disk feature.
- Fix a crash in the WAL sender when starting physical replication.

Please see the [release notes](https://www.postgresql.org/docs/13/release-13.html)
for a complete list of new and changed features:

  [https://www.postgresql.org/docs/13/release-13.html](https://www.postgresql.org/docs/13/release-13.html)

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the general availability of PostgreSQL 13. As
this is a Beta, changes to database behaviors, feature details, and APIs are
still possible. Your feedback and testing will help determine the final tweaks
on the new features, so please test in the near future. The quality of user
testing helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

  [https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the second beta release of version 13. The PostgreSQL Project will
release additional betas as required for testing, followed by one or more
release candidates, until the final release in late 2020. For further
information please see the [Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 13 Beta Release Notes](https://www.postgresql.org/docs/13/release-13.html)
* [PostgreSQL 13 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_13_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)
