Die PostgreSQL Global Development Group hat heute die Veröffentlichung von [PostgreSQL 13](https://www.postgresql.org/docs/13/release-13.html), der aktuellsten Version des weltweit [führenden Open-Source-SQL-Datenbanksystems](https://www.postgresql.org/), bekannt gegeben.

PostgreSQL 13 enthält wesentliche Verbesserungen bei der Indizierung, was großen Datenbanken zugute kommt, einschließlich Platzersparnis und Leistungsgewinnen für Indizes, schnellere Antwortzeiten für Abfragen, welche Aggregate oder Partitionen verwenden, sowie eine bessere Planung der Anfragen bei Verwendung erweiterter Statistiken.

Zusammen mit stark nachgefragten Features wie [paralleles Vacuum](https://www.postgresql.org/docs/13/sql-vacuum.html) und [inkrementelles Sortieren](https://www.postgresql.org/docs/13/runtime-config-query.html#GUC-ENABLE-INCREMENTAL-SORT), bietet PostgreSQL 13 ein besseres Datenmanagement für große und kleine Workloads. Das beinhaltet Optimierungen für die tägliche Administration, mehr Komfort für Applikationsentwickler, und Verbesserungen bei der Sicherheit.

"PostgreSQL 13 zeigt die Zusammenarbeit und das Engagement unserer globalen Community bei der Verbesserung der weltweit fortschrittlichsten relationalen Open Source Datenbank", sagt Peter Eisentraut, ein Mitglied des PostgreSQL-Kernteams. "Die Innovationen, die jede Version mit sich bringt, und die zusammen mit der bekannten Zuverlässigkeit und Stabilität einhergehen, sind der Grund, warum immer mehr Menschen PostgreSQL für ihre Zwecke verwenden."

[PostgreSQL](https://www.postgresql.org), ein innovatives Management System für Daten, bekannt für seine Robustheit und Zuverlässigkeit, profitiert von über 25 Jahren Open Source Entwicklung und einer [globalen Entwicklergemeinschaft](https://www.postgresql.org/community/), und hat sich zur bevorzugten Open Source Datenbank für Unternehmen jeder Größe entwickelt.

### Kontinuierliche Geschwindigkeitsverbesserungen

Aufbauend auf der Arbeit der vorherigen Version, kann PostgreSQL 13 nun effektiv [duplizierte Daten in B-Tree Indexes](https://www.postgresql.org/docs/13/btree-implementation.html#BTREE-DEDUPLICATION) verarbeiten. Der B-Tree Index ist der standardmäßig verwendete Datenbank-Index und wird am häufigsten eingesetzt. Dies reduziert den Gesamtspeicherplatz für B-Tree Indexes und verbessert allgemein die Geschwindigkeit von Abfragen.

PostgreSQL 13 führt eine inkrementelle Sortierung ein, bei der bereits sortierte Daten eines früheren Schrittes die Sortierung in späteren Ausführungsschritten beschleunigt. Zusätzlich kann PostgreSQL nun die [erweiterten Statistiken](https://www.postgresql.org/docs/13/sql-createstatistics.htm) nutzen (erreichbar über [`CREATE STATISTICS`](https://www.postgresql.org/docs/13/sql-createstatistics.html)). Dies erstellt verbesserte Abfragepläne für Abfragen mit `OR` - Formulierungen oder `IN`/`ANY` - Suchen über Listen.

In PostgreSQL 13 können mehr Abfragevarianten die [Aggregierungen](https://www.postgresql.org/docs/13/functions-aggregate.html) und [Sortierungs-Sets](https://www.postgresql.org/docs/13/queries-table-expressions.html#QUERIES-GROUPING-SETS) verwenden, da die Effizienz der Hash-Aggregierung in PostgreSQL verbessert wurde. Abfragen mit grossen Aggregaten müssen nun nicht mehr komplett in den RAM passen. Abfragen auf [partitionierte Tabellen](https://www.postgresql.org/docs/13/ddl-partitioning.html) laufen in der Regel schneller, da mehr Fälle vom Planer gar nicht erst in Betracht gezogen werden. Dies trifft zu wenn entweder zur Plan- oder zur Laufzeit festgestellt wird, dass die Partitionen keine relevanten Daten enthalten können. Partitionen können zudem direkt miteinander verbunden (ge-joined) werden, ohne den Umweg über die partitionierte Tabelle zu gehen. Dies spart erheblichen Aufwand beim Ausführen derartiger Abfragen.

### Administrative Optimierungen

[Vacuuming](https://www.postgresql.org/docs/13/routine-vacuuming.html) ist ein wesentlicher Bestandteil der PostgreSQL Administration. Es erlaubt der Datenbank Speicherplatz zurückzugeben, nachdem Datensätze geändert und gelöscht wurden. Dieser Prozess kann eine administrative Herausforderung sein, auch wenn in früheren PostgreSQL Versionen bereits viel Arbeit in die Verringerung des Vacuum Aufwands investiert wurde.

PostgreSQL 13 verbessert Vacuum mit der Einführung des [Parallelen Vacuum von Indexen](https://www.postgresql.org/docs/13/sql-vacuum.html). Dies bringt nicht nur generelle Geschwindigkeitsvorteile beim Vacuum, sondern Administratoren können mit dieser Verbesserung auch konfigurieren, dass die Anzahl der parallelen Prozesse an spezifische Auslastungen angepasst wird. Eine weitere Neuheit ist, dass Inserts nun auch den Vacuum Prozess auslösen können. Bisher konnte das nur durch Updates und Deletes automatisch ausgelöst werden.

In PostgreSQL 13 können [Replication Slots](https://www.postgresql.org/docs/13/warm-standby.html#STREAMING-REPLICATION-SLOTS) so eingestellt werden, dass nur eine [maximale Anzahl an WAL Segmenten](https://www.postgresql.org/docs/13/runtime-config-replication.html#GUC-MAX-SLOT-WAL-KEEP-SIZE) vorgehalten wird. So wird verhindert, dass der Speicherplatz auf einer Primär-Datenbank ausgeht. Replication Slots sorgen dafür dass Write-Ahead Logs (WAL) nicht gelöscht werden, bevor diese auf einem Replica angekommen sind.

Administratoren bekommen mit PostgreSQL 13 mehr Möglichkeiten die Aktivitäten in der Datenbank zu überwachen. Z. B. werden WAL Statistiken nun in der Ausgabe von `EXPLAIN` dargestellt, der Fortschritt von Base Backups kann überwacht werden und auch der Fortschritt von `ANALYZE` Kommandos kann abgefragt werden. Neu ist zudem, dass die Integrität von [`pg_basebackup` Backups](https://www.postgresql.org/docs/13/app-pgbasebackup.html) mit dem neuen Kommando [`pg_verifybackup`](https://www.postgresql.org/docs/13/app-pgverifybackup.html) sichergstellt werden kann.

### Vorteile für die Applikations-Entwicklung

PostgreSQL 13 vereinfacht die Arbeit mit PostgreSQL Datentypen die von verschiedenen Quellen kommen. Das neue Release kommt mit der [`datetime()`](https://www.postgresql.org/docs/13/functions-json.html#FUNCTIONS-SQLJSON-OP-TABLE) Funktion für SQL/JSON path. Diese konvertiert korrekte Zeitformate (z. B. ISO 8601 strings) zu nativen PostgreSQL Datentypen. Zusätzlich enhält PostgreSQL 13 eine Funktion [`gen_random_uuid()`](https://www.postgresql.org/docs/13/functions-uuid.html) um UUID v4 zu generieren. Die Installationen einer Erweiterung ist damit nicht mehr notwendig.

Das PostgreSQL Partitionierungs-System wird flexibler indem partitionierte Tabellen nun zu 100% die logische Replikation unterstützen (vorher mussten die einzelnen Partitionen logisch repliziert werden). BEFORE row-level Trigger werden nun auch für partitionierte Tabellen unterstützt.

Die [`FETCH FIRST`](https://www.postgresql.org/docs/13/sql-select.html#SQL-LIMIT) Syntax in PostgreSQL 13 wurde erweitert, so dass nun auch die `WITH TIES` Klausel verwendet werden kann. Wird dies angegeben, wird `WITH TIES` alle Zeilen beinhalten die eine Verbindung zur letzten Zeile des Resultats haben (basierend auf der Sortierung des ORDER BY).

### Verbesserungen bei der Sicherheit

Das PostgreSQL Erweiterungs-System ist ein zentraler Bestandteil von PostgreSQL, der es Entwicklern erlaubt die Funktionalität von PostgreSQL zu erweitern. In vorherigen Versionen von PostgreSQL konnten nur Superuser Erweiterungen installieren. Um dies zu vereinfachen, führt PostgreSQL 13 das Konzept von "[vertraulichen  Erweiterungen](https://www.postgresql.org/docs/13/sql-createextension.html)" ein. Dies erlaubt Datenbank-Benutzern Erweiterungen selbst zu installieren, die zuvor von einem Superuser als vertraulich markiert wurden. Einige Standard-Erweiterungen sind nun schon standardmässig als vertraulich markiert, z.B. [`pgcrypto`](https://www.postgresql.org/docs/13/pgcrypto.html), [`tablefunc`](https://www.postgresql.org/docs/13/tablefunc.html), [`hstore`](https://www.postgresql.org/docs/13/hstore.html), und viele mehr.

Für Applikationen die sichere Authentifizierungsmethoden erfordern, bietet PostgreSQL 13 den Clients ein [require channel binding](https://www.postgresql.org/docs/13/libpq-connect.html#LIBPQ-CONNECT-CHANNEL-BINDING) wenn [SCRAM authentication](https://www.postgresql.org/docs/13/sasl-authentication.html#SASL-SCRAM-SHA-256) verwendet wird. Zusätzlich kann der PostgreSQL Foreign Data Wrapper ([`postgres_fdw`](https://www.postgresql.org/docs/13/postgres-fdw.html)) nun Zertifikate für die Authentifizierung verwenden.

## Über PostgreSQL

[PostgreSQL](https://www.postgresql.org) ist das führende Open-Source Datenbanksystem, mit einer weltweiten Community bestehend aus Tausenden von Nutzern und Mitwirkenden sowie Dutzenden von Firmen und Organisationen. Das PostgreSQL Projekt baut auf über 30 Jahre Erfahrung auf, beginnend an der University of California, Berkeley, und hat heute eine nicht zu vergleichende Performance bei der Entwicklung. PostgreSQL's ausgereiftes Feature Set ist nicht nur mit den führenden proprietären Datenbanksystemen vergleichbar, sondern übertrifft diese in erweiterten Datenbankfunktionen, Erweiterbarkeit, Sicherheit und Stabilität.

### Übersetzungen der Presseerklärung

* TBD

### Links

* [Download](https://www.postgresql.org/download/)
* [Versionshinweise](https://www.postgresql.org/docs/13/release-13.html)
* [Pressemitteilung](https://www.postgresql.org/about/press/)
* [Sicherheit](https://www.postgresql.org/support/security/)
* [Versionierungsrichtlinie](https://www.postgresql.org/support/versioning/)
* [Folge @postgresql auf Twitter](https://twitter.com/postgresql)
