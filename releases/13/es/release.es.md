El Grupo Global de Desarrollo de PostgreSQL ha anunciado hoy el lanzamiento de
[PostgreSQL 13](https://www.postgresql.org/docs/13/release-13.html), la versión
más reciente de la [base de datos de código abierto más avanzada del mundo](https://www.postgresql.org/).

PostgreSQL 13 incluye mejoras significativas en su sistema de indexación y
búsqueda, las cuales benefician a bases de datos de gran tamaño. Entre
ellas figuran el ahorro de espacio y un mejor rendimiento para los índices,
tiempos de respuesta más rápidos para las consultas que utilizan funciones de
agregación o tablas particiones, una mejor planificación de las consultas al
utilizar estadísticas mejoradas, y mucho más.

Junto con características muy solicitadas como la ejecución de
[VACUUM en paralelo](https://www.postgresql.org/docs/13/sql-vacuum.html)
y el [ordenamiento incremental](https://www.postgresql.org/docs/13/runtime-config-query.html#GUC-ENABLE-INCREMENTAL-SORT),
PostgreSQL 13 proporciona una mejor experiencia de gestión de datos para
cargas de trabajo grandes y pequeñas. Ofrece optimizaciones para la
administración diaria, utilidades adicionales para los desarrolladores
de aplicaciones y mejoras en la seguridad.

"PostgreSQL 13 es una muestra de la colaboración y dedicación de nuestra
comunidad global en impulsar el desarrollo de la base de datos relacional
de código abierto más avanzada del mundo", dice Peter Eisentraut, miembro
del _core team_ de PostgreSQL. "Las innovaciones que cada versión trae
consigo, junto con su reputación de fiabilidad y estabilidad, son la razón
por la que cada vez más usuarios eligen emplear PostgreSQL para sus aplicaciones".

[PostgreSQL](https://www.postgresql.org) es un innovador sistema de gestión
de datos reconocido por su fiabilidad y robustez.  Cuenta con más de 25 años de
desarrollo de código abierto por parte de una
[comunidad global de desarrolladores](https://www.postgresql.org/community/),
y se ha convertido en la base de datos relacional de código abierto preferida
por organizaciones de todos los tamaños.

### Incrementos de rendimiento constantes

Aprovechando el trabajo realizado en la anterior versión, PostgreSQL 13
puede gestionar eficientemente [datos duplicados en índices B-tree](https://www.postgresql.org/docs/13/btree-implementation.html#BTREE-DEDUPLICATION),
el índice estándar de muchas bases de datos. Esto reduce el espacio total
requerido por dichos índices y mejora el rendimiento general de las consultas.

PostgreSQL 13 introduce el ordenamiento incremental, en el que los datos
ordenados en una etapa anterior de una consulta pueden acelerar el ordenamiento
en una etapa posterior. Adicionalmente, PostgreSQL puede ahora utilizar el
sistema de [estadísticas extendidas](https://www.postgresql.org/docs/13/planner-stats.html#PLANNER-STATS-EXTENDED)
(creadas a través de [`CREATE STATISTICS`](https://www.postgresql.org/docs/13/sql-createstatistics.html))
para crear mejores planes en consultas con cláusulas `OR` y búsquedas en listas con `IN` y `ANY`.

En PostgreSQL 13, más tipos de [funciones de agregación](https://www.postgresql.org/docs/13/functions-aggregate.html)
y consultas de [conjuntos de agrupación](https://www.postgresql.org/docs/13/queries-table-expressions.html#QUERIES-GROUPING-SETS)
pueden aprovechar la eficiente agregación _hash_ de PostgreSQL,
incluso agregaciones de gran tamaño que excedan la capacidad de la memoria.
Se ha incrementado también el rendimiento de las consultas con
[tablas particionadas](https://www.postgresql.org/docs/13/ddl-partitioning.html), puesto
que existen ahora más casos en los que ciertas particiones pueden ser excluidas de la
consulta y en los que ciertas particiones pueden enlazarse (_join_) directamente.


### Administración optimizada

La ejecución de [VACUUM](https://www.postgresql.org/docs/13/routine-vacuuming.html) es
una parte esencial de la administración de PostgreSQL, ya que permite a la base de datos
recuperar espacio de almacenamiento tras la modificación y la eliminación de filas. Aunque
este proceso puede presentar desafíos administrativos, desde las versiones anteriores de
PostgreSQL se ha trabajado para reducir la sobrecarga que conlleva el uso de VACUUM.


PostgreSQL 13 continúa mejorando el sistema de limpieza de la base de datos
introduciendo la funcionalidad de [VACUUM en paralelo para índices](https://www.postgresql.org/docs/13/sql-vacuum.html).
Además de los beneficios de rendimiento que VACUUM ofrece, esta nueva característica
puede ajustarse a cargas de trabajo específicas, puesto que permite a los administradores
seleccionar el número de trabajadores (workers) paralelos requeridos. A estas ventajas
se añade ahora la posibilidad de activar el proceso de autovacuum mediante la inserción
de datos.

Los [slots de replicación](https://www.postgresql.org/docs/13/warm-standby.html#STREAMING-REPLICATION-SLOTS),
utilizados para evitar que los registros de escritura anticipada (WAL) se
eliminen antes de ser recibidos por una réplica, pueden ser configurados en
PostgreSQL 13 para especificar el [número máximo de archivos WAL que deben retenerse](https://www.postgresql.org/docs/13/runtime-config-replication.html#GUC-MAX-SLOT-WAL-KEEP-SIZE)
y así prevenir errores producidos por falta de espacio en el disco.

PostgreSQL 13 ofrece también al administrador métodos adicionales para monitorear
la actividad de la base de datos. Estos incluyen referencias a las estadísticas
de uso de WAL a través de `EXPLAIN`, así como el monitoreo del progreso de los
respaldos y de la ejecución de `ANALYZE`. Adicionalmente, la integridad de los respaldos
tomados por la utilidad [`pg_basebackup`](https://www.postgresql.org/docs/13/app-pgbasebackup.html) puede ser
comprobada usando la nueva utilidad [`pg_verifybackup`](https://www.postgresql.org/docs/13/app-pgverifybackup.html).

### Utilidades para el desarrollo de aplicaciones

En PostgreSQL 13 es aún más fácil trabajar con tipos de datos de PostgreSQL
provenientes de diferentes fuentes. Esta versión añade la
función  [`datetime()`](https://www.postgresql.org/docs/13/functions-json.html#FUNCTIONS-SQLJSON-OP-TABLE)
a su soporte para SQL/JSON path, la cual convierte los formatos de tiempo válidos (por
ejemplo, las cadenas ISO 8601) a tipos nativos de PostgreSQL. Adicionalmente, la función
de generación de UUID v4, [`gen_random_uuid()`](https://www.postgresql.org/docs/13/functions-uuid.html),
está ahora disponible sin necesidad de instalar ninguna extensión.

El sistema de particionamiento de PostgreSQL es más flexible, ya que las tablas
particionadas soportan completamente la replicación lógica y los triggers a nivel
de fila de tipo BEFORE.

La sintaxis [`FETCH FIRST`](https://www.postgresql.org/docs/13/sql-select.html#SQL-LIMIT)
en PostgreSQL 13 se amplía ahora para incluir la cláusula `WITH TIES`. Al especificarse,
`WITH TIES` incluye todas las filas que, en base a la cláusula `ORDER BY`,
quedan empatadas con la última fila visible del resultado.

### Mejoras en la seguridad

El sistema de extensiones de PostgreSQL es un componente clave de su robustez,
puesto que permite a los desarrolladores ampliar su funcionalidad. En versiones
anteriores, únicamente un superusuario de la base de datos podía instalar
nuevas extensiones. Para permitir el máximo provecho a la extensibilidad
de PostgreSQL, en PostgreSQL 13 se añade el concepto de [_trusted extension_](https://www.postgresql.org/docs/13/sql-createextension.html),
que permite a los usuarios de la base de datos instalar extensiones que un
superusuario marca como "de confianza". Ciertas extensiones integradas (como
[`pgcrypto`](https://www.postgresql.org/docs/13/pgcrypto.html),
[`tablefunc`](https://www.postgresql.org/docs/13/tablefunc.html),
[`hstore`](https://www.postgresql.org/docs/13/hstore.html) y otras más) ya están
marcadas como confiables por defecto.

Para aplicaciones que requieren métodos de autenticación seguros, PostgreSQL 13
permite a los clientes [requerir el enlace de canal (_channel binding_)](https://www.postgresql.org/docs/13/libpq-connect.html#LIBPQ-CONNECT-CHANNEL-BINDING)
al usar la [autenticación SCRAM](https://www.postgresql.org/docs/13/sasl-authentication.html#SASL-SCRAM-SHA-256).
Además, el contenedor de datos externos de PostgreSQL ([`postgres_fdw`](https://www.postgresql.org/docs/13/postgres-fdw.html))
puede ahora usar la autenticación basada en certificados.

### Sobre PostgreSQL

[PostgreSQL](https://www.postgresql.org) es la base de datos de código
abierto más avanzada del mundo, que cuenta con una comunidad global de
miles de usuarios, colaboradores, empresas y organizaciones. Basada en
más de 30 años de ingeniería, que comenzaron en la Universidad de
Berkeley en California, PostgreSQL ha continuado con un ritmo de desarrollo
inigualable. El maduro conjunto de características de PostgreSQL no sólo
iguala a los principales sistemas de bases de datos propietarios, sino
que los supera en términos de características avanzadas, extensibilidad,
seguridad y estabilidad.

### Traducciones de los comunicados de prensa

* TBD

### Enlaces

* [Descargas](https://www.postgresql.org/download/)
* [Notas de la versión](https://www.postgresql.org/docs/13/release-13.html)
* [Kit de prensa](https://www.postgresql.org/about/press/)
* [Información de seguridad](https://www.postgresql.org/support/security/)
* [Directiva de versiones](https://www.postgresql.org/support/versioning/)
* [Síganos en Twitter @postgresql](https://twitter.com/postgresql)
