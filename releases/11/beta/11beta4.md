PostgreSQL 11 Beta 4 Released
=============================

The PostgreSQL Global Development Group announces that the fourth beta release
of PostgreSQL 11 is now available for download. This release contains previews
of all features that will be available in the final release of PostgreSQL 11
(though some details could change before a release candidate is made available)
as well as bug fixes that were reported during the third beta.

This is likely the last beta release for PostgreSQL 11 before a release
candidate is made available.

In the spirit of the open source PostgreSQL community, we strongly encourage you
to test the new features of PostgreSQL 11 in your database systems to help us
eliminate any bugs or other issues that may exist.

Upgrading to PostgreSQL 11 Beta 4
---------------------------------

To upgrade to PostgreSQL 11 Beta 4 from Beta 3 or an earlier version of
PostgreSQL 11, you will need to use a strategy similar to upgrading between
major versions of PostgreSQL (e.g. `pg_upgrade` or `pg_dump` / `pg_restore`).
For more information, please visit the documentation section on
[upgrading](https://www.postgresql.org/docs/11/static/upgrading.html).

Changes Since 11 Beta 3
-----------------------

There have been many bug fixes for PostgreSQL 11 reported during the Beta 3
period and applied to the Beta 4 release. This release also includes other bug
fixes that were reported for supported versions of PostgreSQL that also affected
PostgreSQL 11. These fixes include:

* JIT compilation is disabled by default. To enable JIT compilation, you must
set `jit = on` in your configuration file or in a session
* Enforce that names for constraints are unique
* Several fixes for stored procedures, including improved hints and
documentation around usage
* Several fixes for partitioning, including fixes for runtime partition pruning
* Fix for parallelized `CREATE INDEX` by disallowing builds on mapped catalog
relations
* Fix for REINDEX to prevent unprivileged users from reindexing shared catalogs
in certain cases
* Memory-leak fix when multiple calls are made to `XMLTABLE` in a single query
* Several fixes for `pg_verify_checksums`
* Several fixes for `libpq`, including a fix to only look up a hostname when
explicitly requested to do so
* Several `pg_upgrade` fixes, including handling event triggers in extensions
correctly
* Several fixes for `pg_dump` and `pg_restore`

For a detailed list of fixes, please visit the
[open items](https://wiki.postgresql.org/wiki/PostgreSQL_11_Open_Items#resolved_before_11beta4)
page.

Testing for Bugs & Compatibility
--------------------------------

The stability of each PostgreSQL release greatly depends on you, the community,
to test the upcoming version with your workloads and testing tools in order to
find bugs and regressions before the release of PostgreSQL 11. As this is a
Beta, minor changes to database behaviors, feature details, and APIs are still
possible. Your feedback and testing will help determine the final tweaks on the
new features, so please test in the near future. The quality of user testing
helps determine when we can make a final release.

A list of [open issues](https://wiki.postgresql.org/wiki/PostgreSQL_11_Open_Items)
is publicly available in the PostgreSQL wiki.  You can
[report bugs](https://www.postgresql.org/account/submitbug/) using this form on
the PostgreSQL website:

[https://www.postgresql.org/account/submitbug/](https://www.postgresql.org/account/submitbug/)

Beta Schedule
-------------

This is the fourth beta release of PostgreSQL 11. In all likelihood, this is the
final beta release of PostgreSQL 11 before one or more release candidates are
made available. For further information please see the
[Beta Testing](https://www.postgresql.org/developer/beta/) page.

Links
-----

* [Download](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta/)
* [PostgreSQL 11 Beta Release Notes](https://www.postgresql.org/docs/11/static/release-11.html)
* [PostgreSQL 11 Open Issues](https://wiki.postgresql.org/wiki/PostgreSQL_11_Open_Items)
* [Submit a Bug](https://www.postgresql.org/account/submitbug/)
