# PostgreSQL 11 Dirilis

07 SEPT 2018 - Grup Pengembangan Global PostgreSQL hari ini mengumumkan
rilis PostgreSQL 11, versi terbaru dari Open Source paling mumpuni di dunia

PostgreSQL 11 menyediakan peningkatan kinerja keseluruhan RDBMS kepada pengguna, 
dengan peningkatan fitur tambahan khusus yang terkait dengan basis data yang sangat besar
dan beban kerja komputasi tinggi. Selanjutnya, PostgreSQL 11 membuat perbaikan yang signifikan
pada sistem partisi tabel, menambahkan dukungan untuk /Stored Procedure/
dengan kemampuan manajemen transaksional, meningkatkan query paralel dan
menambahkan kemampuan definisi data yang paralel, dan memperkenalkan just-in-time
(JIT) kompilasi untuk mempercepat eksekusi ekspresi dalam query.

"Untuk PostgreSQL 11, komunitas pengembangan kami fokus pada penambahan fitur itu
meningkatkan kemampuan PostgreSQL untuk mengelola basis data yang sangat besar, "kata Bruce
Momjian, seorang [anggota tim inti] (https://www.postgresql.org/developer/core/) dari
[PostgreSQL Global Development Group] (https://www.postgresql.org). "Diatas dari
Kinerja yang terbukti PostgreSQL untuk beban kerja transaksional, PostgreSQL 11 membuat
bahkan lebih mudah bagi pengembang untuk menjalankan aplikasi data besar dalam skala besar. "

##### HOLD UNTUK QUOTE DARI ANGGOTA TIM INTI TENTANG RELEASE

PostgreSQL dikembangkan lebih dari dua puluh tahun dalam Open Source dan 
menjadi database relasional Open Source yang lebih disukai untuk pengembang aplikasi. 
Komunitas PostgreSQL Open Source terus mendapat pengakuan di seluruh industri, dan telah ditampilkan sebagai
"DBMS of the Year 2017" oleh DB-Engines dan dalam "SD Times 2018 100".

PostgreSQL 11 adalah rilis besar pertama sejak PostgreSQL 10 dirilis pada
5 Oktober 2017. Pembaruan pembaruan berikutnya untuk PostgreSQL 11 berisi perbaikan bug
akan menjadi PostgreSQL 11.1, dan rilis besar berikutnya dengan fitur baru akan
PostgreSQL 12.

## Peningkatan Ketajaman dan Kinerja untuk Mempartisi

PostgreSQL 11 menambahkan kemampuan untuk mempartisi data dengan kunci hash, juga dikenal sebagai
hash partitioning, menambah kemampuan saat ini untuk mempartisi data di PostgreSQL
oleh daftar nilai atau rentang. PostgreSQL 11 meningkatkan lebih lanjut datanya
kemampuan federasi dengan peningkatan fungsi untuk partisi yang digunakan fitur "Foreign Data Wrapper/FDW"
[postgres_fdw] (https://www.postgresql.org/docs/current/static/postgres-fdw.html).

Untuk membantu mengelola partisi, PostgreSQL 11 memperkenalkan partisi "Catch-All" 
untuk menampung data yang tidak sesuai dengan kunci partisi, dan kemampuan untuk
membuat Primary Key, Foreign Key, Indeks dan memicu partisi yang diturunkan ke semua tabel. PostgreSQL 11 juga mendukung baris yang bergerak secara otomatis
ke partisi yang benar jika kunci partisi untuk baris itu diperbarui.

##### HOLD UNTUK QUOTE TENTANG PARTITIONING

PostgreSQL 11 meningkatkan kinerja kueri saat membaca dari partisi dengan menggunakan strategi penghapusan
partisi baru. Selain itu, PostgreSQL 11 sekarang mendukung fitur "UPSERT" yang populer pada tabel yang dipartisi, 
yang membantu pengguna untuk menyederhanakan kode aplikasi dan mengurangi overhead jaringan saat berinteraksi dengan
data mereka.

## Transaksi yang Didukung dalam Stored Procedures

Pengembang telah mampu membuat fungsi yang ditentukan pengguna di PostgreSQL untuk
lebih dari 20 tahun, tetapi sebelum PostgreSQL 11, fungsi-fungsi ini tidak dapat dikelola
transaksi mereka sendiri. PostgreSQL 11 menambahkan prosedur SQL yang dapat bekerja penuh
manajemen transaksi dalam tubuh fungsi, memungkinkan pengembang untuk
membuat aplikasi sisi server yang lebih canggih, seperti yang melibatkan
pemuatan data massal inkremental.

Prosedur SQL dapat dibuat menggunakan perintah `CREATE PROCEDURE`, dieksekusi
menggunakan perintah `CALL`, dan didukung oleh prosedural sisi server
bahasa PL / pgSQL, PL / Perl, PL / Python, dan PL / Tcl.

##### HOLD UNTUK QUOTE TENTANG PROSEDUR PENYIMPANAN SQL

## Peningkatan Kemampuan untuk Query Parallelism

PostgreSQL 11 meningkatkan kinerja query paralel, dengan peningkatan kinerja dalam
scan sekuensial paralel dan hash bergabung bersama dengan pemindaian yang lebih efisien
data yang dipartisi. PostgreSQL sekarang dapat mengeksekusi query SELECT yang menggunakan `UNION` di
sejajar jika query yang mendasarinya tidak dapat diparalelkan.

PostgreSQL 11 menambahkan paralelisme ke beberapa perintah definisi data, terutama untuk
pembuatan indeks B-tree yang dihasilkan dengan melaksanakan perintah standar `CREATE INDEX`. 
Beberapa perintah definisi data yang baik buat tabel atau tampilan terwujud dari query juga paralel sekarang,
termasuk `CREATE TABLE .. AS`,` SELECT INTO`, dan `CREATE MATERIALIZED VIEW`.

##### HOLD FOR QUOTE TENTANG PARALELISME

## Just-in-Time (JIT) Kompilasi untuk Ekspresi

PostgreSQL 11 memperkenalkan dukungan untuk kompilasi Just-In-Time (JIT)
mempercepat eksekusi ekspresi tertentu selama eksekusi query. JIT
Kompilasi ekspresi untuk PostgreSQL menggunakan proyek LLVM untuk mempercepat
eksekusi ekspresi dalam klausa WHERE, daftar target, agregat,
proyeksi, dan beberapa operasi internal.

##### HOLD FOR QUOTE TENTANG JIT

Untuk memanfaatkan kompilasi JIT, Anda harus menginstal LLVM
dependensi untuk mengaktifkan kompilasi JIT di file pengaturan PostgreSQL Anda
dengan menetapkan `jit = on` atau dari sesi PostgreSQL Anda dengan mengeksekusi
`SET jit = on`.

## Peningkatan Pengalaman Pengguna Umum

Peningkatan ke database relasional PostgreSQL tidak mungkin tanpa
umpan balik dari komunitas pengguna aktif dan kerja keras oleh orang-orang yang
bekerja di PostgreSQL. Di bawah ini menyoroti beberapa dari banyak fitur yang termasuk dalam
PostgreSQL 11 dirancang untuk meningkatkan pengalaman pengguna secara keseluruhan:

- Menghapus kebutuhan untuk `ALTER TABLE .. ADD COLUMN .. DEFAULT ..` dengan tidak
`NULL`, default untuk menulis ulang seluruh tabel pada eksekusi. Hal ini meningkatan kinerja 
yang signifikan saat menjalankan perintah ini.
- "Meliputi indeks," yang memungkinkan pengguna menambahkan kolom tambahan ke indeks
menggunakan `termasuk` klausul dan sangat membantu untuk melakukan scan indeks saja,
terutama pada tipe data yang tidak dapat diindeks oleh indeks B-tree.
- Fungsionalitas tambahan untuk bekerja dengan fungsi-fungsi window, termasuk mengizinkan
`RANGE` untuk menggunakan` PRECEDING` / `FOLLOWING`,` GROUPS`, dan pengecualian frame
- Dimasukkannya kata kunci "berhenti" dan "keluar" di baris perintah PostgreSQL
antarmuka untuk membantu membuatnya lebih mudah meninggalkan alat baris perintah

Untuk daftar lengkap fitur yang termasuk dalam rilis ini, silakan baca
[catatan rilis] (https://www.postgresql.org/docs/11/static/release-11.html),
yang dapat ditemukan di:

[https://www.postgresql.org/docs/11/static/release-11.html](https://www.postgresql.org/docs/11/static/release-11.html)

## Tentang PostgreSQL

PostgreSQL adalah aplikasi Basis Data berbasiskan Open Source paling mumpuni di dunia, dengan ribuan komunitas pengguna, kontributor, perusahaan, dan organisasi tersebar diseluruh dunia. Open Source PostgreSQL dibangun dan direkayasa lebih dari 30 tahun, 
mulai dari riset akademis di University of California, Berkeley, dan terus dengan kecepatan pengembangan yang tak tertandingi. 
Fitur matang PostgreSQL tidak hanya cocok dengan sistem basis data proprietary teratas, 
tetapi melebihi mereka dalam fitur yang lebih lanjut, ekstensibilitas, keamanan dan stabilitas. 
Pelajari lebih lanjut tentang PostgreSQL dan berpartisipasi dalam komunitas kita di [PostgreSQL.org] (https://www.postgresql.org).