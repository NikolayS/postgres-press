# Sortie de PostgreSQL 11

JJ octobre 2018— Le PostgreSQL Global Development Group a annoncé aujourd’hui la
sortie de PostgreSQL 11, la dernière version du SGBD open source le plus avancé
au monde.

PostgreSQL 11 propose des améliorations globales de performance ainsi que des
avancées spécifiques pour les bases de données à très gros volume ou à très gros
trafic. De plus, PostgreSQL 11 a amélioré de manière importante le système de
partitionnement, a ajouté la possibilité de gérer les transactions au sein des
procédures stockées et étend les possibilités autour du parallélisme tant au
niveau de l'exécution des requêtes qu'au niveau de la définition des données.
Les cas où le parallélisme pourra être
utilisé pour répondre plus rapidement à une requête ont également été élargis et le
Just-in-Time (JIT) a été introduit, permettant d’injecter une expression
précompilée directement dans la requête.

"Pour PostgreSQL 11, notre communauté de développeurs s'est concentrée sur
l'ajout de fonctionnalités pour améliorer la gestion de très grosses bases" a
dit Bruce Momjian, un
[membre de la core team](https://www.postgresql.org/developer/core/) du [PostgreSQL Global
Development Group](https://www.postgresql.org). "Ainsi, en plus de son
excellente gestion de bases transactionnelles, PostgreSQL 11 simplifie la
gestion d'applications big data, même à très grande échelle."

Forte de plus de vingt ans de développement open source, PostgreSQL est devenu la
base de données open source préférée des développeurs. Le projet séduit toujours
plus le monde professionnel et a été nommé "[SGBD de l’année
2017](https://db-engines.com/en/blog_post/76)" par DB-Engines et
est dans le palmarès du [SD Times 100](https://sdtimes.com/sdtimes-100/2018/best-in-show/database-and-database-management-2018/).

PostgreSQL 11 est la première version majeure depuis la sortie de Postgres 10,
le 5 octobre 2017. La prochaine mise à jour mineure (contenant uniquement des
correctifs) pour PostgreSQL 11 sera la version 11.1 et la prochaine version
majeure (avec de nouvelles fonctionnalités) sera PostgreSQL 12.

## Fiabilité et performance accrues pour le partitionnement

PostgreSQL 11 ajoute la possibilité de partitionner une table par hashage de clé
aux autres possibilités de partitionnement (par listes de valeur ou par
intervalles). De plus, PostgreSQL 11 augmente la convergence des données avec de
nombreuses améliorations du partitionnement sur les Foreign Data Wrappers,
[postgres\_fdw](https://www.postgresql.org/docs/current/static/postgres-fdw.html).

Pour faciliter la maintenance sur les partitions, PostgreSQL 11 introduit
une partition par défaut pour les données ne correspondant à aucune partition,
ainsi que la possibilité de créer des clés primaires, clés étrangères, index et
triggers qui seront automatiquement applicables sur l'ensemble partitions. PostgreSQL 11 supporte aussi
le changement automatique de partition si la clé de partitionnement est
mise à jour pour une ligne spécifique et que cela impose un changement de
partition.

PostgreSQL 11 améliore aussi les performances lors de la lecture des partitions
grâce à une nouvelle stratégie plus efficace d’élimination de partition. De
plus, PostgreSQL 11 supporte maintenant l’"upsert" sur les tables partitionnées, ce
qui simplifie le code des applications et réduit l’utilisation du réseau.

## Gestion des transactions dans les procédures stockées

Les développeurs peuvent créer des fonctions spécifiques dans PostgreSQL depuis
plus de 20 ans, mais avant PostgreSQL 11, ces fonctions ne pouvaient pas gérer
leurs propres transactions. PostgreSQL 11 apporte les procédures SQL qui
permettent de gérer complètement les transactions dans le corps de la fonction, ce
qui permet aux développeurs de déporter des traitements avancés sur le serveur
de base de données, notamment pour tout ce qui implique du chargement
incrémental de données brutes.

Les procédures SQL peuvent être créées en utilisant la commande `CREATE
PROCEDURE`, être exécutées en utilisant la commande `CALL` et peuvent,  pour
l'instant, être écrites dans les langages PL/pgSQL, PL/Perl, PL/Python, et PL/Tcl.

## Amélioration du parallélisme des requêtes

PostgreSQL 11 améliore les performances du parallélisme avec des gains de
performances pour les scans séquentiels parallèles et pour les hash joins tout
en permettant des scans plus efficaces des données partitionnées. PostgreSQL
peut maintenant exécuter un `SELECT` avec `UNION` en parallèle dans le cas où
les requêtes impliquées ne peuvent pas être parallélisées.

PostgreSQL 11 permet aussi de paralléliser plusieurs requêtes de modification de
schéma, notamment
la création d’index B-tree générés par l’exécution de la commande standard
`CREATE INDEX`. Plusieurs commandes de modification de structure, qu’elles
créent des tables ou des vues matérialisées à partir de requêtes peuvent
maintenant être parallélisées, comme `CREATE TABLE .. AS`, `SELECT INTO` et
`CREATE MATERIALIZED VIEW`.

## Compilation Just-in-Time (JIT) pour les expressions

PostgreSQL 11 introduit la prise en charge de la compilation Just-In-Time (JIT) pour
accélérer l’exécution de certaines expressions durant l’exécution d’une requête.
La compilation d’expressions JIT pour PostgreSQL utilise le travail du projet LLVM pour
accélérer l’exécution d’expressions dans les clauses `WHERE`, les listes
cibles, les agrégats, les projections et pour certaines opérations internes.

Pour pouvoir utiliser la compilation JIT, vous devrez installer la dépendance
LLVM puis activer la compilation JIT soit dans le fichier de configuration (`jit
= on`), soit durant votre session en exécutant `SET jit = on`.

## Améliorations générales de l’expérience utilisateur

Les améliorations de PostgreSQL ne seraient pas possibles sans les retours
que nous recevons d’une communauté d’utilisateurs très active ni sans le dur
travail des personnes contribuant au projet PostgreSQL. Vous trouverez
ci-dessous quelques fonctionnalités de PostgreSQL développées pour améliorer globalement
l’expérience utilisateur, mais sachez qu’il y en a de très nombreuses autres :

- L’ordre `ALTER TABLE .. ADD COLUMN .. DEFAULT ..` avec une valeur par défaut non `NULL`
n’a plus besoin de réécrire entièrement la table lors de son exécution, ce qui entraîne
une grosse amélioration des performances.
- Il est désormais possible de créer un index "couvrant" en ajoutant une ou plusieurs
colonne(s) à un index existant dans la clause `INCLUDE`. Ce type d’index est très utile
pour avoir des index-only scans dans les plans d’exécution, surtout sur les types de
données non indexables par des index B-tree.
- De nouvelles fonctionnalités pour les fonctions de fenêtrage sont ajoutées,
dont permettre l’utilisation de `RANGE` dans des clauses
`PRECEDING`/`FOLLOWING`, `GROUPS` ou d’exclusion
- Ajout des commandes "quit" et "exit" dans le client ligne de commandes de
PostgreSQL (psql) pour faciliter la sortie

Pour une liste complète des fonctionnalités de cette nouvelle version, vous
pouvez lire les [notes de version] (https://www.postgresql.org/docs/11/static/release-11.html), qui peut
être trouvée ici :

[https://www.postgresql.org/docs/11/static/release-11.html](https://www.postgresql.org/docs/11/static/release-11.html)

## À propos de PostgreSQL

[PostgreSQL](https://www.postgresql.org) est le système de gestion de bases de données libre de référence. Sa
communauté mondiale est composée de plusieurs milliers d’utilisateurs et
contributeurs, et de plusieurs dizaines d’entreprises et institutions. Le projet
PostgreSQL, démarré il y a 30 ans, à l’université de Californie, à Berkeley, a
atteint aujourd’hui un rythme de développement sans pareil. L’ensemble des
fonctionnalités proposées est mature et plus riche que ceux des systèmes
commerciaux leaders sur les fonctionnalités avancées, les extensions, la
sécurité et la stabilité, offertes à un niveau que seul PostgreSQL atteint.
Pour en savoir plus à propos de PostgreSQL et participer à la communauté :
[PostgreSQL.org](https://www.postgresql.org).
