PostgreSQL Global Development Groupは本日、[世界で最も高度なオープンソースデータベース](https://www.postgresql.org/)の最新バージョンである[PostgreSQL 14](https://www.postgresql.org/docs/14/release-14.html)のリリースを発表しました。

PostgreSQL14には、開発者や管理者がデータバックアップアプリケーションを配備するのに役立つさまざまな機能が用意されています。PostgreSQLは、複雑なデータ型に対して革新的な機能を追加し続けています。
これには、JSONへのより便利なアクセスや、連続していないデータ範囲のサポートなどが含まれます。今回の最新リリースでは、接続の同時実行性、大量書き込みワークロード、クエリの並列性、および論理レプリケーションの向上に伴い、PostgreSQLのハイパフォーマンスおよび分散データワークロードの改善傾向がさらに強化されています。

PostgreSQLコアチームのメンバーであるMagnus Hagander氏は次のように述べています。「PostgreSQLのこの最新リリースは、ユーザのデータワークロードを大規模に管理する能力を向上させ、可観測性を高め、アプリケーション開発者を支援する新機能を含んでいます。」
「PostgreSQL14は、グローバルなPostgreSQLコミュニティがフィードバックに対応し、大小さまざまな組織が導入する革新的なデータベースソフトウェアを提供し続けていることを示すものです。」

革新的なデータ管理システムである[PostgreSQL](https://www.postgresql.org)は、信頼性と堅牢性で知られており、世界中の開発者コミュニティによる25年以上にわたるオープンソース開発の恩恵を受けており、あらゆる規模の組織に好まれるオープンソースリレーショナルデータベースとなっています。

### JSON利便性とマルチ範囲

PostgreSQLはPostgreSQL9.2のリリース以来、[JSON](https://www.postgresql.org/docs/14/datatype-json.html)データの操作をサポートしてきましたが、値の取得には固有の構文が使用されていました。PostgreSQL14では、[添字を使用してJSONデータにアクセス](https://www.postgresql.org/docs/14/datatype-json.html#JSONB-SUBSCRIPTING)できるようになりました。例えば、「SELECT ('{ "postgres": { "release": 14 }}'::jsonb)['postgres']['release'];」のようなクエリが動作するようになりました。
これにより、PostgreSQLは、JSONデータから情報を取得するために一般的に認識されている構文と整合します。PostgreSQL14に追加された添字フレームワークは、一般に他のネストされたデータ構造に拡張することができ、このリリースでは「[hstore](https://www.postgresql.org/docs/14/hstore.html)」データ型にも適用されます。

同じくPostgreSQL9.2で最初にリリースされた[範囲型](https://www.postgresql.org/docs/14/rangetypes.html)は、「[マルチ範囲](https://www.postgresql.org/docs/14/rangetypes.html#RANGETYPES-BUILTIN)」データ型の導入により、非隣接範囲をサポートするようになりました。マルチ範囲は、重複しない範囲の順序付きリストです。これにより、開発者は、範囲の複雑なシーケンスを処理するためのより単純な問合せを作成できます。PostgreSQL固有の範囲型(日付、時刻、数値)はマルチ範囲をサポートしており、その他のデータ型も拡張してマルチレンジのサポートを使用できます。

### 高負荷ワークロードに対する性能改善

PostgreSQL14では、多くの接続を使用するワークロードのスループットが大幅に向上し、ベンチマークによっては2倍の高速化が示されています。
このリリースでは、[頻繁に更新されるインデックス](https://www.postgresql.org/docs/14/btree-implementation.html#BTREE-DELETION)を持つテーブルのインデックスの膨張を軽減することにより、B-treeインデックスの管理に対する最近の改善が継続されています。

PostgreSQL14では[問い合わせをパイプライン化](https://www.postgresql.org/docs/14/libpq-pipeline-mode.html)する機能が導入されました。
これにより、遅延の大きい接続や多くの小さな書き込み(INSERT/UPDATE/DELETE)操作を伴うワークロードのパフォーマンスが大幅に向上します。
これはクライアント側の機能であるため、最新のPostgreSQLデータベースで、バージョン14クライアントまたは[バージョン14のlibpqで作成されたクライアントドライバ](https://wiki.postgresql.org/wiki/List_of_drivers)を使って、パイプラインモードを使用できます。


### 分散ワークロードに対する強化

分散PostgreSQLデータベースはPostgreSQL14から恩恵を受けることができます。[論理レプリケーション](https://www.postgresql.org/docs/current/logical-replication.html)を使用する場合、PostgreSQLは進行中のトランザクションをサブスクライバにストリーミングできるようになりました。これにより、サブスクライバに大規模なトランザクションを適用する際のパフォーマンスが大幅に向上します。
PostgreSQL14では、論理レプリケーションを実行する論理デコード・システムに対して、他にもいくつかのパフォーマンス拡張が追加されています。

PostgreSQLおよび他のデータベース間の連合ワークロードを処理するために使用される[外部データラッパー](https://www.postgresql.org/docs/14/sql-createforeigndatawrapper.html)は、PostgreSQL14の問い合わせ並列性を利用できるようになりました。このリリースでは、他のPostgreSQLデータベースとインタフェースする外部データラッパーである「[postgres_fdw](https://www.postgresql.org/docs/14/postgres-fdw.html)」でこの機能を実装しています。

「postgres_fdw」では、クエリの並列処理のサポートに加えて、外部テーブルにデータを一括挿入したり、「[IMPORT FOREIGN SCHEMA](https://www.postgresql.org/docs/14/sql-importforeignschema.html)」指定を使用してテーブルパーティションをインポートできるようになりました。

### 管理と可観測性

PostgreSQL14では、B-Treesによるオーバーヘッドを削減するための最適化を含め、パフォーマンスの向上が[バキューム](https://www.postgresql.org/docs/14/routine-vacuuming.html)システムにまで拡張されました。
このリリースでは、トランザクションIDのラップアラウンドを防止するように設計されたバキューム「緊急モード」も追加されました。
データベース統計を収集するために使用される「[ANALYZE](https://www.postgresql.org/docs/14/sql-analyze.html)」は、独自のパフォーマンス改善により、PostgreSQL14で大幅に高速に動作するようになりました。

PostgreSQLの[TOAST](https://www.postgresql.org/docs/14/storage-toast.html)システム用の圧縮は、テキストやジオメトリのブロックのような大きなデータを格納するために使用されますが、これを[設定できるようになりました](https://www.postgresql.org/docs/14/runtime-config-client.html#GUC-DEFAULT-TOAST-COMPRESSION)。
PostgreSQL14でTOAST列用にLZ4圧縮が追加されていますが、「pglz」圧縮のサポートは維持されています。

PostgreSQL 14 adds several new features to help with monitoring and
observability, including the ability to [track the progress of `COPY` commands](https://www.postgresql.org/docs/14/progress-reporting.html#COPY-PROGRESS-REPORTING),
[write-ahead-log (WAL) activity](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-WAL-VIEW),
and [statistics on replication slots](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-SLOTS-VIEW).
Enabling [`compute_query_id`](https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-COMPUTE-QUERY-ID)
lets you uniquely track a query through several PostgreSQL features, including
[`pg_stat_activity`](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW),
[`EXPLAIN VERBOSE`](https://www.postgresql.org/docs/14/sql-explain.html), and
more.

PostgreSQL14には、[「COPY」コマンドの進行状況を追跡する](https://www.postgresql.org/docs/14/progress-reporting.html#COPY-PROGRESS-REPORTING)、[Write Ahead-log(WAL)アクティビティ](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-WAL-VIEW)、[レプリケーションスロットに関する統計情報](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-SLOTS-VIEW)など、監視と可観測性を支援するいくつかの新機能が追加されています。
「[compute_query_id](https://www.postgresql.org/docs/14/runtime-config-statistics.html#GUC-COMPUTE-QUERY-ID)」を有効にすると、「[pg_stat_activity](https://www.postgresql.org/docs/14/monitoring-stats.html#MONITORING-PG-STAT-ACTIVITY-VIEW)」や「[EXPLAIN VERBOSE](https://www.postgresql.org/docs/14/sql-explain.html)」など、PostgreSQLのいくつかの機能を使ってクエリを一意に追跡することができます。


### SQLの性能、標準準拠、利便性

クエリの計画と実行には、PostgreSQL14の拡張による利点があります。
このリリースでは、PostgreSQLのクエリ並列処理のサポートにいくつかの改善が加えられました。
具体的には、並列シーケンシャルスキャンのパフォーマンスの向上、「[PL/pgSQL](https://www.postgresql.org/docs/14/plpgsql.html)」が「RETURN QUERY」コマンドを使用して並列クエリを実行する機能、「[REFRESH MATERIALIZED VIEW](https://www.postgresql.org/docs/14/sql-refreshmaterializedview.html)」が並列クエリを実行する機能などです。
さらに、ネストされたループ結合を使用するクエリでは、PostgreSQL14で追加された追加のキャッシュによってパフォーマンスの利点が得られる可能性があります。

PostgreSQL14では、[拡張統計](https://www.postgresql.org/docs/14/planner-stats.html#PLANNER-STATS-EXTENDED)に式を使えるようになりました。
加えて、[ウィンドウ関数](https://www.postgresql.org/docs/14/functions-window.html)が、[PostgreSQL13](https://www.postgresql.org/about/news/postgresql-13-released-2077/)で導入された機能であるインクリメンタルソートの恩恵を受けられるようになりました。

コードのブロックでトランザクション制御ができる、[ストアドプロシージャ](https://www.postgresql.org/docs/14/sql-createprocedure.html)は、「OUT」パラメータでデータを返せるようになりました。

PostgreSQL14では、「[date_bin](https://www.postgresql.org/docs/14/functions-datetime.html#FUNCTIONS-DATETIME-BIN)」関数を使用して、特定の間隔にタイムスタンプをbinする(大箱に入れる)、あるいは、揃える、機能が導入されています。
このリリースでは、「[SEARCH](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-SEARCH)」および「[CYCLE](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-CYCLE)」句に準拠するSQLも追加されており、再帰的な[共通テーブル式](https://www.postgresql.org/docs/14/queries-with.html#QUERIES-WITH-RECURSIVE)の順序付けとサイクル検出に役立ちます。

### セキュリティ強化

PostgreSQL14では、「pg_read_all_data」と「pg_write_all_data」[事前定義されたロール](https://www.postgresql.org/docs/14/predefined-roles.html)を使用して、テーブル、ビュー、スキーマ上のユーザに対して読み取り専用と書き込み専用の権限を割り当てることができます。
さらに今回のリリースでは、標準に準拠した「[SCRAM-SHA-256](https://www.postgresql.org/docs/14/auth-password.html)」のパスワード管理と認証システムが新しいPostgreSQLインスタンスのデフォルトになりました。

### PostgreSQLについて

[PostgreSQL](https://www.postgresql.org)は世界で最も先進的なオープンソースデータベースであり、数千人のユーザー、貢献者、企業、組織からなるグローバルコミュニティが存在します。PostgreSQLは、カリフォルニア大学バークレー校で始まった30年以上のエンジニアリングの上に構築されたものですが、その開発ペースは他に類を見ません。PostgreSQLの成熟した機能セットは最上のプロプライエタリなデータベースシステムに匹敵するだけでなく、先進的なデータベース機能、拡張性、セキュリティ、および安定性においてそれらを上回ってもいます。

### リンク

* [ダウンロード](https://www.postgresql.org/download/)
* [リリースノート](https://www.postgresql.org/docs/14/release-14.html)
* [プレスキット](https://www.postgresql.org/about/press/)
* [セキュリティページ](https://www.postgresql.org/support/security/)
* [バージョンポリシー](https://www.postgresql.org/support/versioning/)
* [Twitter @postgresql フォロー](https://twitter.com/postgresql)
