# PostgreSQL 10 báo ra mắt

5 tháng 10 2017 - Tập đoàn Phát triển Toàn cầu PostgreSQL ngày hôm nay đã thông báo ra mắt PostgreSQL 10, phiên bản mới nhất của cơ sở dữ liệu nguồn mở tiên tiến nhất trên thế giới.

Một tính năng quan trọng của khối lượng công việc hiện đại là khả năng phân phối dữ liệu qua nhiều nút để tiếp cận, quản lý, và phân tích nhanh hơn, đồng thời được biết đến là chiến lược &quot;chia để trị&quot; Ra mắt PostgreSQL 10 gồm có những cải tiến quan trọng để thiết lập một cách có hiệu quả chiến lược chia để trị, bao gồm sao lưu logic gốc, phân chia bảng khai báo, và xử lý song song câu hỏi được cải thiện.

"Cộng đồng nhà phát triển của chúng tôi tập trung xây dựng các tính năng sẽ tận dụng các cài đặt cơ sở hạ tầng hiện đại để phân phối khối lượng công việc,"" Magnus Hagander nói, một [core team](https://www.postgresql.org/developer/core/) thành viên của [PostgreSQL Global Development Group](https://www.postgresql.org/). "Các tính năng như sao lưu logic và xử lý song song câu hỏi được cải thiện là kết quả của nhiều năm làm việc và minh chứng cho sự cống hiến liên tục của cộng đồng để bảo đảm sự dẫn đầu của Postgres khi các nhu cầu về công nghệ phát triển."

Sự ra mắt này cũng đánh dấu sự thay đổi của giản đồ phiên bản cho PostgreSQL sang định dạng "x.y". Điều này có nghĩa sự ra mắt với những thay đổi nhỏ lần tiếp theo của PostgreSQL sẽ là 10.1 và ra mắt với những thay đổi lớn sẽ là 11.

## Sao lưu logic - Bộ khung phát hành/đăng ký để phân phối dữ liệu

Sao lưu logic mở rộng các tính năng sao lưu hiện tại của PostgreSQL với khả năng gửi các hiệu chỉnh trên mỗi cơ sở dữ liệu và mỗi cấp độ trên bảng thành các cơ sở dữ liệu PostgreSQL khác nhau.  Người sử dụng hiện nay có thể tinh chỉnh dữ liệu sao lưu sang nhiều nhóm cơ sở dữ liệu khác nhau và sẽ có khả năng để thực hiện nâng cấp sang các phiên bản PostgreSQL với thay đổi lớn mà không có thời gian dừng.

"Chúng tôi đang sử dụng rất nhiều PostgreSQL kể từ phiên bản 9.3 và rất háo hức với phiên bản 10 bởi vì phiên bản này mang đến cơ sở cho sự phân chia đã chờ đợi từ lâu và sự sao lưu logic cài sẵn. Nó sẽ cho phép chúng ta sử dụng PostgreSQL trong nhiều các dịch vụ hơn nữa," Vladimir Borodin cho biết, Người đứng đầu nhóm DBA tại [Yandex](https://www.yandex.com/).

## Phân chia bảng khai báo - Sự tiện lợi trong phân chia dữ liệu của quý vị

Phân chia bảng đã tồn tại nhiều năm trong PostgreSQL nhưng yêu cầu người dùng phải duy trì một bộ các quy tắc không hề tầm thường và các bộ khởi động để phân chia làm việc. PostgreSQL 10 giới thiệu một cú pháp phân chia bảng cho phép người sử dụng có thể dễ dàng tạo và duy trì phạm vi và các bảng được phân chia theo danh sách. Bổ sung cú pháp phân chia là bước đầu tiên trong một chuỗi các tính năng được lập kế hoạch để cung cấp một bộ khung phân chia thiết thực trong PostgreSQL.

## Xử lý song song câu hỏi được cải thiện - Giải quyết nhanh chóng phân tích của quý vị

PostgreSQL 10 cung cấp hỗ trợ tốt hơn cho các câu hỏi song song bằng cách cho phép quá trình xử lý các phần của câu hỏi được song song hóa.  Sự cải tiến bao gồm các loại quét dữ liệu bổ sung được song song hóa cũng như tối ưu hóa khi dữ liệu được kết hợp lại, như sự sắp xếp từ trước. Những nâng cấp này cho phép các kết quả có thể quay trở lại nhanh chóng hơn.

## Cam kết số tối thiểu cần thiết cho Sao lưu đồng bộ - Phân bổ dữ liệu có độ tin cậy

PostgreSQL 10 giới thiệu cam kết số tối thiểu cần thiết cho Sao lưu đồng bộ, tạo sự linh hoạt trong cách thức một cơ sở dữ liệu sơ cấp nhận xác nhận các thay đổi đã thành công được viết cho các mô hình từ xa.  Người quản trị hiện nay có thể xác định nếu có bất cứ số lượng mô hình đã được xác nhận rằng có một sự thay đổi ở cơ sở dữ liệu đã được thực hiện, sau đó dữ liệu có thể được cân nhắc ghi một cách an toàn.

"Cam kết số tối thiểu cần thiết cho Sao lưu đồng bộ ở PostgreSQL 10 mang lại nhiều lựa chọn để mở rộng khả năng của chúng ta để thúc đẩy cơ sở hạ tầng dữ liệu có thời gian dừng gần bằng không từ phối cảnh ứng dụng. Điều này cho phép chúng ta triển khai và cập nhập cơ sở hạ tầng dữ liệu mà không phải chịu tình trạng cửa sổ bảo dưỡng kéo dài," Curt Micol nói, Kỹ sư Cơ sở dữ liệu nhân viên tại [Simple Finance](https://www.simple.com/).

## SCRAM-SHA-256 Sự xác thực - Bảo đảm sự tiếp cận dữ liệu của quý vị

Cơ chế xác thực phản ứng thách thức có kinh nghiệm (SCRAM) được định nghĩa trong [RFC5802](https://tools.ietf.org/html/rfc5802) định nghĩa là một giao thức để cải thiện lưu trữ bảo đảm và truyền dẫn mật khẩu bằng cách cung cấp một bộ khung để thương lượng mật khẩu mạnh. PostgreSQL 10 giới thiệu phương pháp xác thực SCRAM-SHA-256, định nghĩa trong [RFC7677](https://tools.ietf.org/html/rfc7677), để cung cấp bảo mật tốt hơn phương pháp xác thực mật khẩu dựa trên MD5 hiện tại.

Links
-----

* [Downloads](https://www.postgresql.org/downloads)
* [Press Kit](https://www.postgresql.org/about/press/presskit10)
* [Release Notes](https://www.postgresql.org/docs/current/static/release-10.html)
* [What's New in 10](https://wiki.postgresql.org/wiki/New_in_postgres_10)

Giới thiệu về PostgreSQL
----------------

PostgreSQL là một cơ sở dữ liệu nguồn mở tiên tiến nhất trên thế giới, với một cộng đồng toàn cầu gồm hàng nghìn người sử dụng, người đóng góp, các công ty và tổ chức.  Dự án PostgreSQL xây dựng trên 30 năm thiết kế, bắt đầu tại Trường Đại học California, Berkeley, và đã tiếp tục với một bước phát triển không gì có thể sánh kịp. Bộ tính năng hoàn thiện của PostgreSQL không chỉ phù hợp với các hệ thống cơ sở dữ liệu độc quyền, mà còn vượt xa về các tính năng cơ sở dữ liệu tiên tiến,  khả năng mở rộng, bảo mật và tính bền vững. Để biết thêm PostgreSQL và tham gia vào cộng đồng của chúng tôi hãy truy cập [PostgreSQL.org](https://www.postgresql.org).
