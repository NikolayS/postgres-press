# Rilasciato PostgreSQL 10

5 OTTOBRE 2017 - Il PostgreSQL Global Development Group ha annunciato oggi il rilascio di PostgreSQL 10, l'ultima versione del più avanzato database open source al mondo.

Una funzionalità critica richiesta dai moderni carichi di lavoro è l'abilità di distribuire dati su più nodi per renderne più veloce l'accesso, la gestione e l'analisi - conosciuta anche come strategia "divide et impera".
PostgreSQL 10 comprende miglioramenti significativi per implementare la strategia dividi e domina, fra cui la replica logica nativa, il partizionamento dichiarativo di tabelle e query parallele più potenti.

"La nostra comunità di sviluppatori si è concentrata sullo sviluppo di funzionalità che potessero sfruttare le moderne infrastrutture per workload distribuiti", afferma Magnus Hagander, membro del [core team](https://www.postgresql.org/developer/core/) del [PostgreSQL Global Development Group](https://www.postgresql.org/). "Funzionalità come replica logica e query parallele più potenti rappresentano anni di lavoro e testimoniano la dedizione della comunità nel garantire la posizione di leadership tecnologica di PostgreSQL."

Questa release inoltre segna il cambio dello schema di versionamento di PostgreSQL al formato "x.y". Ciò significa che la prossima minor release di PostgreSQL sarà la 10.1, mentre la prossima major release sarà la 11.

## Replica logica - Una piattaforma publish/subscribe per distribuire i dati

La replica logica estende le funzionalità di replica attualmente presenti in PostgreSQL con l'abilità di inviare modifiche a livello di singolo database o tabella ad altri database PostgreSQL. Gli utenti possono controllare i dati replicati su altri cluster di database, e avranno la possibilità di effettuare upgrade con downtime zero a future major release di PostgreSQL.

"Abbiamo usato estensivamente PostgreSQL fino dalla versione 9.3 e siamo molto eccitati per la versione 10, dato che incorpora il supporto per il tanto atteso partizionamento e per la replica logica. Ci permetterà di usare PostgreSQL in ancora più servizi," dice Vladimir Borodin, DBA Team Lead presso [Yandex](https://www.yandex.com/).

## Partizionamento dichiarativo di tabella - Semplicità nel dividere i tuoi dati

Il partizionamento di tabella esiste in PostgreSQL da molti anni, ma ha sempre richiesto agli utenti di mantenere un insieme di regole e trigger affinché il partizionamento potesse funzionare.
PostgreSQL 10 introduce una sintassi che semplifica la creazione e la gestione di tabelle partizionate per range o lista. L'aggiunta di questa sintassi è il primo passo di una serie di funzionalità pianificate per garantire nel lungo termine una robusta infrastruttura di partizionamento all'interno di PostgreSQL.

## Query parallele più potenti - Domina in modo veloce le tue analisi

PostgreSQL 10 fornisce un miglior supporto per le query parallele, permettendo a più parti del processo di esecuzione delle query di svolgersi in concorrenza. I miglioramenti comprendono nuovi tipi di scansione dei dati che sono eseguiti in parallelo, e ottimizzazioni in fase di aggregazione dei dati, come il pre-ordinamento. Queste funzionalità garantiscono risultati più veloci.

## Replica sincrona con commit basata su quorum - Distribuisci i dati in sicurezza

PostgreSQL 10 introduce il commit basato su quorum per la replica sincrona, che dà flessibilità al modo in cui il database primario riceve da parte delle repliche remote la conferma che i cambiamenti siano stati scritti con successo.
Un amministratore è adesso in grado di specificare che, qualora qualsiasi numero dei server standby confermi l'accettazione delle modifiche al database, i dati siano considerati scritti in modo sicuro.

"Il Quorum Commit per la replica sincrona in PostgreSQL 10 aumenta la nostra possibilità di promuovere l'infrastruttura di database con downtime vicino a zero per quel che riguarda le applicazioni. Questo ci consente di mettere in servizio e di aggiornare la nostra infrastruttura di database in continuazione, senza incorrere in lunghi tempi di manutenzione," dice Curt Micol, Staff Infrastructure Engineer presso [Simple Finance](https://www.simple.com/).  

## Autenticazione SCRAM-SHA-256 - Metti al sicuro l'accesso ai tuoi dati

SCRAM, acronimo di "Salted Challenge Response Authentication Mechanism", parte di [RFC5802](https://tools.ietf.org/html/rfc5802), definisce un protocollo per il miglioramento della memorizzazione e della trasmissione di password tramite l'implementazione di un framework per la negoziazione di password. PostgreSQL 10 introduce il metodo di autenticazione SCRAM-SHA-256, definito in [RFC7677](https://tools.ietf.org/html/rfc7677), per fornire maggior sicurezza del metodo di autenticazione di password basato su MD5.

Link
-----

* [Downloads](https://www.postgresql.org/downloads)
* [Press Kit](https://www.postgresql.org/about/press/presskit10)
* [Note di Rilascio](https://www.postgresql.org/docs/current/static/release-10.html)
* [Le novità di PostgreSQL 10](https://wiki.postgresql.org/wiki/New_in_postgres_10)

Su PostgreSQL
----------------

PostgreSQL è il principale sistema di gestione di database open source, con una comunità internazionale costituita da migliaia di utenti e sviluppatori nonché decine di aziende ed enti provenienti da tutte le parti del mondo. Il progetto PostgreSQL si porta dietro oltre 30 anni di attività di ingegneria del software, a partire dal campus di Berkeley dell'Università di California, ed oggi può vantare un ritmo di sviluppo senza uguali. La gamma di funzionalità mature messe a disposizione da PostgreSQL non soltanto è in grado di competere con quelle offerte da sistemi di database proprietari, ma le migliora in termini di funzionalità avanzate, estensibilità, sicurezza e stabilità. Scopri maggiori informazioni su PostgreSQL e partecipa attivamente alla nostra comunità su [PostgreSQL.org](https://www.postgresql.org) e, per l'Italia, [ITPUG](http://www.itpug.org).
