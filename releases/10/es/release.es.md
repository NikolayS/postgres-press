# PostgreSQL 10 Liberado

5 de octubre de 2017 — El Grupo Global de Desarrolladores de PostgreSQL
(PGDG) anunció hoy el lanzamiento de PostgreSQL 10, la versión más reciente de
la base de datos de código abierto más avanzada en el mundo.

Una característica crítica de las cargas de trabajo en la actualidad es la
capacidad de distribuir datos entre muchos nodos para un acceso, gestión y
análisis más rápidos, también conocido como "estrategia _divide y
vencerás_". La versión 10 de PostgreSQL incluye mejoras significativas para
implementar de forma efectiva la estrategia divide y vencerás, lo que incluye
replicación lógica nativa, particionamiento declarativo de tablas y mejoras de
consultas en paralelo.

"Nuestra comunidad de desarrolladores se centró en construir características
que aprovecharan la infraestructura moderna para distribuir cargas de trabajo",
dice Magnus Hagander, un miembro del [equipo
principal](https://www.postgresql.org/developer/core/) del [Grupo Global de
Desarrolladores de PostgreSQL](https://www.postgresql.org/). "Características
como la replicación lógica y las mejoras en la ejecución de consultas en
paralelo representan años de esfuerzo y demuestran la dedicación continua de la
comunidad a asegurar que Postgres siga llevando la delantera mientras las
demandas tecnológicas evolucionan".

Esta versión también marca el cambio del esquema de versionamiento para
PostgreSQL a un formato "x.y".  Esto significa que la próxima versión menor de
PostgreSQL será la 10.1 y la próxima versión mayor será la 11.

## Replicación lógica — Un framework basado en publicación/suscripción para distribución de datos

La replicación lógica extiende las funciones de replicación actuales de
PostgreSQL con la capacidad de enviar modificaciones a nivel de base de datos y
de tabla a diferentes bases de datos PostgreSQL.  Los usuarios ahora pueden
ajustar los datos replicados a varios clusters de base de datos y tendrán la
capacidad de realizar actualizaciones a futuras versiones mayores de PostgreSQL
sin la necesidad de una ventana de mantenimiento.

"Hemos usado intensivamente PostgreSQL desde 9.3 y estamos muy
emocionados por la versión 10 pues trae la base para el
particionamiento y la replicación lógica integrada tan esperados. Eso nos permitirá usar
PostgreSQL en aún más servicios", dice Vladimir Borodin, Director del equipo
DBA en [Yandex](https://www.yandex.com/).

## Particionamiento declarativo de tablas — Comodidad al dividir sus datos

El particionamiento de tablas ha existido por años en PostgreSQL, pero requería
que el usuario mantuviera un conjunto de triggers y reglas no triviales para que
el particionamiento funcione.  PostgreSQL 10 introduce sintaxis para el
particionamiento de tablas que les permite a los usuario crear y mantener
fácilmente tablas particionadas por lista y rango.  La adición de esta sintaxis
para particionamiento es el primer paso en una serie de características
planificadas para proveer un framework robusto de particionamiento dentro de
PostgreSQL.

## Mejoras en la ejecución de consultas en paralelo — Conquiste rápidamente su análisis

PostgreSQL 10 ofrece mejor soporte para consultas en paralelo permitiendo que
más partes del proceso de ejecución de consultas se realice en paralelo.  Las
mejoras incluyen tipos adicionales de escaneo de datos que se realizan en
paralelo así como optimizaciones cuando los datos se recombinan, como en el
pre-ordenamiento. Estas mejoras permiten obtener los resultados más rápido.

## Commit por quorum para replicación sincrónica — Distribuya datos con seguridad

PostgreSQL 10 introduce el commit por quorum para replicación sincrónica, lo
cual permite tener flexibilidad al momento de hacer que una base de datos
primaria esté al tanto de que se escribieron satisfactoriamente los cambios a
réplicas remotas.  Ahora un administrador puede especificar que si un número
determinado de réplicas ha confirmado que un cambio se ha hecho en la base de
datos, los datos pueden considerarse escritos con seguridad.

"El commit por quorum para replicación sincrónica en PostgreSQL 10 da más
opciones para extender nuestra capacidad de promover infraestructura de base de
datos con una ventana de mantenimiento casi imperceptible desde la perspectiva
de la aplicación. Esto nos permite implementar y actualizar continuamente
nuestra infraestructura de base de datos sin recurrir a largas ventanas de
mantenimiento", dice Curt Micol, Ingeniero de infraestructura en
[Simple Finance](https://www.simple.com/).

## Autenticación SCRAM-SHA-256 — Proteja el acceso a sus datos

El mecanismo de autenticación SCRAM (Salted Challenge Response Authentication
Mechanism) definido en [RFC5802](https://tools.ietf.org/html/rfc5802) define un
protocolo para avanzar a un almacenamiento y transmisión de contraseñas seguro
proveyendo un framework robusto para negociación de contraseñas. PostgreSQL 10
introduce el método de autenticación SCRAM-SHA-256, definido en
[RFC7677](https://tools.ietf.org/html/rfc7677), para ofrecer mayor
seguridad que el método de autenticación por contraseñas anterior, basado en
MD5.

Links
-----

* [Descargas](https://www.postgresql.org/downloads)
* [Comunicado de prensa](https://www.postgresql.org/about/press/presskit10)
* [Notas de la versión](https://www.postgresql.org/docs/current/static/release-10.html)
* [Qué hay de nuevo en 10](https://wiki.postgresql.org/wiki/New_in_postgres_10)

Sobre PostgreSQL
----------------

PostgreSQL es el sistema de bases de datos de código abierto más avanzado del
mundo, con una comunidad global de miles de usuario, contribuyentes, compañías
y organizaciones.  El Proyecto PostgreSQL se construye sobre más de 30 años de
ingeniería, empezando en la Universidad de California, Berkeley, y ha
continuado con un ritmo de desarrollo inigualable. El maduro conjunto de
características de PostgreSQL no sólo iguala a los sistemas de bases de datos
propietarios, sino que los supera en características avanzadas de bases de
datos, extensibilidad, seguridad y estabilidad.  Aprenda más sobre PostgreSQL y
participe en nuestra comunidad en [PostgreSQL.org](https://www.postgresql.org).
