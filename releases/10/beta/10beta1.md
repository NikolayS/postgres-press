# PostgreSQL 10 Beta 1 Released

The PostgreSQL Global Development Group announces today that the first beta release of PostgreSQL 10 is available for download. This release contains previews of all of the features which will be available in the final release of version 10, although some details will change before then.  Users are encouraged to begin testing their applications against this latest release.

## Major Features of 10

The new version contains multiple features that will allow users to both scale out and scale up their PostgreSQL infrastructure:

* Logical Replication: built-in option for replicating specific tables or using replication to upgrade
* Native Table Partitioning: range and list partitioning as native database objects
* Additional Query Parallelism: including index scans, bitmap scans, and merge joins
* Quorum Commit for Synchronous Replication: ensure against loss of multiple nodes

We have also made three improvements to PostgreSQL connections, which we are calling on driver authors to support, and users to test:

* SCRAM Authentication, for more secure password-based access
* Multi-host "failover", connecting to the first available in a list of hosts
* target_session_attrs parameter, so a client can request a read/write host

## Additional Features

Many other new features and improvements have been added to PostgreSQL 10, some of which may be as important, or more important, to specific users than the above.  Certainly all of them require testing. Among them are:

* Crash-safe and replicable Hash Indexes
* Multi-column Correlation Statistics
* New "monitoring" roles for permission grants
* Latch Wait times in pg_stat_activity
* XMLTABLE query expression
* Restrictive Policies for Row Level Security
* Full Text Search support for JSON and JSONB
* Compression support for pg_receivewal
* ICU collation support
* Push Down Aggregates to foreign servers
* Transition Tables in trigger execution

Further, developers have contributed performance improvements in the SUM() function, character encoding conversion, expression evaluation, grouping sets, and joins against unique columns. Analytics queries against large numbers of rows should be up to 40% faster. Please test if these are faster for you and report back.

See the [Release Notes](https://www.postgresql.org/docs/devel/static/release-10.html) for a complete list of new and changed features.

## Test for Bugs and Compatibility

We count on you to test the altered version with your workloads and testing tools in order to find bugs and regressions before the release of PostgreSQL 10. As this is a Beta, minor changes to database behaviors, feature details, and APIs are still possible. Your feedback and testing will help determine the final tweaks on the new features, so test soon. The quality of user testing helps determine when we can make a final release.

Additionally, version 10 contains several changes that are incompatible with prior major releases, particularly renaming "xlog" to "wal" and a change in version numbering. We encourage all users test it against their applications, scripts, and platforms as soon as possible.  See the [Release Notes](https://www.postgresql.org/docs/devel/static/release-10.html) and the [What's New in 10](https://wiki.postgresql.org/wiki/New_in_postgres_10) page for more details.

## Beta Schedule

This is the first beta release of version 10. The PostgreSQL Project will release additional betas as required for testing, followed by one or more release candidates, until the final release in late 2017. For further information please see the [Beta Testing](https://www.postgresql.org/developer/beta) page.

## Links

* [Downloads Page](https://www.postgresql.org/download/)
* [Beta Testing Information](https://www.postgresql.org/developer/beta)
* [10 Beta Release Notes](https://www.postgresql.org/docs/devel/static/release-10.html)
* [What's New in 10](https://wiki.postgresql.org/wiki/New_in_postgres_10)
* [10 Open Items](https://wiki.postgresql.org/wiki/PostgreSQL_10_Open_Items)
