# PostgreSQL 10 Sudah Dirilis

5 OCTOBER 2017 - PostgreSQL Global Development Group hari ini mengumumkan peluncuran PostgreSQL 10, versi terbaru dari database open source yang paling terdepan di dunia.

Fitur kritikal dari sebuah *workloads* modern adalah kemampuan untuk mendistribusikan data ke banyak simpul untuk mempercepat akses data, manajemen dan analisis yang dikenal juga sebagai strategi "membagikan dan menaklukkan"(*divide and conquer*). Peluncuran PostgreSQL 10 mencakup peningkatan yang signifikan dalam penerapan strategi membagikan dan menaklukkan (*divide and conquer*) secara efektif, termasuk replikasi logika yang tertanam, partisi table *declarative* dan pararelisasi *query* yang dimutakhirkan.

"Komunitas pengembang kami berfokus pada pengembangan fitur yang akan memanfaatkan pengaturan infrastruktur modern untuk mendistribusikan workloads" ujar Magnus Hagander, seorang anggota [tim inti](https://www.postgresql.org/developer/core/) [PostgreSQL Global Development Group](https://www.postgresql.org/). "Fitur seperti replikasi logikal dan pararelisme query mutakhir menggambarkan pekerjaan selama bertahun-tahun dan menunjukkan dedikasi yang berkesinambungan dari komunitas untuk memastikan kepemimpinan PostgreSQL seiring tuntutan perkembangan teknologi."

Rilis ini juga menandai perubahan skema pembuatan versi untuk PostgreSQL ke format "x.y". Ini berarti rilis minor PostgreSQL berikutnya akan menjadi 10.1 dan rilis besar berikutnya adalah 11

## Replikasi Logikal - Sebuah framework publikasi/subskripsi untuk pendistribusian data

Replikasi logikal memperluas fitur replikasi dari PostgreSQL saat ini dengan kemampuan untuk mengirim perubahan pada tingkat per-database dan per-tabel ke database PostgreSQL yang berbeda. Pengguna sekarang dapat menyempurnakan data yang direplikasi ke berbagai cluster database dan akan memiliki kemampuan untuk melakukan upgrade dengan zero-downtime ke versi PostgreSQL utama di kemudian hari.

"Kami telah banyak PostgreSQL secara besar-besaran sejak versi 9.3 dan sangat membuncah dengan kehadiran versi 10 karena menyajikan dasar untuk partisi yang telah lama ditunggu dan replikasi logikal yang tertanam pada PostgreSQL. Ini akan memungkinkan kami untuk menggunakan PostgreSQL bahkan di dalam lebih banyak lagi layanan," ujar Vladimir Borodin, Kepala Tim DBA di [Yandex](https://www.yandex.com/).  

## Deklarasi Tabel Partisi - Kenyamanan dalam membagi data Anda

Tabel partisi telah ada selama bertahun-tahun di PostgreSQL namun mengharuskan pengguna untuk mempertahankan beberapa rules dan triggers agar partisi dapat berfungsi. PostgreSQL 10 memperkenalkan sintaks partisi tabel yang memungkinkan pengguna membuat dan megelola list dan range partisi dengan mudah. Penambahan sintaks partisi adalah langkah pertama dalam serangkaian fitur yang direncanakan untuk menyediakan kerangka partisi yang kuat di dalam PostgreSQL.

## Peningkatan Pararelisme Query - Menaklukkan analisis Anda dengan cepat

PostgreSQL 10 memberikan dukungan yang lebih baik untuk paralelisasi query dengan memungkinkan lebih banyak bagian proses eksekusi query yang akan diparalelkan. Perbaikan mencakup tambahan jenis pemindaian data yang diparalelkan dan dioptimalkan saat data digabungkan, seperti pra-penyortiran. Penyempurnaan ini memungkinkan hasil query dapat dikembalikan lebih cepat.

## Quorum Commit untuk Replikasi Sinkron - Mendistribusikan data dengan percaya diri

PostgreSQL 10 memperkenalkan Quorum Commit untuk replikasi sinkron, yang memungkinkan fleksibilitas dalam bagaimana database utama menerima informasi bahwa perubahan berhasil ditulis ke replika jarak jauh. Administrator sekarang dapat menentukan bahwa jika sejumlah replika telah mengetahui bahwa perubahan pada database telah dilakukan, maka data dapat dianggap aman ditulis.

"Quorum Commit untuk melakukan replikasi sinkron di PostgreSQL 10 memberikan lebih banyak pilihan untuk memperluas kemampuan kita dalam mempromosikan infrastruktur database dengan hampir nol downtime dari perspektif aplikasi. Hal ini memungkinkan kita untuk terus menyebarkan dan memperbarui infrastruktur database kita tanpa menimbulkan waktu maintenance yang panjang," kata Curt Micol, Staff Infrastructure Engineer di [Simple Finance](https://www.simple.com/).

## Otentikasi SCRAM-SHA-256 - Amankan akses data Anda

Salted Challenge Response Authentication Mechanism (SCRAM) yang didefinisikan dalam [RFC5802](https://tools.ietf.org/html/rfc5802) mendefinisikan protokol untuk memperbaiki penyimpanan dan transmisi kata sandi yang aman dengan menyediakan kerangka kerja untuk negosiasi kata sandi yang kuat. PostgreSQL 10 memperkenalkan metode otentikasi SCRAM-SHA-256, yang didefinisikan dalam
[RFC7677](https://tools.ietf.org/html/rfc7677), untuk memberikan keamanan yang lebih baik daripada metode otentikasi kata sandi MD5 yang ada.

Tautan
-----

* [Downloads](https://www.postgresql.org/downloads)
* [Press Kit](https://www.postgresql.org/about/press/presskit10)
* [Release Notes](https://www.postgresql.org/docs/current/static/release-10.html)
* [What's New in 10](https://wiki.postgresql.org/wiki/New_in_postgres_10)

Tentang PostgreSQL
----------------

PostgreSQL adalah database open source paling maju di dunia, dengan komunitas global dari ribuan pengguna, kontributor, perusahaan dan organisasi. Proyek PostgreSQL dibangun lebih dari 30 tahun rekayasa, dimulai dari University of California, Berkeley, dan terus berlanjut dengan tahap perkembangan yang tak tertandingi. Fitur PostgreSQL  yang telah matang tidak hanya sesuai dengan pemilik yang memiliki sistem database terbaik, namun melebihi mereka dari segi fitur database canggih, kelayakan, keamanan dan stabilitas. Pelajari lebih lanjut tentang PostgreSQL dan berpartisipasi dalam komunitas kami di [PostgreSQL.org](https://www.postgresql.org).
