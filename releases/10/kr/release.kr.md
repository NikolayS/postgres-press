# PostgreSQL 10 출시

PostgreSQL 글로벌 개발 그룹에서는 오늘 2017년 10월 5일에 PostgreSQL 10의 출시를 공식 발표했습니다.

최근 워크로드 처리의 중요한 기능중 하나는 "divide and conquer" (분할 정복)로 알려진 데이터를 여러 노드로 분산을 통한 빠른 엑세스, 관리, 분석할 수 있는 기능입니다. PostgreSQL 10은 로지컬 리플리케이션, 선언적 테이블 파티셔닝 및 병렬처리 기능 개선을 포함한 분할 정복을 위한 상당한 기능 개선이 이루어졌습니다.

[PostgreSQL 글로벌 개발 그룹](https://www.postgresql.org/)의 [코어팀](https://www.postgresql.org/developer/core/) 개발자인 Magnus Hagander는 "우리 개발 커뮤니티는 워크로드 분산처리를 위한 모던 인프라스트럭처 구성을 위한 기능 개발에 중점을 두었습니다. 로지컬 리플리케이션이나 쿼리 병렬 처리 기능 개선 등은 PostgreSQL 기능 개선을 위한 다년간의 끊임없는 커뮤니티의 노력의 결과입니다."

이번 버전에서는 PostgreSQL의 버전 정보가 'x.y'로 변경되었습니다. 다음 마이너 릴리즈는 PostgreSQL 10.1이 될 것이고 다음 메이저 버전은 11이 됩니다.

## 로지컬 리플리케이션 - 분산 데이터를 위한 publish/subscribe (발행/구독) 프레임웍

로지컬 리플리케이션은 기존 PostgreSQL 리플리케이션 기능의 확장으로 다른 PostgreSQL 데이터베이스로 데이터베이스 단위 또는 테이블 단위의 변경 사항을 전송합니다. 사용자는 PostgreSQL 메이저 버전의 제로 다운타임 업그레이드를 실행할 수 있게 되었습니다.

"우리는 PostgreSQL 9.3 버전부터 사용한 헤비 유저로 오랜 기간 기다려온 파티셔닝과 로지컬 리플리케이션이 가능한 10 버전에 매우 기대가 큽니다. 이는 PostgreSQL로 더 많은 서비스를 확대할 수 있음을 나타냅니다." - Vladimir Borodin, DBA Team Lead at [Yandex](https://www.yandex.com/)

## 선언적 테이블 파티셔닝 - 편리한 데이터 분할 저장

테이블 파티셔닝은 PostgreSQL에서 이미 오래전부터 지원되던 기능이었습니다. 하지만 구성, 관리를 위한 많은 부가 작업이 수반되었습니다. PostgreSQL 10에서는 레인지, 리스트 파티션을 구현할 수 있는 구문이 소개되었습니다. 테이블 파티셔닝 구문의 추가는 더욱 강력해질 파티셔닝 프레임웍의 개발 계획의 첫 단계입니다.

## 병렬 처리 개선 - 신속한 데이터 분석

PostgreSQL 10은 쿼리 실행 프로세스의 더 많은 부분을 병렬 처리할 수 있도록 함으로써 병렬화된 쿼리를 보다 잘 지원합니다. 사전 정렬과 같이 데이터가 재결합될 때 병렬화되는 추가 유형의 데이터 스캔과 최적화가 개선되었습니다. 이러한 향상된 기능을 통해 쿼리 성능을 개선할 수 있습니다.

## 동기화 리플리케이션의 Quorum Commit - 높은 신뢰도의 데이터 분산

PostgreSQL 10에서는 동기식 리플리케이션에서 quorum commit을 지원합니다. 이는 마스터 데이터베이스의 변경 사항이 원격 복제본에 성공적으로 기록되었음을 확인하는 방법에 유연성을 제공합니다. 관리자는 데이터베이스의 변경이 확인된 복제본이 여러개 있으면 데이터가 안전하게 작성되었다고 간주할 수 있음을 지정할 수 있습니다.

PostgreSQL 10의 동기식 복제를 위한 quorum commit은 애플리케이션 관점에서 거의 제로 다운타임으로 데이터베이스 인프라를 향상시키는 능력을 확장할 수 있는 더 많은 옵션을 제공합니다. 이를 통해 우리는 긴 유지 관리 기간없이 데이터베이스 인프라를 지속적으로 배치 및 업데이트 할 수 있습니다. - Curt Micol, Staff Infrastructure Engineer at [Simple Finance](https://www.simple.com/)

## SCRAM-SHA-256 인증 - 인증 강화

SCRAM (Salted Challenge Response Authentication Mechanism)은 [RFC5802](https://tools.ietf.org/html/rfc5802)에 강력한 암호 인증을 위한 프레임 워크를 제공하여 암호의 안전한 저장 및 전송을 향상시키는 프로토콜을 정의합니다. PostgreSQL 10은 기존의 MD5 기반 암호 인증 방법보다 향상된 보안을 제공하기 위해 [RFC7677](https://tools.ietf.org/html/rfc7677)에 정의 된 SCRAM-SHA-256 인증 방법을 도입했습니다.

관련 자료
-----

* [Downloads](https://www.postgresql.org/downloads)
* [Press Kit](https://www.postgresql.org/about/press/presskit10)
* [Release Notes](https://www.postgresql.org/docs/current/static/release-10.html)
* [What's New in 10](https://wiki.postgresql.org/wiki/New_in_postgres_10)

PostgreSQL에 대하여
----------------

PostgreSQL은 수천명의 사용자, 공헌자, 회사 및 조직의 글로벌 커뮤니티가 있는 세계에서 가장 진보 된 오픈 소스 데이터베이스입니다. PostgreSQL 프로젝트는 캘리포니아 버클리 (Berkeley) 대학에서 시작하여 30 년이 넘는 엔지니어링을 기반으로하며 탁월한 개발 속도로 계속되었습니다. PostgreSQL의 성숙한 기능 세트는 상용 데이터베이스 시스템과 일치할 뿐만 아니라 고급 데이터베이스 기능, 확장성, 보안 및 안정성 면에서도 뛰어납니다. PostgreSQL에 대해 더 자세히 알아보고 [PostgreSQL.org](https://www.postgresql.org)에서 커뮤니티에 참여하세요.
