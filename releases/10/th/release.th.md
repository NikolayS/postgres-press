# เปิดตัว PostgreSQL 10

5 ตุลาคม 2017 - วันนี้ PostgreSQL Global Development Group ได้แถลงข่าวการวางจำหน่าย PostgreSQL 10 ซึ่งเป็นเวอร์ชั่นล่าสุดของฐานข้อมูลโอเพ่นซอร์สที่ก้าวหน้าที่สุดในโลก

คุณสมบัติที่สำคัญของการจัดการกับข้อมูลจำนวนมาก คือ ความสามารถในการกระจายข้อมูลไปยังสถานีเชื่อมโยงจำนวนมาก เพื่อการเข้าถึง การจัดการ และการวิเคราะห์ที่รวดเร็วขึ้น  การเปิดตัว PostgreSQL 10 ยังครอบคลุมถึงการพัฒนาให้สามารถใช้กลยุทธิ์การแบ่งและการพิชิตข้อมูล รวมถึงการจำลองตรรกะดั้งเดิม การแบ่งตารางที่เปิดเผย และการสอดคล้องของการสืบค้นที่พัฒนาขึ้น

"ชุมชนนักพัฒนาของเรามุ่งเน้นที่การสร้างคุณสมบัติใหม่ที่สามารถใช้ประโยชน์จากการติดตั้งโครงสร้างอันทันสมัยเพื่อกระจายปริมาณข้อมูล"  Magnus Hagander ซึ่งเป็นสมาชิกทีมหลัก (https://www.postgresql.org/developer/core/) ของ [PostgreSQL Global Development Group](https://www.postgresql.org/) กล่าว "คุณสมบัติอย่างเช่น การจำลองตรรกะ และการสอดคล้องของคำสืบค้นนั้น ใช้เวลาในการพัฒนาหลายปี แสดงให้เห็นถึงการทุ่มเทอย่างต่อเนื่องของชุมชนเพื่อให้มั่นใจได้ว่า Postgres ยังคงเป็นผู้นำในการตอบสนองต่อความต้องการทางด้านเทคโนโลยีอยู่เสมอ "

การเปิดตัวครั้งนี้ยังได้เน้นย้ำถึงการเปลี่ยนแปลงรูปแบบเวอร์ชั่นของ PostgreSQL ไปเป็นฟอร์แมต "x.y"  ซึ่งหมายความว่าการเปิดตัวครั้งหน้าแบบย่อมๆ ของ PostgreSQL จะเป็น 10.1 และครั้งใหญ่ในคราวถัดไปก็คือ 11

## การจำลองตรรกะ - กรอบการเผยแพร่และลงทะเบียนสำหรับการกระจายข้อมูล   

การจำลองตรระกะได้ขยายคุณสมบัติการจำลองในปัจจุบันของ PostgreSQL ด้วยความสามารถในการส่งการแก้ไขปรับปรุงในระดับฐานข้อมูลและตารางไปยังฐานข้อมูลต่างๆ ของ PostgreSQL   ปัจจุบันผู้ใช้สามารถแก้ไขปรับปรุงข้อมูลที่จำลองไปยังคลัสเตอร์ฐานข้อมูลที่หลากหลาย และจะสามารถอัพเกรดไปยังเวอร์ชั่นหลักๆ ในอนาคตของ PostgreSQL ได้อย่างต่อเนื่องไม่สะดุด

"เราได้ใช้ PostgreSQL เยอะมากตั้งแต่ 9.3 และรู้สึกตื่นเต้นมากกับเวอร์ชั่น 10 เพราะเป็นจุดเริ่มต้นของแบ่งข้อมูลที่รอกันมานาน และการจำลองตรรกะที่ติดตั้งไว้ในตัว  ยังทำให้เราสามารถใช้ PostgreSQL ในบริการอื่นๆ ได้มากขึ้นด้วย" Vladimir Borodin หัวหน้าทีม DBA ที่ [Yandex]  (https://www.yandex.com/) กล่าว

## การแบ่งตารางที่เปิดเผย - ความสะดวกในการแบ่งข้อมูลของคุณ

การแบ่งตารางมีอยู่ใน  PostgreSQL มาหลายปีแล้ว แต่ผู้ใช้ยังต้องใช้กฎสำคัญๆ และการกระตุ้นเพื่อให้การแบ่งทำงาน  PostgreSQL 10 ได้สร้างรูปแบบการเขียนภาษาในการแบ่งตารางที่ช่วยให้ผู้ใช้สามารถสร้าง รักษาความแตกต่างระหว่างจำนวนสูงสุดและต่ำสุด และลิสต์รายการตารางที่ถูกแบ่งได้อย่างง่าย  นอกจากรูปแบบการเขียนภาษาในการแบ่งข้อมูลแล้ว ยังถือเป็นก้าวแรกของชุดคุณลักษณะที่ได้วางแผนไว้เพื่อพัฒนากรอบการแบ่งข้อมูลที่แข็งแกร่งของ PostgreSQL  

## การสอดคล้องของการสืบค้นที่พัฒนาขึ้น  - การวิเคราะห์ที่รวดเร็ว  

PostgreSQL 10 มีการสนับสนุนการสืบค้นที่สอดคล้องด้วยการเปรียบเทียบกระบวนการสืบค้นหลายๆ ส่วนที่เพิ่มมากขึ้น  การพัฒนายังรวมถึงการสแกนประเภทข้อมูลที่มากขึ้น เช่นเดียวกับการหาค่าที่เหมาะสมที่สุดเมื่อข้อมูลอยู่รวมกัน เช่น การเรียงลำดับข้อมูล การพัฒนานี้ช่วยให้ผลการสืบค้นปรากฏเร็วขึ้น

## การใช้ Quorum เพื่อการจำลองซิงโครไนซ์ - แบ่งข้อมูลอย่างมั่นใจ  

PostgreSQL 10 ได้สร้างการจำลองที่ซิงโครไนซ์ ซึ่งทำให้มีความยืดหยุ่นให้การที่ฐานข้อมูลหลักสามารถรับข้อมูลเกี่ยวกับการเปลี่ยนแปลงที่บันทึกยังสำเนาที่อยู่ห่างออกไป  ต่อไปนี้แอดมินสามารถกำหนดได้ว่าหากสำเนาใดก็ตามได้รับข้อมูลการเปลี่ยนแปลงฐานข้อมูลที่เกิดขึ้น ข้อมูลนั้นถือว่าถูกเขียนอย่างปลอดภัยแล้ว

"การใช้ Quorum เพื่อการจำลองซิงโครไนซ์ใน PostgreSQL 10 ได้เพิ่มทางเลือกในการขยายขีดความสามารถของโครงสร้างฐานข้อมูลให้เกือบเป็นไปได้อย่างต่อเนื่องแบบไม่มีสะดุด  นี่ยังช่วยให้เราสามารถใช้งานและอัพเดทโครงสร้างฐานข้อมูลของเราโดยไม่ต้องเปิดวินโดว์บำรุงรักษาเป็นเวลานาน" Curt  Micol วิศวกรด้านโครงสร้างพนักงาน ที่ [Simple Finance](https://www.simple.com/) กล่าว  

## การพิสูจน์ตัวตน SCRAM-SHA-256  - รักษาความปลอดภัยในการเข้าถึงข้อมูลของคุณ  

Salted Challenge Response Authentication Mechanism (SCRAM) ที่ระบุอยู่ใน [RFC5802](https://tools.ietf.org/html/rfc5802) หมายถึงโปรโตคอลที่พัฒนาเพื่อการเก็บข้อมูลความปลอดภัยและการส่งผ่านรหัสผ่าน ด้วยการใช้กรอบการต่อรองรหัสผ่านที่เข้มแข็ง PostgreSQL 10 ได้สร้างเครื่องมือพิสูจน์ตัวตน SCRAM-SHA-256 ที่ระบุใน [RFC7677](https://tools.ietf.org/html/rfc7677) เพื่อสร้างความปลอดภัยที่มากกว่าเครื่องมือพิสูจน์ตัวตนรหัสผ่าน MD5 ที่มีอยู่

ลิงค์
-----

* [ดาวน์โหลด](https://www.postgresql.org/downloads)
* [ข่าวเผยแพร่](https://www.postgresql.org/about/press/presskit10)
* [ข้อความข่าว](https://www.postgresql.org/docs/current/static/release-10.html)
* [มีอะไรใหม่ใน 10](https://wiki.postgresql.org/wiki/New_in_postgres_10)

เกี่ยวกับ PostgreSQL
----------------

PostgreSQL คือฐานข้อมูลโอเพ่นซอร์สที่ก้าวหน้าที่สุดในโลก ประกอบด้วยชุมชนผู้ใช้ ผู้จำหน่าย บริษัท และองค์กรทั่วโลกหลายพันชุมชน  โครงการของ PostgreSQL เกิดขึ้นโดยพื้นฐานทางวิศวกรรมกว่า 30 ปี เริ่มที่มหาวิทยาลัยแคลิฟอร์เนีย เบิร์คลีย์  จากนั้นก็พัฒนาก้าวไปเรื่อยๆ อย่างไม่มีใครตามทันได้ จุดเด่นหลักของ PostgreSQL ไม่เพียงแต่มีระบบฐานข้อมูลที่สุดยอดที่สุดเท่านั้น แต่ยังมีการปฏิบัติการฐานข้อมูลที่ก้าวหน้า มีความสามารถในการขยายขีดความสามารถ การรักษาความปลอดภัย และการรักษาเสถียรภาพด้วย ศึกษาข้อมูลเพิ่มเติมเกี่ยวกับ PostgreSQL และมีส่วนร่วมกับชุมชนของเราได้ที่  [PostgreSQL.org](https://www.postgresql.org).
