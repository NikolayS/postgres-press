29 DE SETEMBRO DE 2016: PostgreSQL 9.6, a última versão do líder mundial em banco de dados SQL de código aberto, foi lançada hoje pelo Grupo de Desenvolvimento Global do PostgreSQL. Esta versão permitirá a usuários escalarem verticalmente e horizontalmente cargas de trabalho que exigem alta performance. Novas funcionalidades incluem consulta paralela, melhorias na replicação síncrona, busca de frase e melhorias na performance e usabilidade.

Escalar Verticalmente com Consulta Paralela
-------------------------------------------

A versão 9.6 adiciona suporta a paralelização de algumas operações da consulta, habilitando a utilização de vários ou todos os cores de um servidor para retornar os resultados da consulta rapidamente. Esta versão inclui busca sequencial (de tabela), agregação e junções paralelas. Dependendo dos detalhes e da quantidade de cores disponíveis, o paralelismo pode acelerar consultas de big data em até 32 vezes mais rápido.

"Eu migrei toda nossa plataforma de dados genômicos - todos os 25 bilhões de registros legados do MySQL - para um único banco de dados Postgres, aproveitando as habilidades de compressão do tipo de dados JSONB e os excelentes métodos de indexação GIN, BRIN e B-tree. Agora com a versão 9.6, eu espero aproveitar a funcionalidade de consulta paralela para permitir uma maior escalabilidade para consultas nas nossas tabelas grandes", disse Mike Sofen, Arquiteto Chefe de Banco de Dados, Synthetic Genomics.

Escalar Horizontalmente com Replicação Síncrona e postgres_fdw
--------------------------------------------------------------

Duas novas opções foram adicionadas a funcionalidade de replicação síncrona do PostgreSQL que permite utilizá-lo para manter leituras consistentes entre agrupamentos de bancos de dados. Em primeiro lugar, ele permite configurar grupos de réplicas síncronas. Em segundo lugar, o modo &quot;remote_apply&quot; cria uma visão consistente dos dados entre os múltiplos nós. Essas funcionalidades suportam o uso de replicação integrada para manter um conjunto de nós &quot;idênticos&quot; para cargas de trabalho de leitura com balanceamento.

O driver de federação de dados PostgreSQL-para-PostgreSQL, postgres_fdw, tem novas funcionalidades para executar o trabalho em servidores remotos. Ao &quot;incumbir a outros servidores&quot;, ordenações, junções e atualizações de dados em lote, usuários podem distribuir carga de trabalho entre vários servidores PostgreSQL. Essas funcionalidades devem em breve serem adicionadas a outros drivers FDW.

"Com a capacidade de fazer JOIN, UPDATE e DELETE remotos, Adaptadores de Dados Externos são agora uma solução completa para compartilhamento de dados entre outros bancos de dados e o PostgreSQL. Por exemplo, PostgreSQL pode ser utilizado para manipular a entrada de dados de dois ou mais tipos diferentes de bancos de dados", disse Julyanto Sutandang, Diretor de Soluções de Negócios na Equnix.

Melhor Busca Textual com Frases
-------------------------------

A funcionalidade de busca textual do PostgreSQL agora suporta "busca de frase". Isto permite que usuários busquem frases exatas, ou por palavras com uma proximidade específica uma da outra, utilizando índices GIN rápidos. Combinando com novas funcionalidades para o ajuste fino de opções de busca textual, o PostgreSQL é uma opção superior para "busca híbrida" que junta busca relacional, JSON e busca textual.

Mais Suave, Rápido e Fácil de Usar
----------------------------------

Graças aos comentários e testes dos usuários do PostgreSQL com bancos de dados de alto volume em produção, o projeto foi capaz de melhorar muitos aspectos de performance e usabilidade nesta versão. Replicação, agregação, indexação, ordenação e procedimentos armazenados se tornaram mais eficientes, e PostgreSQL agora faz melhor uso dos recursos com versões recentes do kernel do Linux. A sobrecarga administrativa para tabelas grandes e cargas de trabalho complexas também foram reduzidas, especialmente por melhorias no VACUUM.

Links
-----

* [Onde Baixar](https://www.postgresql.org/downloads)
* [Kit de Imprensa](https://www.postgresql.org/about/press/presskit96)
* [Notas de Lançamento](https://www.postgresql.org/docs/current/static/release-9-6.html)
* [O Que Há de Novo na 9.6](https://wiki.postgresql.org/wiki/NewIn96)

Sobre PostgreSQL
----------------

PostgreSQL é o sistema de banco de dados mais avançado do mundo, com uma comunidade global de milhares de usuários e colaboradores e dezenas de empresas e organizações. O Projeto PostgreSQL foi construído ao longo de 25 anos de engenharia, iniciando na Universidade da Califórnia, Berkeley, e tem um ritmo inigualável de desenvolvimento hoje. Conjunto de funcionalidades maduras do PostgreSQL não só se igualam aos principais sistemas de bancos de dados proprietários, mas os supera em funcionalidades avançadas, extensibilidade, segurança e estabilidade. Saiba mais sobre o PostgreSQL e participe da nossa comunidade em [PostgreSQL.org](https://www.postgresql.org).
