October 13, 2022 - The PostgreSQL Global Development Group today announced the
release of [PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html),
the latest version of the world’s
[most advanced open source database](https://www.postgresql.org/).

PostgreSQL 15 builds on the performance improvements of recent releases with
noticeable gains for managing workloads in both local and distributed
deployments, including improved sorting. This release improves the developer
experience with the addition of the popular
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) command, and adds
more capabilities for observing the state of the database.

"The PostgreSQL developer community continues to build features that simplify
running high performance data workloads while improving the developer
experience," said Jonathan Katz, a PostgreSQL Core Team member. "PostgreSQL 15
highlights how, through open software development, we can deliver to our users a
database that is great for application development and safe for their critical
data."

[PostgreSQL](https://www.postgresql.org), an innovative data management system
known for its reliability and robustness, benefits from over 25 years of open
source development from a [global developer community](https://www.postgresql.org/community/)
and has become the preferred open source relational database for organizations
of all sizes.

### Improved Sort Performance and Compression

In this latest release, PostgreSQL improves on its in-memory and on-disk
[sorting](https://www.postgresql.org/docs/15/queries-order.html) algorithms,
with benchmarks showing speedups of 25% - 400% based on which data types are
sorted. Using `row_number()`, `rank()`, `dense_rank()`, and `count()` as
[window functions](https://www.postgresql.org/docs/15/functions-window.html)
also have performance benefits in PostgreSQL 15. Queries using
[`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT)
can now be
[executed in parallel](https://www.postgresql.org/docs/15/parallel-query.html).

Building on work from the [previous PostgreSQL release](https://www.postgresql.org/about/press/presskit14/)
for allowing async remote queries, the
[PostgreSQL foreign data wrapper](https://www.postgresql.org/docs/15/postgres-fdw.html),
[`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html),
now supports
[asynchronous commits](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7).

The performance improvements in PostgreSQL 15 extend to its archiving and backup
facilities. PostgreSQL 15 adds support for LZ4 and Zstandard (zstd)
[compression to write-ahead log (WAL) files](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION),
which can have both space and performance benefits for certain workloads. On
certain operating systems, PostgreSQL 15 adds support to
[prefetch pages referenced in WAL](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH)
to help speed up recovery times. PostgreSQL's built-in backup command,
[`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html), now
supports server-side compression of backup files with a choice of gzip, LZ4, and
zstd. PostgreSQL 15 includes the ability to use
[custom modules for archiving](https://www.postgresql.org/docs/15/archive-modules.html),
which eliminates the overhead of using a shell command.

### Expressive Developer Features

PostgreSQL 15 includes the SQL standard
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) command.
`MERGE` lets you write conditional SQL statements that can include `INSERT`,
`UPDATE`, and `DELETE` actions within a single statement.

This latest release adds
[new functions for using regular expressions](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP)
to inspect strings: `regexp_count()`, `regexp_instr()`, `regexp_like()`, and
`regexp_substr()`. PostgreSQL 15 also extends the `range_agg` function to
aggregate
[`multirange` data types](https://www.postgresql.org/docs/15/rangetypes.html),
which were introduced in the
[previous release](https://www.postgresql.org/about/press/presskit14/).

PostgreSQL 15 lets users
[create views that query data using the permissions of the caller, not the view creator](https://www.postgresql.org/docs/15/sql-createview.html).
This option, called `security_invoker`, adds an additional layer of protection
to ensure that view callers have the correct permissions for working with the
underlying data.

### More Options with Logical Replication

PostgreSQL 15 provides more flexibility for managing
[logical replication](https://www.postgresql.org/docs/15/logical-replication.html).
This release introduces
[row filtering](https://www.postgresql.org/docs/15/logical-replication-row-filter.html)
and
[column lists](https://www.postgresql.org/docs/15/logical-replication-col-lists.html)
for
[publishers](https://www.postgresql.org/docs/15/logical-replication-publication.html),
letting users choose to replicate a subset of data from a table. PostgreSQL 15
adds features to simplify
[conflict management](https://www.postgresql.org/docs/15/logical-replication-conflicts.html),
including the ability to skip replaying a conflicting transaction and to
automatically disable a subscription if an error is detected. This release also
includes support for using two-phase commit (2PC) with logical replication.

### Logging and Configuration Enhancements

PostgreSQL 15 introduces a new logging format:
[`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG).
This new format outputs log data using a defined JSON structure, which allows
PostgreSQL logs to be processed in structured logging systems.

This release gives database administrators more flexibility in how users can
manage PostgreSQL configuration, adding the ability to grant users permission to
alter server-level configuration parameters. Additionally, users can now search
for information about configuration using the `\dconfig` command from the
[`psql`](https://www.postgresql.org/docs/15/app-psql.html) command-line tool.

### Other Notable Changes

PostgreSQL
[server-level statistics](https://www.postgresql.org/docs/15/monitoring-stats.html)
are now collected in shared memory, eliminating both the statistics collector
process and periodically writing this data to disk.

PostgreSQL 15 makes it possible to make an
[ICU collation](https://www.postgresql.org/docs/15/collation.html) the default
collation for a cluster or an individual database.

This release also
adds a new built-in extension,
[`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html), that
lets users inspect the contents of write-ahead log files directly from a SQL
interface.

PostgreSQL 15 also
[revokes the `CREATE` permission from all users](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)
except a database owner from the `public` (or default) schema.

PostgreSQL 15 removes both the long-deprecated "exclusive backup" mode and
support for Python 2 from PL/Python.

### About PostgreSQL

[PostgreSQL](https://www.postgresql.org) is the world's most advanced open
source database, with a global community of thousands of users, contributors,
companies and organizations. Built on over 35 years of engineering, starting at
the University of California, Berkeley, PostgreSQL has continued with an
unmatched pace of development. PostgreSQL's mature feature set not only matches
top proprietary database systems, but exceeds them in advanced database
features, extensibility, security, and stability.

### Links

* [Download](https://www.postgresql.org/download/)
* [Release Notes](https://www.postgresql.org/docs/15/release-15.html)
* [Press Kit](https://www.postgresql.org/about/press/)
* [Security Page](https://www.postgresql.org/support/security/)
* [Versioning Policy](https://www.postgresql.org/support/versioning/)
* [Follow @postgresql on Twitter](https://twitter.com/postgresql)

## More About the Features

For explanations of the above features and others, please see the following
resources:

* [Release Notes](https://www.postgresql.org/docs/15/release-15.html)
* [Feature Matrix](https://www.postgresql.org/about/featurematrix/)

## Where to Download

There are several ways you can download PostgreSQL 15, including:

* The [Official Downloads](https://www.postgresql.org/download/) page, with contains installers and tools for [Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/), and more.
* [Source Code](https://www.postgresql.org/ftp/source/v15.0)

Other tools and extensions are available on the
[PostgreSQL Extension Network](http://pgxn.org/).

## Documentation

PostgreSQL 15 comes with HTML documentation as well as man pages, and you can also browse the documentation online in both [HTML](https://www.postgresql.org/docs/15/) and [PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf) formats.

## Licence

PostgreSQL uses the [PostgreSQL License](https://www.postgresql.org/about/licence/),
a BSD-like "permissive" license. This
[OSI-certified license](http://www.opensource.org/licenses/postgresql/) is
widely appreciated as flexible and business-friendly, since it does not restrict
the use of PostgreSQL with commercial and proprietary applications. Together
with multi-company support and public ownership of the code, our license makes
PostgreSQL very popular with vendors wanting to embed a database in their own
products without fear of fees, vendor lock-in, or changes in licensing terms.

## Contacts

Website

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

## Images and Logos

Postgres, PostgreSQL, and the Elephant Logo (Slonik) are all registered
trademarks of the [PostgreSQL Community Association of Canada](https://www.postgres.ca).
If you wish to use these marks, you must comply with the [trademark policy](https://www.postgresql.org/about/policies/trademarks/).

## Corporate Support

PostgreSQL enjoys the support of numerous companies, who sponsor developers,
provide hosting resources, and give us financial support. See our
[sponsors](https://www.postgresql.org/about/sponsors/) page for some of these
project supporters.

There is also a large community of
[companies offering PostgreSQL Support](https://www.postgresql.org/support/professional_support/),
from individual consultants to multinational companies.

If you wish to make a financial contribution to the PostgreSQL Global
Development Group or one of the recognized community non-profit organizations,
please visit our [donations](https://www.postgresql.org/about/donate/) page.
