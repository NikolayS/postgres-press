6 Oktober 2022 - PostgreSQL Global Development Group presenterade idag
[PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html),
den senaste versionen av
[världens mest avancerade databas byggd med öppen källkod](https://www.postgresql.org/).

PostgreSQL 15 bygger vidare på prestandaförbättringarna i tidigare versioner
och förbättrar hantering av både lokala och distribuerade
installationer, bland annat med förbättrad sortering av data. 
För utvecklare kommer det populära
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) kommandot, och
bättre funktionalitet för att inspektera och övervaka databasen.

"PostgreSQL:s utvecklargrupp fortsätter att bygga funktioner som förenklar
for högpresterande arbetsbelastningar samtidigt som utvecklarupplevelsen
förbättras" säger Jonathan Katz, medlem i PostgreSQL Core Team.
"PostgreSQL 15 gör det uppenbart hur vi, genom en öppen utvecklingsmodell, kan
leverera en databas till våra användare som är utmärkt för applikationsutveckling
samtidigt som den är säker och pålitlig för viktig data."

[PostgreSQL](https://www.postgresql.org), ett innovativt databassystem känt
för sin pålitlighet, har utvecklats med öppen källkod i mer än 25 år av
[utvecklare från hela världen](https://www.postgresql.org/community/).
PostgreSQL är den databas som organisationer av alla storlekar föredrar.


### Förbättrad prestanda för sortering och komprimering

Denna versionen av PostgreSQL förbättrar algoritmerna för
[sortering](https://www.postgresql.org/docs/15/queries-order.html)
i minnet
och på disk, med hastighetsökningar uppmätta till 25% - 400% 
beroende på vilken datatyp som används.
Prestandan vid användning av `row_number()`, `rank()`, `dense_rank()`, och `count()` som
[fönsterfunktioner](https://www.postgresql.org/docs/15/functions-window.html)
har också förbättrats i PostgreSQL 15.

Funktionen för asynkrona frågor mot fjärrdatabaser som lanserades i 
[förra versionen av PostgreSQL](https://www.postgresql.org/about/press/presskit14/)
har nu utarbetats, och som följd stödjer nu
[`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html)
[asynkron commit](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7).

Prestanda för arkivering och säkerhetskopiering har också förbättrats i PostgreSQL 15.
Stöd för LZ4 och Zstandard (zstd)
[komprimering av write-ahead logg (WAL) filer](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION) har adderats
vilket både kan minsta utrymmesanvändning och öka hastighet beroende på belastning.
Snabbare återställning kan på utvalda operativsystem ske via 
[förinläsning av disksidor som WAL refererar till](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH).
PostgreSQL:s inbyggda kommando för säkerhetskopiering,
[`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html),
stödjer nu komprimering på servern med gzip, LZ4 och zstd. PostgreSQL 15 har
även stöd för
[specialiserade arkiveringsmoduler](https://www.postgresql.org/docs/15/archive-modules.html)
vilka ger bättre systemanvändning än skalkommandon.

### Utvecklarfunktioner

PostgreSQL 15 inkluderar det SQL standardiserade 
[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) kommandot.
`MERGE` ger möjlighet att skriva SQL-satser med villkor som kan inkludera
`INSERT`, `UPDATE` och `DELETE` kommandon i en enda sats.

[Nya funktioner för användande av reguljära uttryck](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP)
för att arbeta med textsträngar har lagts till:
`regexp_count()`, `regexp_instr()`, `regexp_like()` och
`regexp_substr()`. PostgreSQL 15 utökar också `range_agg` funktionen för
aggregering av
[`multirange` data typer](https://www.postgresql.org/docs/15/rangetypes.html),
vilka lanserades i
[förra versionen](https://www.postgresql.org/about/press/presskit14/).

I PostgreSQL 15 kan användare
[skapa vyer med frågor som använder rättigheterna av anroparen, istället för vyns skapare](https://www.postgresql.org/docs/15/sql-createview.html).
Denna funktionen, kallad `security_invoker`, ger ett extra skydd för att 
säkerställa att vyns anropare har rättigheter att arbeta med dess underliggande data.

### Fler möjligheter för logisk replikering

PostgreSQL 15 underlättar hanteringen av
[logisk replikering](https://www.postgresql.org/docs/15/logical-replication.html).
Denna versionen lanserar
[filtrering på rader](https://www.postgresql.org/docs/15/logical-replication-row-filter.html)
och 
[kolumn listor](https://www.postgresql.org/docs/15/logical-replication-col-lists.html)
för
[publicering](https://www.postgresql.org/docs/15/logical-replication-publication.html),
vilket möjliggör replikering av endast en delmängd av all data i en tabell.
PostgreSQL 15 inkluderar också funktioner för att förenkla
[konflikthantering](https://www.postgresql.org/docs/15/logical-replication-conflicts.html)
vilket gör det möjligt att hoppa över återspelning av transaktioner som skapar
konflikter och att automatiskt inaktivera en replikering om ett fel upptäcks.
Stöd för tvåfas-commit (2PC) i logisk replikering lanseras också i denna versionen
av PostgreSQL.

### Förbättringar av loggning och konfiguration

Ett nytt loggningsformat lanseras i PostgreSQL 15:
[`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG).
Med detta formatet skrivs loggposter som en JSON-struktur vilket förenklar
hantering av PostgreSQL loggfiler i externa loggsystem.

Denna versionen ger databasadministratörer större flexibilitet kring hur
användare kan ges rättigheter att hantera serverkonfiguration.
Utöver det så kan användare nu söka information om konfigurationsparametrar
med `\dconfig` kommandot i 
[`psql`](https://www.postgresql.org/docs/15/app-psql.html) programmet.

### Övriga nämnvärda förändringar

[Statistik på servernivå](https://www.postgresql.org/docs/15/monitoring-stats.html)
hanteras nu i delat minne. Detta gör att processen för statistikinsamling är
borttagen.

PostgreSQL 15 gör det möjligt att använda en 
[ICU sorteringsordning](https://www.postgresql.org/docs/15/collation.html) som
standardiserad sorteringsordning för ett helt kluster eller en enskild databas.

[`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html) är en
ny modul som
låter användare inspektera innehållet i write-ahead (WAL) filer genom ett
SQL gränssnitt.

PostgreSQL 15 tar också
[bort `CREATE` behörighet från alla användare](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)
utöver för databasägare i `public` (eller standard) schemat.

Det sedan länge utfasade "exklusiv säkerhetskopiering" driftsläget
samt stöd för Python 2 från PL/Python är borttagna från PostgreSQL 15.

### Om PostgreSQL

[PostgreSQL](https://www.postgresql.org) är världens mest avancerade öppen
källkod databas med tusentals användare, utvecklare, företag och organisationer
världen över. Med mer än 35 års utveckling sedan starten på University of
California, Berkeley, har PostgreSQL fortsatt en oöverträffad utvecklingstakt.
PostgreSQL:s rika funktionalitet matchar inte bara de största proprietära
databassystemen utan överträffar dem med avancerade funktioner, utbyggbarhet,
säkerhet och stabilitet.

### Länkar

* [Nerladdning](https://www.postgresql.org/download/)
* [Nytt i version 15](https://www.postgresql.org/docs/15/release-15.html)
* [Pressmaterial](https://www.postgresql.org/about/press/)
* [Säkerhet](https://www.postgresql.org/support/security/)
* [Versionspolicy](https://www.postgresql.org/support/versioning/)
* [Följ @postgresql på Twitter](https://twitter.com/postgresql)

### Mer information

De följande länkarna har mer information om både de nya funktionerna samt den
sedan tidigare tillgängliga funktionalitet:

* [Nytt i version 15](https://www.postgresql.org/docs/15/release-15.html)
* [Funktionsmatris](https://www.postgresql.org/about/featurematrix/)

### Nerladdning

PostgreSQL 15 kan laddas ner på ett antal olika sätt, bland annat:

* Den [officiella sidan för nerladdningar](https://www.postgresql.org/download/),
vilken har installationsmedia och verktyg för 
[Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) med flera.
* [Källkod](https://www.postgresql.org/ftp/source/v15.0)

Ytterligare verktyg och moduler är tillgängliga via
[PostgreSQL Extension Network](http://pgxn.org/).

### Dokumentation

PostgreSQL 15 kommer med dokumentation i HTML format samt man-sidor, och all
dokumentation kan även läsas på Internet som [HTML](https://www.postgresql.org/docs/15/)
eller [PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf).

### Licens

PostgreSQL använder [PostgreSQL Licensen](https://www.postgresql.org/about/licence/),
en BSD-liknande "tillåtande" licens. Denna
[OSI-certifierade licens](http://www.opensource.org/licenses/postgresql/)
anses flexibel och företagsvänlig
eftersom den inte begränsar användningen av PostgreSQL i kommersiella eller
proprietära applikationer. Licensen, tillsammans med bredd stöd från många företag och ett
publikt ägande av koden, gör att PostgreSQL är väldigt populär bland tillverkare
som vill bygga in en databas i sin produkt utan att riskera avgifter, inlåsning
eller förändrade licensvillkor.

### Kontakter

Hemsida

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)

### Bilder och logotyper

Postgres, PostgreSQL och elefantlogotypen (Slonik) är registrerade varumärken
hos [PostgreSQL Community Association of Canada](https://www.postgres.ca).
Användning av dessa varumärken måste följa dess [varumärkespolicy](https://www.postgresql.org/about/policies/trademarks/).

### Kommersiell support

PostgreSQL projektet stöttas av ett stort antal företag som bland annat
sponsrar utvecklare, erbjuder infrastruktur och ger finansiellt stöd. Se listan
över PostgreSQL:s [sponsorer](https://www.postgresql.org/about/sponsors/) för
mer information om vem de är.

Det finns också en stor grupp företag som säljer
[kommersiell PostgreSQL support](https://www.postgresql.org/support/professional_support/),
allt från små konsultföretag till multinationella företag.

För att ge ett ekonomiskt bidrag till PostgreSQL Global Development Group
eller en av de erkända ideella organisationerna, se sidan för
[donationer](https://www.postgresql.org/about/donate/) för mer information.
