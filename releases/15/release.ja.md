2022年10月13日 - PostgreSQLグローバル開発グループは、本日、[最も先進的なオープンソースデータベース](https://www.postgresql.org/)の最新版である[PostgreSQL 15](https://www.postgresql.org/docs/15/release-15.html)のリリースを発表しました。

PostgreSQL 15では、最近のリリースの性能向上に加えて、ソートの改良を含む、ローカルと分散環境の両方でワークロードを管理するための顕著な利点を備えています。このリリースでは、人気の高い[`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) コマンドを追加し、開発者の使い勝手を向上させました。また、データベースの状態を監視する機能が追加されています。
PostgreSQLのコアチームメンバーであるJonathan Katzは、「PostgreSQLの開発者コミュニティは、開発者体験を改善しながら、高性能なデータワークロードの実行を単純化する機能を構築し続けています」と述べています。「PostgreSQL 15は、オープンなソフトウェア開発を通じて、アプリケーション開発に最適で、重要なデータを安全に保護するデータベースをユーザに提供できることを強調しています。」

[PostgreSQL](https://www.postgresql.org)は、[グローバル開発コミュニティ](https://www.postgresql.org/community/)による25年以上にわたるオープンソース開発の恩恵を受けており、その信頼性と堅牢性で知られる革新的なデータ管理システムです。そしてあらゆる規模の組織に好まれるオープンソースリレーショナルデータベースになっています。


### ソート性能と圧縮率の向上

この最新リリースでは、PostgreSQLはインメモリとディスク上の[ソート](https://www.postgresql.org/docs/15/queries-order.html)アルゴリズムを改善し、ベンチマークではどのデータ型をソートするかによって25%から400%の速度向上を示しています。PostgreSQL 15 では[ウィンドウ関数](https://www.postgresql.org/docs/15/functions-window.html) `row_number()`, `rank()`, `dense_rank()`, `count()` を使用することも 性能上の利点があります。[`SELECT DISTINCT`](https://www.postgresql.org/docs/15/queries-select-lists.html#QUERIES-DISTINCT) を使用した問い合わせが [並列に実行](https://www.postgresql.org/docs/15/parallel-query.html) できるようになりました。

非同期リモートクエリを可能にする [以前の PostgreSQL リリース](https://www.postgresql.org/about/press/presskit14/) を基に、 [PostgreSQL 外部データラッパー](https://www.postgresql.org/docs/15/postgres-fdw.html), [`postgres_fdw`](https://www.postgresql.org/docs/15/postgres-fdw.html) で [非同期コミット](https://www.postgresql.org/docs/15/postgres-fdw.html#id-1.11.7.47.11.7) をサポートするようにしました。

PostgreSQL 15の性能向上はアーカイブとバックアップの機能にも及んでいます。PostgreSQL 15はLZ4とZstandard (zstd) [ライトアヘッドログ (WAL) ファイルへの圧縮](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-WAL-COMPRESSION)のサポートを追加しました。 これは、特定の作業負荷に対して容量と性能の両方の利点をもたらすことができます。特定のオペレーティングシステムでは、PostgreSQL 15はリカバリ時間を短縮するために[WALによって参照されるページのプリフェッチ](https://www.postgresql.org/docs/15/runtime-config-wal.html#GUC-RECOVERY-PREFETCH)のサポートを追加しています。PostgreSQLの組み込みバックアップコマンドである[`pg_basebackup`](https://www.postgresql.org/docs/15/app-pgbasebackup.html)は、gzip、LZ4、zstdから選択できるバックアップファイルのサーバサイド圧縮をサポートするようになりました。PostgreSQL 15では、[アーカイブのためのカスタムモジュール](https://www.postgresql.org/docs/15/archive-modules.html)を使用することで、シェルコマンドを使用するオーバーヘッドをなくすことができるようになりました。


### 表現力豊かな開発者向け機能

PostgreSQL 15 には、標準SQLの [`MERGE`](https://www.postgresql.org/docs/15/sql-merge.html) コマンドが含まれています。`MERGE` を使うと、`INSERT`、`UPDATE`、`DELETE` の操作を一つの文に含めることができる条件付き SQL 文を書くことができます。

この最新リリースでは、文字列を検査するための [正規表現を使用するための新しい関数](https://www.postgresql.org/docs/15/functions-matching.html#FUNCTIONS-POSIX-REGEXP) が追加されています。`regexp_count()`、 `regexp_instr()`、 `regexp_like()`、そして `regexp_substr()` です。PostgreSQL 15 では、`range_agg` 関数を拡張して、[以前のリリース](https://www.postgresql.org/about/press/presskit14/) で導入された [multirange データ型](https://www.postgresql.org/docs/15/rangetypes.html) を集約することもできます。

PostgreSQL 15では、ユーザが[ビューの作成者ではなく呼び出し元の権限を使用してデータを問い合わせるビューを作成する](https://www.postgresql.org/docs/15/sql-createview.html)ことができます。このオプションは `security_invoker` と呼ばれ、ビューの呼び出し元が基礎となるデータを操作するための正しい権限を持っていることを保証するために、追加の保護レイヤーを追加します。


### 論理レプリケーションで広がる選択肢

PostgreSQL 15では、[論理レプリケーション](https://www.postgresql.org/docs/15/logical-replication.html)をより柔軟に管理することができます。このリリースでは、[パブリッシャ](https://www.postgresql.org/docs/15/logical-replication-publication.html)に[行フィルタリング](https://www.postgresql.org/docs/15/logical-replication-row-filter.html)と[列リスト](https://www.postgresql.org/docs/15/logical-replication-col-lists.html)を導入し、ユーザがテーブルからデータのサブセットを複製することを選択できるようにしています。PostgreSQL 15では、競合するトランザクションの再生をスキップする機能や、エラーが検出された場合に自動的にサブスクリプションを無効にする機能など、[競合管理](https://www.postgresql.org/docs/15/logical-replication-conflicts.html)を簡素化する機能が追加されています。このリリースでは、論理レプリケーションでの2相コミット(2PC)の使用もサポートされています。


### ログ出力と設定手法の強化

PostgreSQL 15 では、新しいログ記録形式が導入されました。[`jsonlog`](https://www.postgresql.org/docs/15/runtime-config-logging.html#RUNTIME-CONFIG-LOGGING-JSONLOG)です。この新しい書式は定義されたJSON構造を使用してログデータを出力し、これによりPostgreSQLのログを構造化されたログ記録システムで処理することができます。

このリリースでは、データベース管理者が、ユーザがPostgreSQLの設定を管理する方法をより柔軟に変更できるようになりました。サーバレベルの設定パラメータを変更する権限をユーザに付与する機能が追加されました。さらに、[`psql`](https://www.postgresql.org/docs/15/app-psql.html) コマンドラインツールから `dconfig` コマンドを使用して、設定に関する情報を検索できるようになりました。


### その他の主な変更点

PostgreSQLの[サーバレベル統計](https://www.postgresql.org/docs/15/monitoring-stats.html)が共有メモリで収集されるようになり、統計収集プロセスとこのデータのディスクへの定期的な書き込みの両方が不要になりました。

PostgreSQL 15では、クラスタや個々のデータベースのデフォルト照合順序を[ICU照合順序](https://www.postgresql.org/docs/15/collation.html)にすることが可能です。

このリリースでは、新しい組み込みの拡張機能である [`pg_walinspect`](https://www.postgresql.org/docs/15/pgwalinspect.html) も追加され、ユーザは SQL インターフェースから直接、先行書き込みログファイルの内容を検査できるようになりました。

PostgreSQL 15では、[`public`（またはデフォルト）スキーマのデータベース所有者を除くすべてのユーザから`CREATE`権限を剥奪します](https://www.postgresql.org/docs/15/ddl-schemas.html#DDL-SCHEMAS-PATTERNS)。

PostgreSQL 15では、長い間非推奨だった "排他的バックアップ "モードの削除と、PL/Pythonから、Python 2のサポートが削除されました。


### PostgreSQLについて

[PostgreSQL](https://www.postgresql.org)は、世界で最も先進的なオープンソースデータベースで、何千人ものユーザ、貢献者、企業、組織からなるグローバルコミュニティがあります。カリフォルニア大学バークレー校から始まった35年以上のエンジニアリングを基に、PostgreSQLは比類のないペースで開発を続けてきました。PostgreSQLの成熟した機能セットは、トップクラスのプロプライエタリデータベースシステムに匹敵するだけでなく、先進のデータベース機能、拡張性、セキュリティ、安定性においてそれらを凌駕しています。


### リンク

* [ダウンロード](https://www.postgresql.org/download/)
* [リリースノート](https://www.postgresql.org/docs/15/release-15.html)
* [プレスキット](https://www.postgresql.org/about/press/)
* [セキュリティ](https://www.postgresql.org/support/security/)
* [バージョンポリシー](https://www.postgresql.org/support/versioning/)
* [Twitter](https://twitter.com/postgresql)


## 機能についての詳細

上記の機能などの説明については、以下の資料をご覧ください。

* [リリースノート](https://www.postgresql.org/docs/15/release-15.html)
* [機能一覧表](https://www.postgresql.org/about/featurematrix/)


## ダウンロード先

PostgreSQL 15をダウンロードするには、以下のような方法があります。

* [Official Downloads](https://www.postgresql.org/download/) のページには、[Windows](https://www.postgresql.org/download/windows/), [Linux](https://www.postgresql.org/download/), [macOS](https://www.postgresql.org/download/macosx/) などのインストーラやツールを含みます。
* [ソースコード](https://www.postgresql.org/ftp/source/v15.0)

その他のツールや拡張機能は、[PostgreSQL Extension Network](http://pgxn.org/)で入手できます。


## ドキュメンテーション

PostgreSQL 15 には man ページだけでなく HTML ドキュメントも付属しており、オンラインでも [HTML](https://www.postgresql.org/docs/15/) と [PDF](https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf) の両方の形式でドキュメントを閲覧することが可能です。


## ライセンス

PostgreSQLは、BSDに似た「寛容な」ライセンスである[PostgreSQLライセンス](https://www.postgresql.org/about/licence/)を使用しています。この[OSI認証ライセンス](http://www.opensource.org/licenses/postgresql/)は、PostgreSQLを商用およびプロプライエタリなアプリケーションで使用することを制限しないため、柔軟でビジネスフレンドリーであると広く評価されています。複数企業のサポートやコードの公的所有権とともに、このライセンスは、料金やベンダロックイン、ライセンス条項の変更を心配せずにデータベースを自社製品に組み込みたいと考えるベンダにとってPostgreSQLを非常に人気のあるものにしています。


## お問い合わせ先

ウェブサイト

* [https://www.postgresql.org/](https://www.postgresql.org/)

Email

* [press@postgresql.org](mailto:press@postgresql.org)


## 画像とロゴ

Postgres、PostgreSQL、象のロゴ(Slonik)は、すべて[PostgreSQL Community Association of Canada](https://www.postgres.ca)の登録商標です。これらのマークの使用を希望する場合は、[商標ポリシー](https://www.postgresql.org/about/policies/trademarks/)に従わなければなりません。


## コーポレートサポート

PostgreSQLは、開発者のスポンサーとなり、ホスティングリソースを提供し、財政的なサポートを提供してくれる多くの企業の支援を受けています。これらのプロジェクト支援者の一部は [スポンサー](https://www.postgresql.org/about/sponsors/) のページを参照してください。

また、個人のコンサルタントから多国籍企業まで、[PostgreSQLサポートを提供する企業](https://www.postgresql.org/support/professional_support/)の大きなコミュニティがあります。

PostgreSQL グローバル開発グループ、または認定されたコミュニティの非営利団体に金銭的な寄付をしたい場合は、 [寄付](https://www.postgresql.org/about/donate/) のページを参照してください。
