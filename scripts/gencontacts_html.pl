#!/usr/bin/perl -w
#use strict;

use DBI ;
my $user = "josh" ;
my $passwd = "" ;

my $pdbh = DBI->connect("dbi:Pg:dbname=contacts host=127.0.0.1", $user, $passwd);

$action = $pdbh->prepare("SELECT name, pgemail, office_phone, cell_phone, xtra_line, continent as con, region,
                         url FROM contacts WHERE verified ORDER BY continent, region, name;") ;
$action->execute() ;
$action->bind_columns( undef, \$name, \$pgemail, \$office, \$cell, \$xtra_line, \$continent, \$region, \$url );


open CONTACTS, ">contact.html";
print CONTACTS '{%extends "base/page.html"%}
{%block title%}PostgreSQL Regional Contacts{%endblock%}
{%block contents%}

<div id="pgPressContacts">
<h1>Press Contacts</h1>

<p>Please refer to the list of country contacts below for press enquiries.</p>
<p>For general press enquiries in English, see <a href="#USA">USA &amp; General Enquiries</a>.</p>';
print CONTACTS "\n";

my $last_region;
my $last_continent;

while ( $action->fetch ) {
    if ( $continent ne $last_continent ) {
        if ( defined $last_continent ) {
            print CONTACTS "</dl>\n";
        }
        print CONTACTS "\n<h2>$continent</h2>\n";
        print CONTACTS "<dl>\n";
        $last_continent = $continent;
    }

    if ( $region ne $last_region ) {
        print CONTACTS "<dt>";
        if ( $region =~ "USA" ) {
            print CONTACTS "<a name=\"USA\"></a>";
        }
	print CONTACTS "$region</dt>\n";
        $last_region = $region;
    }
    print CONTACTS "<dd>$name\n";
    print CONTACTS "<br /><a href=\"mailto:$pgemail\">$pgemail</a>\n";
    $office and print CONTACTS "<br />Phone: $office\n";
    $cell and print CONTACTS "<br />Cell: $cell\n";
    $xtra_line and print CONTACTS "<br />$xtra_line\n";
    $url and print CONTACTS "<br /><a href=\"$url\">$url</a>\n";
    print CONTACTS "</dd>\n";
}

print CONTACTS "</dl>\n</div>\n";

print CONTACTS "{%endblock%}\n";

$pdbh->disconnect;
close CONTACTS;

exit(0);
